import '@fontsource/roboto';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

import renderToHTML from './renderToHTML';
import '../src/css/index.css';

// const STORY_SORT_LIST = [];

const customViewports = {
  hdDesktop: {
    name: 'HD Desktop',
    styles: {
      width: '1396px',
      height: '900px',
    },
  },
  smDesktop: {
    name: 'Small Desktop',
    styles: {
      width: '1024px',
      height: '900px',
    },
  },
  tablet: {
    name: 'Tablet',
    styles: {
      width: '768px',
      height: '900px',
    },
  },
  lgMobile: {
    name: 'Large Mobile',
    styles: {
      width: '480px',
      height: '900px',
    },
  },
  mobile: {
    name: 'Mobile',
    styles: {
      width: '360px',
      height: '900px',
    },
  },
};

export const parameters = {
  actions: { argTypesRegex: '^on[A-Z].*' },
  docs: {
    transformSource: (_, storyContext) => renderToHTML(storyContext.storyFn),
  },
  options: {
    storySort: {
      method: '',
      order: [
        'Biweekly Updates',
        ['2021-05-31', '2021-05-17', '2021-05-03', '2021-04-19'],
      ],
      locales: '',
    },
  },
  // previewTabs: { 'storybook/docs/panel': { index: -1 } },
  // viewMode: 'docs',
  viewport: {
    viewports: customViewports,
    defaultViewport: 'smDesktop',
  },
};
