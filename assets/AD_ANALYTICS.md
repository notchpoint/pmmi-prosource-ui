# Sponsored Listing Google Analytics Events

The following pertain to the individual items on the sponsored solutions block
of the company listing page.

| Description    | Category          | Action  | Label            |
| -------------- | ----------------- | ------- | ---------------- |
| **Impression** | Sponsored Listing | Viewed  | _{product name}_ |
| **Click**      | Sponsored Listing | Clicked | _{product name}_ |

## Example implementation

This requires a GA invocation, which should already be on all pages.

```js
/*
 ** for the following examples, assume that the sponsored product's name
 ** is stored in a variable called `sponsoredProductName`
 */

// impression (fires on page load for each listing)
ga('send', 'event', 'Sponsored Listing', 'Viewed', sponsoredProductName);

// click (fires on result link click for each listing)
ga('send', 'event', 'Sponsored Listing', 'Clicked', sponsoredProductName);
```
