# UI Notes

- [Storybook](https://eager-goodall-55d6c1.netlify.app/)
- [UIs](https://vibrant-kare-64df9e.netlify.app/)

## a11y

A lot of UI elements have seemingly random IDs assigned to them. These are, for
the most part, there to identify certain accessibility attributes. Though they
aren't necessary for the design to look okay visually, it's imperative to keep
them so that the site functions as expected for users browsing the site through
screen readers. The value of the ID doesn't matter as long as it matches (like
it does in the UI). I used JS library that automatically assigns IDs so that
they aren't duplicated in the app (which is why they are all numbers), so this
sort of approach is fine.

## Typeahead, filter chips, reset search link, etc.

The JS for these elements were not built out into the UI, but all of the
HTML/CSS is built out for them and demoed across the different pages/Storybook.
Whatever typeahead library is typically used by FUSE probably works fine here
too.

## About the UI app

It's built on top of React/Gatsby, and it may or may not be helpful to reference
depending on how the FUSE app is written. Source code lives in `src`.

## Questions

Please contact [tevin@notchpoint.com](mailto:tevin@notchpoint.com).
