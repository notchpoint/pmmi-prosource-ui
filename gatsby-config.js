module.exports = {
  siteMetadata: {
    title: 'Happy Little Trees',
  },
  plugins: [
    'gatsby-plugin-postcss',
    'gatsby-plugin-resolve-src',
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/,
        },
      },
    },
  ],
};
