import { useId } from '@reach/auto-id';
import * as React from 'react';

import SuggestionChip from 'components/Search/SuggestionChip';

export default function AdditionalCompanies({ list, type }) {
  const headingId = useId();
  return (
    <div className="related-searches">
      <h2 className="related-searches__heading" id={headingId}>
        Related {type} searches
      </h2>
      <ul aria-labelledby={headingId} className="related-searches__list">
        {list.map(listItem => (
          <li className="related-searches__list-item" key={listItem}>
            <SuggestionChip Component="a" href="#" suggestion={listItem} />
          </li>
        ))}
      </ul>
    </div>
  );
}
