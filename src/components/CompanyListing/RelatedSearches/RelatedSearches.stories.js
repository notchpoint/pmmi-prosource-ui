import React from 'react';

import RelatedSearches from './RelatedSearches';

export default {
  title: 'Pages/Company Listing Page/Related Searches',
  component: RelatedSearches,
};

const Template = ({ text, ...args }) => <RelatedSearches {...args} />;

export const Story = Template.bind({});

Story.args = {
  list: [
    'Aerosol & Dispensing Filling Systems',
    'Piston Fillers',
    'Vacuum Fillers',
    'Pressure Fillers',
    'Flow Meter Fillers',
  ],
  type: 'Liquid Fillers',
};
