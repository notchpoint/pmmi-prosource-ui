import { useId } from '@reach/auto-id';
import faker from 'faker';
import * as React from 'react';

export default function SponsoredListings() {
  const headingId = useId();
  const descriptionId = useId();
  return (
    <section className="sponsored-listings" aria-labelledby={headingId}>
      <p className="sponsored-listings__preamble">
        Sponsored content; company listings continue after
      </p>
      <div className="sponsored-listings__main">
        <div className="sponsored-listings__introduction">
          <h2 className="sponsored-listings__heading" id={headingId}>
            Sponsored content related to your search
          </h2>
          <p className="sponsored-listings__description" id={descriptionId}>
            These companies produce machinery that can complete your Gravity
            Fillers needs
          </p>
        </div>
        <div className="sponsored-listings__body">
          <SponsoredListing />
          <SponsoredListing />
          <SponsoredListing />
        </div>
      </div>
    </section>
  );
}

const image = faker.image.image(200, 150);

function SponsoredListing() {
  return (
    <div className="sponsored-listing">
      <img className="sponsored-listing__image" src={image} alt="demo image" />
      <div className="sponsored-listing__body">
        <h3 className="sponsored-listing__company">Paxton Products</h3>
        <h4 className="sponsored-listing__heading">
          Paxton Products' Air blow-off systems
        </h4>
        <p className="sponsored-listing__description">
          Complete air rinsing, knife dryers, blow-off cleaning systems for your
          filling, capping and labeling line. Paxton Products is a pioneer in
          development of air-knives as
        </p>
      </div>
    </div>
  );
}
