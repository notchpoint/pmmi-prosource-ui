import * as React from 'react';

import SponsoredListings from './SponsoredListings';

export default {
  title: 'Pages/Company Listing Page/Sponsored Listings',
  component: SponsoredListings,
};

const Template = args => <SponsoredListings {...args} />;

export const Story = Template.bind({});

Story.args = {};
