import { useId } from '@reach/auto-id';
import * as React from 'react';

export default function AdditionalCompanies({ list }) {
  const headingId = useId();
  return (
    <div className="additional-companies">
      <h2 className="additional-companies__heading" id={headingId}>
        Additional companies you may be interested in
      </h2>
      <ul aria-labelledby={headingId}>
        {list.map(listItem => (
          <li className="additional-companies__list-item" key={listItem}>
            <a className="additional-companies__link" href="#">
              {listItem}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}
