import React from 'react';

import AdditionalCompanies from './AdditionalCompanies';

export default {
  title: 'Pages/Company Listing Page/Additional Companies',
  component: AdditionalCompanies,
};

const Template = ({ text, ...args }) => <AdditionalCompanies {...args} />;

export const Story = Template.bind({});

Story.args = {
  list: [
    'Asceptic Filling Equipment & Packaging Machines, Inc.',
    'Chase-Logeman',
    'Kaps-All Packaging Systems',
    'M & O Perry Industries, Inc.',
    'Neumann Packaging Services LLC',
    'Optima',
    'Pope Scientific Inc',
  ],
};
