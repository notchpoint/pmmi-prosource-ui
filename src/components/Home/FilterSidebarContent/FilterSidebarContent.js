import * as React from 'react';

import {
  Heading,
  GlossarySection,
  SearchButton,
} from 'components/MobileSidebar';
import ParentFilter from 'components/ParentFilter';
import EquipmentFilter from 'components/Search/EquipmentFilter';

export default function FilterSidebarContentComponent({ isMobile = false }) {
  return (
    <div className="home-filter-sidebar-content">
      <div className="border-between">
        <Heading label="Browse by" />
        <ParentFilter name="Select your package type">
          <EquipmentFilter name="Rigid" isExpanded={!isMobile} />
        </ParentFilter>
      </div>
      <SearchButton>Search</SearchButton>
      <GlossarySection />
    </div>
  );
}
