import React from 'react';

import Heading from './Heading';

export default {
  title: 'Pages/Home Page/Heading',
  component: Heading,
};

const Template = ({ text, ...args }) => <Heading {...args}>{text}</Heading>;

export const Story = Template.bind({});

Story.args = {
  text: 'Equipment, Materials & Services',
  prefix: 'Browse companies by',
};
