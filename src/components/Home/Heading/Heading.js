import * as React from 'react';

export default function Heading({ children, prefix, props }) {
  return (
    <h1 className="home-heading" {...props}>
      <span className="home-heading__prefix">{prefix}</span>
      <span className="home-heading__main">{children}</span>
    </h1>
  );
}
