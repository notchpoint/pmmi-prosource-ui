import React from 'react';

import CategoryCard from './CategoryCard';

export default {
  title: 'Pages/Home Page/Category Card',
  component: CategoryCard,
};

const Template = ({ text, ...args }) => (
  <div style={{ width: '240px' }}>
    <CategoryCard {...args} />
  </div>
);

export const Story = Template.bind({});

Story.args = {
  name: 'Processing Equipment',
  count: 132,
  tooltip: 'This is some more information',
};
