import { useId } from '@reach/auto-id';
import faker from 'faker';
import * as React from 'react';

import Tooltip from 'components/Tooltip';

export default function CategoryCard({ count, name, tooltip }) {
  const headingId = useId();
  const descriptionId = useId();
  const imageUrl = faker.image.image(250, 250);
  return (
    <div
      className="home-category-card"
      aria-labelledby={headingId}
      aria-describedby={descriptionId}
    >
      <img className="home-category-card__image" src={imageUrl} alt={name} />
      <div className="home-category-card__content">
        <h2 className="home-category-card__name" id={headingId}>
          <a href="#">{name}</a>
        </h2>
      </div>
      <div className="home-category-card__footer">
        <p className="home-category-card__count" id={descriptionId}>
          {count} Companies
        </p>
        <Tooltip tooltipContent={tooltip} />
      </div>
    </div>
  );
}
