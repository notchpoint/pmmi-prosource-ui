import clsx from 'clsx';
import * as React from 'react';

import ChevronRightIcon from 'components/Icons/ChevronRightIcon';

export default function FilterLayout({ children, isExpanded }) {
  return (
    <div
      className={clsx('filter-layout', {
        'filter-layout--collapsed': !isExpanded,
      })}
      data-filter-layout
    >
      {children}
    </div>
  );
}

FilterLayout.Content = function ({ children, withBreadcrumbs }) {
  return (
    <div
      id="main-content"
      className={clsx('filter-layout__content', {
        'filter-layout__content--with-breadcrumbs': withBreadcrumbs,
      })}
    >
      {children}
    </div>
  );
};

FilterLayout.Aside = function ({ children, isExpanded = false }) {
  const sidebarContentId = 'sidebar-content';

  return (
    <aside
      className={clsx('filter-layout__aside', {
        'filter-layout__aside--collapsed': !isExpanded,
      })}
      data-desktop-sidebar=""
    >
      <button
        className="filter-layout__aside-toggle"
        aria-controls={sidebarContentId}
        aria-expanded={isExpanded}
        data-desktop-sidebar-toggle=""
      >
        <ChevronRightIcon aria-hidden="true" />
        <div className="sr-only">toggle sidebar</div>
      </button>

      <div
        className="filter-layout__aside-content"
        id="sidebar-content"
        role="region"
      >
        {children}
      </div>
    </aside>
  );
};
