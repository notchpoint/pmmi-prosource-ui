import faker from 'faker';
import * as React from 'react';

import ParentCompanyCard from './ParentCompanyCard';

export default {
  title: 'Pages/Parent Company Page/Parent Company Card',
  component: ParentCompanyCard,
};

const Template = args => (
  <div style={{ width: '300px' }}>
    <ParentCompanyCard {...args} />
  </div>
);

export const Story = Template.bind({});

Story.args = {
  description: 'Product & Package Handling Equipment',
  image: faker.image.image(200, 50),
  name: 'Allpax',
};
