import { useId } from '@reach/auto-id';
import * as React from 'react';

import CtaLink from 'components/CtaLink';

export default function ParentCompanyCard({ description, image, name }) {
  const headingId = useId();
  const descriptionId = useId();

  return (
    <article
      className="parent-company-card"
      aria-labelledby={headingId}
      aria-describedby={descriptionId}
    >
      <h2 className="parent-company-card__heading" id={headingId}>
        {name}
      </h2>
      <div className="parent-company-card__content">
        <img alt={name} src={image} />
        <p id={descriptionId}>{description}</p>
      </div>
      <div className="parent-company-card__actions">
        <CtaLink>View Company Profile</CtaLink>
      </div>
    </article>
  );
}
