import * as React from 'react';

export default function SkipLink({ children, href }) {
  return (
    <a href={href} className="skip-link">
      {children}
    </a>
  );
}
