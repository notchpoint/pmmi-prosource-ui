import '@fontsource/roboto';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';
import clsx from 'clsx';
import faker from 'faker';
import * as React from 'react';
import Helmet from 'react-helmet';

import Breadcrumbs from './Breadcrumbs';
import Footer from './Footer';
import Header from './Header';
import SkipLink from './SkipLink';

export default function Page({ children, isShaded, withImageBg }) {
  return (
    <React.Fragment>
      <Helmet>
        <script src="/scripts.js" type="text/javascript" />
      </Helmet>
      <div
        className={clsx('page', { 'page--shaded': isShaded })}
        style={
          withImageBg
            ? {
                backgroundImage: `url("${faker.image.image(3000, 500)}")`,
                backgroundRepeat: 'no-repeat',
                backgroundSize: 'auto',
              }
            : {}
        }
      >
        <SkipLink href="#header-search-input">Skip to search</SkipLink>
        <SkipLink href="#main-content">Skip to main content</SkipLink>
        <Header />
        {children}
        <Footer />
      </div>
    </React.Fragment>
  );
}

Page.Breadcrumbs = Breadcrumbs;

Page.Content = function ({ children, className, ...props }) {
  return (
    <div className={clsx('container', className)} {...props}>
      {children}
    </div>
  );
};
