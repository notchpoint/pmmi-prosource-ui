import React from 'react';

import Header from './Header';

export default {
  title: 'General/Header',
  component: Header,
};

const Template = args => <Header {...args} />;

export const Story = Template.bind({});

Story.args = {};
