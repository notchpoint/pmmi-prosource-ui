import * as React from 'react';

import Navigation from './Navigation';
import SearchBar from './SearchBar';

export default function Header() {
  return (
    <header className="header">
      <div className="header__logo">
        <a className="block" href="#">
          <img
            src={'/logo.svg'}
            alt="PMMI ProSource"
            width="220px"
            height="60px"
          />
        </a>
      </div>

      <Navigation items={[{ label: 'Articles', href: '#' }]} />
      <SearchBar />
    </header>
  );
}
