import clsx from 'clsx';
import { useCombobox } from 'downshift';
import faker from 'faker';
import * as React from 'react';

import ArrowRightIcon from 'components/Icons/ArrowRightIcon';
import CloseIcon from 'components/Icons/CloseIcon';
import SearchIcon from 'components/Icons/SearchIcon';

const AUTOSUGGEST_MAX = 7;

const capitalize = s => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
};

const flags = new Set();
const TYPEAHEAD_DATA = new Array(300)
  .fill(undefined)
  .map(() =>
    faker.random.arrayElement([
      {
        label: capitalize(faker.commerce.productMaterial()),
        type: 'Equipment Category',
      },
      {
        label: faker.company.companyName(),
        type: 'Company',
      },
    ]),
  )
  .filter(entry => {
    if (flags.has(entry.label)) {
      return false;
    }
    flags.add(entry.label);
    return true;
  });

export default function SearchBar() {
  const [inputItems, setInputItems] = React.useState(TYPEAHEAD_DATA);

  const {
    isOpen,
    getLabelProps,
    getMenuProps,
    getInputProps,
    getComboboxProps,
    highlightedIndex,
    getItemProps,
    inputValue,
    reset,
  } = useCombobox({
    id: 'header-search',
    items: inputItems,
    itemToString: item => (item ? item.label : ''),
    onInputValueChange: ({ inputValue }) => {
      setInputItems(
        TYPEAHEAD_DATA.filter(item =>
          item.label.toLowerCase().startsWith(inputValue.toLowerCase()),
        ).slice(0, AUTOSUGGEST_MAX),
      );
    },
  });

  const isSearching = isOpen && inputValue && !!inputItems.length;

  return (
    <div
      aria-label="search"
      role="search"
      className="search-bar"
      {...getComboboxProps()}
    >
      <label className="sr-only" {...getLabelProps()}>
        Search
      </label>
      <input
        className={clsx('search-bar__input', {
          'search-bar__input--open': isSearching,
        })}
        placeholder="Liquid fillers, cartoners, vertical baggers, etc."
        {...getInputProps()}
      />
      <button
        className="search-bar__clear"
        type="button"
        onClick={() => {
          reset();
        }}
      >
        <span className="sr-only">Clear search</span>
        <CloseIcon aria-hidden="true" />
      </button>
      <SearchIcon className="search-bar__icon" aria-hidden="true" />
      <button className="search-bar__submit" type="submit">
        <span className="sr-only">Search</span>
        <ArrowRightIcon aria-hidden="true" className="tablet:hidden" />
        <SearchIcon aria-hidden="true" className="hidden tablet:block" />
      </button>
      <ul
        className={clsx('search-autosuggest-list', {
          'search-autosuggest-list--open': isSearching,
        })}
        {...getMenuProps()}
      >
        {isOpen &&
          inputValue &&
          !!inputItems.length &&
          inputItems.map((item, index) => (
            <li
              className={clsx('search-autosuggest-list-item', {
                'search-autosuggest-list-item--highlighted':
                  highlightedIndex === index,
              })}
              key={`${item.label}${index}`}
              {...getItemProps({ key: item.label, item, index })}
            >
              <span className="search-autosuggest-list-item__label">
                {item.label}
              </span>
              <span className="search-autosuggest-list-item__type">
                {item.type}
              </span>
              <SearchIcon
                aria-hidden="true"
                className="search-autosuggest-list-item__icon"
              />
            </li>
          ))}
      </ul>
    </div>
  );
}
