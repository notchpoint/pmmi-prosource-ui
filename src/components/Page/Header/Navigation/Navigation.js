import * as React from 'react';

import CloseIcon from 'components/Icons/CloseIcon';

export default function Navigation({ items }) {
  return (
    <React.Fragment>
      <button className="mobile-nav-trigger">
        <HamburgerIcon
          aria-haspopup="true"
          aria-controls="main-nav"
          aria-expanded="false"
          className="mobile-nav-trigger__icon"
          data-main-nav-toggle=""
        />
        <span className="sr-only">toggle menu</span>
      </button>
      <div
        data-main-nav=""
        className="mobile-nav-menu"
        id="main-nav"
        role="region"
      >
        <div className="mobile-nav-menu__header">
          <img
            src={'/logo.svg'}
            alt="PMMI ProSource"
            width="220px"
            height="60px"
          />
          <button
            className="mobile-nav-menu__close-button"
            data-main-nav-toggle=""
            aria-expanded="true"
          >
            <span className="sr-only">close navigation menu</span>
            <CloseIcon />
          </button>
        </div>
        <div className="mobile-nav-menu__content">
          <nav className="mobile-main-nav" aria-label="mobile main menu">
            <ul
              className="mobile-main-nav__list"
              role="menubar"
              aria-hidden="false"
            >
              {items.map(({ href, label }) => {
                return (
                  <li
                    className="mobile-main-nav__list-item"
                    role="menuitem"
                    key={label}
                  >
                    <a className="mobile-main-nav__link" href={href}>
                      {label}
                    </a>
                  </li>
                );
              })}
            </ul>
          </nav>
        </div>
      </div>
      <nav className="main-nav" aria-label="main menu">
        <ul className="main-nav__list" role="menubar" aria-hidden="false">
          {items.map(({ href, label }) => {
            return (
              <li className="main-nav__list-item" role="menuitem" key={label}>
                <a className="main-nav__link" href={href}>
                  {label}
                </a>
              </li>
            );
          })}
        </ul>
      </nav>
    </React.Fragment>
  );
}

function HamburgerIcon(props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      stroke="currentColor"
      {...props}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth={2}
        d="M4 6h16M4 12h16M4 18h16"
      />
    </svg>
  );
}
