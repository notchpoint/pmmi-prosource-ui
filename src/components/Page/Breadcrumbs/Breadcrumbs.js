import clsx from 'clsx';
import * as React from 'react';

import ChevronLeftIcon from 'components/Icons/ChevronLeftIcon';
import ChevronRightIcon from 'components/Icons/ChevronRightIcon';

export default function Breadcrumbs({ links }) {
  const showCondensedMobile = links.length > 2;

  let crumbs = (
    <nav aria-label="breadcrumbs">
      <ol className="breadcrumb__list">
        {links.map((link, index) => {
          const isLastLink = index + 1 === links.length && 'page';
          return (
            <React.Fragment key={link}>
              <li
                className="breadcrumb__item"
                aria-current={isLastLink ? 'page' : undefined}
              >
                <a href="#" className="breadcrumb__link">
                  {link}
                </a>
                {!isLastLink && (
                  <ChevronRightIcon
                    className="breadcrumb__separator"
                    aria-hidden="true"
                  />
                )}
              </li>
            </React.Fragment>
          );
        })}
      </ol>
    </nav>
  );

  return (
    <div
      className={clsx('breadcrumb', {
        'breadcrumb--mobile-condensed': showCondensedMobile,
      })}
    >
      {showCondensedMobile && (
        <a href="#">
          <ChevronLeftIcon aria-hidden="true" />
          Back to {links[links.length - 1]}
        </a>
      )}
      {crumbs}
    </div>
  );
}
