import React from 'react';

import Breadcrumbs from './Breadcrumbs';

export default {
  title: 'General/Breadcrumbs',
  component: Breadcrumbs,
};

const Template = args => <Breadcrumbs {...args} />;

export const Story = Template.bind({});

Story.args = {
  links: [
    'Home',
    'Filling & Closing',
    'Liquid Fillers',
    'Gravity Filler',
    'Fogg Filler',
  ],
};
