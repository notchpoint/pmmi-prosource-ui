import * as React from 'react';

import Navigation from './Navigation';

export default function Footer() {
  return (
    <footer className="footer">
      <p>&copy;2021 PMMI.</p>
      <Navigation
        items={[
          { label: 'Privacy Policy', href: '#' },
          { label: 'Terms of Use', href: '#' },
        ]}
      />
    </footer>
  );
}
