import * as React from 'react';

export default function Navigation({ items }) {
  return (
    <nav className="footer-nav" aria-label="resources">
      <ul className="footer-nav__list">
        {items.map(({ label, href }) => {
          return (
            <li className="footer-nav__list-item" key={label}>
              <a className="footer-nav__link" href={href}>
                {label}
              </a>
            </li>
          );
        })}
      </ul>
    </nav>
  );
}
