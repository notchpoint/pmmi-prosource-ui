import * as React from 'react';

export default function Image(props) {
  return (
    <div className="image">
      <img {...props} />
    </div>
  );
}
