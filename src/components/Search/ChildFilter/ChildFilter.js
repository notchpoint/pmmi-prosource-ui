import clsx from 'clsx';
import faker from 'faker';
import * as React from 'react';

export default function ChildFilter({
  children,
  name,
  fieldsId,
  isExpanded = true,
}) {
  fieldsId = fieldsId || `${name}-fields`;
  return (
    <fieldset
      className={clsx('child-filter', {
        'child-filter--collapsed': !isExpanded,
      })}
    >
      <legend className="child-filter__legend">
        <button
          aria-controls={fieldsId}
          aria-expanded={isExpanded}
          data-toggle=""
        >
          {name}
          <span className="child-filter__toggle">
            <svg viewBox="0 0 10 10" focusable="false">
              <rect className="vert" height="8" width="2" y="1" x="4" />
              <rect height="2" width="8" y="4" x="1" />
            </svg>
            <span className="sr-only">Show {name} filters</span>
          </span>
        </button>
      </legend>
      <div className="child-filter__body" id={fieldsId} role="region">
        {children}
      </div>
    </fieldset>
  );
}
