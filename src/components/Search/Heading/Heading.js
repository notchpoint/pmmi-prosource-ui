import { useId } from '@reach/auto-id';
import * as React from 'react';

import SuggestionChip from 'components/Search/SuggestionChip';

export default function Heading({ keyword, hasResults = true, suggestions }) {
  const suggestionsHeadingId = useId();
  return (
    <div className="search-heading">
      <h1 className="search-heading__title">
        {hasResults ? 'Search results for' : 'No search results for'} "
        <span className="search-heading__keyword">{keyword}</span>"
      </h1>
      {suggestions && (
        <React.Fragment>
          <h2
            className="search-heading__suggestions-title"
            id={suggestionsHeadingId}
          >
            Did you mean:
          </h2>
          <ul
            aria-describedby={suggestionsHeadingId}
            className="search-heading__suggestions-list"
          >
            {suggestions.map(suggestion => (
              <li key={suggestion}>
                <SuggestionChip suggestion={suggestion} />
              </li>
            ))}
          </ul>
        </React.Fragment>
      )}
    </div>
  );
}
