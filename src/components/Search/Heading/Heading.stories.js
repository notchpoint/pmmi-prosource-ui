import React from 'react';

import Heading from './Heading';

export default {
  title: 'Search/Heading',
  component: Heading,
};

const Template = args => <Heading {...args} />;

export const Story = Template.bind({});

Story.args = {
  keyword: 'Label',
};
