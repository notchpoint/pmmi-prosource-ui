import * as React from 'react';

export default function KeywordHighlight({ highlight, phrase }) {
  const searchRegExp = new RegExp(highlight, 'g');
  return (
    <span
      dangerouslySetInnerHTML={{
        __html: phrase.replace(
          searchRegExp,
          `<span class="keyword-highlight">${highlight}</span>`,
        ),
      }}
    />
  );
}
