import React from 'react';

import KeywordHighlight from './KeywordHighlight';

export default {
  title: 'Search/Keyword Highlight',
  component: KeywordHighlight,
};

const Template = args => <KeywordHighlight {...args} />;

export const Story = Template.bind({});

Story.args = {
  phrase: 'Liquid fillers',
  highlight: 'Li',
};
