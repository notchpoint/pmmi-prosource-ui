import * as React from 'react';

import CloseIcon from 'components/Icons/CloseIcon';

export default function SelectedFilterChip({ name }) {
  return (
    <span className="selected-filter-chip">
      <span>{name}</span>
      <button className="selected-filter-chip__button">
        <CloseIcon aria-hidden="true" />
        <span className="sr-only">stop filtering by {name}.</span>
      </button>
    </span>
  );
}

SelectedFilterChip.List = function ({ children }) {
  return (
    <ul aria-label="current filters" className="selected-filter-list">
      {children}
    </ul>
  );
};

SelectedFilterChip.ListItem = function ({ children }) {
  return <li className="m-8">{children}</li>;
};
