import React from 'react';

import SelectedFilterChip from './SelectedFilterChip';

export default {
  title: 'Search/Selected Filter Chip',
  component: SelectedFilterChip,
};

const Template = args => <SelectedFilterChip {...args} />;

export const Story = Template.bind({});

Story.args = {
  name: 'Caustic/volatile chemicals',
};
