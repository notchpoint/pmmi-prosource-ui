import * as React from 'react';

import SearchIcon from 'components/Icons/SearchIcon';

export default function SuggestionChip({
  Component = 'button',
  suggestion,
  ...props
}) {
  return (
    <Component
      aria-label={`search for ${suggestion}`}
      className="search-suggestion"
      data-truncate
      {...props}
    >
      <SearchIcon className="search-suggestion__icon" />
      {suggestion}
    </Component>
  );
}
