import React from 'react';

import SuggestionChip from './SuggestionChip';

export default {
  title: 'Search/Suggestion Chip',
  component: SuggestionChip,
};

const Template = args => <SuggestionChip {...args} />;

export const Story = Template.bind({});

Story.args = {
  suggestion: 'Labeling Equipment',
};
