import { useId } from '@reach/auto-id';
import * as React from 'react';

export default function SearchSection({ children, keyword, type }) {
  const headingId = useId();
  return (
    <section className="search-section" aria-labelledby={headingId}>
      <h2 className="search-section__heading" id={headingId}>
        {type} matching "
        <span className="search-section__heading-keyword">{keyword}</span>"
      </h2>
      {children}
    </section>
  );
}
