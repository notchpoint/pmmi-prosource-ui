import React from 'react';

import SearchTip from './SearchTip';

export default {
  title: 'Search/No Results Search Tip',
  component: SearchTip,
};

const Template = args => <SearchTip {...args} />;

export const Story = Template.bind({});

Story.args = {
  title: (
    <React.Fragment>
      Check for <a href="#">typos</a>
    </React.Fragment>
  ),
  description: (
    <React.Fragment>
      <strong>Gravity filler</strong> instead of{' '}
      <strong>Gravitie filler</strong>
    </React.Fragment>
  ),
};
