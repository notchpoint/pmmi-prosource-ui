import * as React from 'react';

export default function SearchTip({ title, description, image }) {
  return (
    <div className="search-tip">
      <h3 className="search-tip__title">{title}</h3>
      {description && <p className="search-tip__description">{description}</p>}
      {image && <img src={image} alt="demo image" />}
    </div>
  );
}
