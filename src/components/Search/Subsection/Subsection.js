import { useId } from '@reach/auto-id';
import * as React from 'react';

import KeywordHighlight from '../KeywordHighlight';

export default function SearchSubsection({ children, description, heading }) {
  const headingId = useId();
  const descriptionId = useId();
  return (
    <article
      className="search-subsection"
      aria-labelledby={headingId}
      aria-describedby={descriptionId}
    >
      <h3 className="search-subsection__heading" id={headingId}>
        {heading}
      </h3>
      <p className="search-subsection__description" id={descriptionId}>
        {description}
      </p>
      {children}
    </article>
  );
}
