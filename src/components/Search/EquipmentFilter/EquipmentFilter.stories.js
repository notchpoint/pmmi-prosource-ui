import React from 'react';

import EquipmentFilter from './EquipmentFilter';

export default {
  title: 'Search/Equipment Filter',
  component: EquipmentFilter,
};

const Template = args => (
  <div style={{ width: '300px' }}>
    <EquipmentFilter {...args} />
  </div>
);

export const Story = Template.bind({});

Story.args = {
  name: 'Rigid',
};
