import clsx from 'clsx';
import faker from 'faker';
import * as React from 'react';

import ChildFilter from '../ChildFilter';

const FIELDS = [
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Bottles & Jars',
    name: 'rigid',
    value: 'bottles-and-jars',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Cans & Canisters',
    name: 'rigid',
    value: 'cans-and-canisters',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Trays',
    name: 'rigid',
    value: 'trays',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Cartons, Folding',
    name: 'rigid',
    value: 'cartons-folding',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Cartons, Gabletop',
    name: 'rigid',
    value: 'cartons-gabletop',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Cups, bowls & tubs',
    name: 'rigid',
    value: 'cups-bowls-and-tubs',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Ampoules & vials',
    name: 'rigid',
    value: 'ampoules-vials',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Bag-in box for liquids',
    name: 'rigid',
    value: 'bag-in-box-for-liquids',
  },
  {
    image: faker.image.imageUrl(150, 150),
    label: 'Cartridges',
    name: 'rigid',
    value: 'cartridges',
  },
];

export default function EquipmentFilter({ name, isExpanded = true }) {
  return (
    <ChildFilter name={name} isExpanded={isExpanded}>
      <div className="equipment-filter-fields">
        {FIELDS.map(field => (
          <React.Fragment key={field.value}>
            <input
              className="equipment-filter-checkbox"
              type="checkbox"
              name={field.name}
              id={field.value}
              value={field.value}
            />
            <label htmlFor={field.value} className="equipment-filter-label">
              <div className="equipment-filter-label__img">
                <img src={field.image} alt="" />
              </div>
              <span className="equipment-filter-label__text">
                {field.label}
              </span>
            </label>
          </React.Fragment>
        ))}
      </div>
    </ChildFilter>
  );
}
