import React from 'react';

import BackToHomeLink from './BackToHomeLink';

export default {
  title: 'General/Back To Home Link',
  component: BackToHomeLink,
};

const Template = ({ text, ...args }) => <BackToHomeLink {...args} />;

export const Story = Template.bind({});

Story.args = {};
