import * as React from 'react';

export default function BackToHomeLink() {
  return (
    <a className="back-to-home" href="#">
      Back to home
    </a>
  );
}
