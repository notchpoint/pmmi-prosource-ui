import * as React from 'react';

import Checkbox from 'components/Checkbox';
import ChevronDownIcon from 'components/Icons/ChevronDownIcon';
import FilterBlock from 'components/FilterBlock';

//https://www.sketch.com/s/d1bf1b62-abec-4624-a81f-94668addb2d8/a/1KPkaP4
export default function CategoryFilter({ isExpanded = true, name, options }) {
  const fieldsId = name.toLowerCase().replace(/\W/g, '-');
  return (
    <FilterBlock
      isExpanded={isExpanded}
      fieldsId={fieldsId}
      name={name}
      as="fieldset"
    >
      <FilterBlock.Trigger as="legend">
        <button
          aria-controls={fieldsId}
          aria-expanded={isExpanded}
          data-category-filter-toggle=""
        >
          {name}
          <ChevronDownIcon className="category-filter__toggle-icon" />
        </button>
      </FilterBlock.Trigger>
      <FilterBlock.Content className="category-filter-fields">
        {options.map(option => {
          return <Checkbox key={option} option={option} fieldsId={fieldsId} />;
        })}
      </FilterBlock.Content>
    </FilterBlock>
  );
}
