import React from 'react';

import CategoryFilter from './CategoryFilter';

export default {
  title: 'Search/Category Filter',
  component: CategoryFilter,
  parameters: {
    backgrounds: {
      default: 'dark',
    },
  },
};

const Template = args => (
  <div style={{ width: '350px' }}>
    <CategoryFilter {...args} />
  </div>
);

export const Story = Template.bind({});

Story.args = {
  isExpanded: true,
  name: 'Product Type',
  options: [
    { name: 'Non-viscous liquids', resultCount: 10 },
    { name: 'Viscous liquids', resultCount: 4 },
    { name: 'Liquids with particulates', resultCount: 2 },
    { name: 'Caustic/volatile chemicals', resultCount: 11 },
  ],
};
