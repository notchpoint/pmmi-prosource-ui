import * as React from 'react';

import InformationCircleIcon from 'components/Icons/InformationCircleIcon';

export default function Tooltip({ tooltipContent }) {
  return (
    <span className="tooltip">
      <button
        className="tooltip__trigger"
        type="button"
        data-tippy-content={tooltipContent}
      >
        <InformationCircleIcon className="tooltip__icon" aria-hidden="true" />
        <span className="sr-only">more info</span>
      </button>
    </span>
  );
}
