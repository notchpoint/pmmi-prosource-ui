import React from 'react';

import Tooltip from './Tooltip';

export default {
  title: 'General/Tooltip',
  component: Tooltip,
};

const Template = ({ text, ...args }) => <Tooltip {...args}>{text}</Tooltip>;

export const Story = Template.bind({});

Story.args = {
  tooltipContent:
    'Here is more information about the item. It may not be as concise as we might like, in which case it would wrap with a max width so that we do not lose any readability',
};
