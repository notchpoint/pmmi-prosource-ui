import React from 'react';

import CtaLink from './CtaLink';

export default {
  title: 'General/Call to Action Link',
  component: CtaLink,
};

const Template = ({ text, ...args }) => <CtaLink {...args}>{text}</CtaLink>;

export const Story = Template.bind({});

Story.args = {
  text: 'View 36 Companies',
};
