import clsx from 'clsx';
import * as React from 'react';

export default function CtaLink({ children, className, ...props }) {
  return (
    <a href="#" className={clsx('cta-link', className)} {...props}>
      {children}
    </a>
  );
}
