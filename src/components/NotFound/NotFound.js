import faker from 'faker';
import * as React from 'react';

import ArrowRightIcon from 'components/Icons/ArrowRightIcon';

const imageUrl = faker.image.image(500, 500);

export default function NotFound() {
  return (
    <div className="not-found">
      <img className="not-found__image" src={imageUrl} alt="demo image." />
      <div className="not-found__content">
        <h1 className="not-found__heading">
          We could not find the page you’re looking for
        </h1>
        <div className="not-found__body">
          <p className="not-found__description">
            The page either doesn’t exist or has been moved. Try searching in
            the bar above or use the links below to get back on track.
          </p>
          <ul className="not-found__list">
            <li className="not-found__list-item">
              <a href="#">
                Search the directory
                <ArrowRightIcon />
              </a>
            </li>
            <li className="not-found__list-item">
              <a href="#">
                Articles
                <ArrowRightIcon />
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
