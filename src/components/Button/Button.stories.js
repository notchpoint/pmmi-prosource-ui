import React from 'react';

import Button from './Button';

export default {
  title: 'General/Button',
  component: Button,
};

const Template = ({ text, ...args }) => <Button {...args}>{text}</Button>;

export const Story = Template.bind({});

Story.args = {
  text: 'Button',
  color: 'default',
};
