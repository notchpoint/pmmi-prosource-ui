import clsx from 'clsx';
import PropTypes from 'prop-types';
import * as React from 'react';

function getButtonColorClass(color) {
  switch (color) {
    case 'navy':
      return 'button--navy';
    case 'red':
      return 'button--red';
    default:
      return undefined;
  }
}

export default function Button({
  as: Component = 'button',
  children,
  color,
  ...props
}) {
  return (
    <Component
      className={clsx('button', getButtonColorClass(color))}
      {...props}
    >
      {children}
    </Component>
  );
}

Button.propTypes = {
  color: PropTypes.oneOf(['blue', 'navy', 'red']),
};
