import { useId } from '@reach/auto-id';
import * as React from 'react';

//https://www.sketch.com/s/d1bf1b62-abec-4624-a81f-94668addb2d8/a/1KPkaP4
export default function Field({ option, fieldsId }) {
  const inputId = useId();
  return (
    <div className="checkbox" key={inputId}>
      <input
        className="checkbox__input"
        id={inputId}
        name={fieldsId}
        type="checkbox"
      />
      <label className="checkbox__label" htmlFor={inputId}>
        {option.name} <span aria-hidden="true">({option.resultCount})</span>
        <span className="sr-only">{option.resultCount} results</span>
      </label>
    </div>
  );
}
