import * as React from 'react';

import FilterBlock from 'components/FilterBlock';
import ChevronDownIcon from 'components/Icons/ChevronDownIcon';

export default function ChildFilter({
  children,
  isCollapsible = false,
  isExpanded = true,
  name,
}) {
  const fieldsId = name.toLowerCase().replace(/\W/g, '-');
  return (
    <FilterBlock
      isExpanded={isExpanded}
      fieldsId={fieldsId}
      name={name}
      as="div"
      isCollapsible={isCollapsible}
    >
      <FilterBlock.Trigger as="h3">
        {name}
        <ChevronDownIcon className="category-filter__toggle-icon" />
      </FilterBlock.Trigger>
      <FilterBlock.Content className="child-filter-content">
        {children}
      </FilterBlock.Content>
    </FilterBlock>
  );
}
