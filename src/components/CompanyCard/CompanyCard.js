import Tooltip from 'components/Tooltip';
import * as React from 'react';

export default function CompanyCard({
  companyName,
  description,
  image,
  headingLevel = 2,
  url,
  alt,
  sponsor,
}) {
  const hasSponsor = Boolean(sponsor);
  const Heading = `h${headingLevel}`;
  return (
    <article className="company-card">
      <img className="company-card__image" src={image} alt={alt} />
      <div className="company-card__content">
        <Heading className="company-card__heading">
          <a href={url} className="company-card__link">
            {companyName}
          </a>
        </Heading>
        <p className="company-card__description" data-truncate="">
          {description}
        </p>
      </div>
      {hasSponsor && (
        <span className="company-card__sponsor">
          Sponsored solution by {sponsor}{' '}
          <Tooltip tooltipContent="More info about this advertisement" />
        </span>
      )}
    </article>
  );
}
