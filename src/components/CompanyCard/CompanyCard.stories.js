import faker from 'faker';
import React from 'react';

import CompanyCard from './CompanyCard';

const image = faker.image.imageUrl(300, 180);
const description = faker.lorem.sentence();

export default {
  title: 'General/Company Card',
  component: CompanyCard,
};

const Template = args => (
  <div style={{ width: '150px' }}>
    <CompanyCard {...args} />
  </div>
);

export const Story = Template.bind({});

Story.args = {
  alt: 'demo image',
  companyName: 'Bausch Advanced Technologies',
  description,
  image,
  url: '#',
};
