import * as React from 'react';

export default function Heading({ name, numberOfCompanies, description }) {
  return (
    <div className="category-heading">
      <h1 className="category-heading__name">{name}</h1>
      <p className="category-heading__results">
        &nbsp;({numberOfCompanies} companies)
      </p>
      <p className="category-heading__description">{description}</p>
    </div>
  );
}
