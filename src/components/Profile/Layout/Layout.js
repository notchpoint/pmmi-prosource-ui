import * as React from 'react';

import './styles.css';

function Layout({ children }) {
  return <div className="profile-layout">{children}</div>;
}

Layout.Main = ({ children }) => {
  return (
    <main role="main" className="profile-layout__main">
      {children}
    </main>
  );
};

Layout.Aside = ({ children }) => {
  return <aside className="profile-layout__aside">{children}</aside>;
};

export default Layout;
