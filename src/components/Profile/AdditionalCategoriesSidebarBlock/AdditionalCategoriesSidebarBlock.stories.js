import React from 'react';

import AdditionalCategories from './AdditionalCategoriesSidebarBlock';

export default {
  title: 'Pages/Profile Page/Sidebar/Additional Categories',
  component: AdditionalCategories,
};

const Template = args => <AdditionalCategories {...args} />;

export const Story = Template.bind({});

Story.args = {
  categoryLists: [
    ['Liquid Fillers', 'Gravity Fillers'],
    ['Cappers', 'Screw Cappers'],
  ],
};
