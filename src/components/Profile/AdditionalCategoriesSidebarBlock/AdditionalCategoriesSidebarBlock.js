import * as React from 'react';

import ChevronRightIcon from 'components/Icons/ChevronRightIcon';
import SidebarBlock from '../SidebarBlock';

export default function AdditionalCategoriesSidebarBlock({ categoryLists }) {
  return (
    <SidebarBlock heading="See additional companies in these categories:">
      {categoryLists.map((list, index) => {
        return (
          <ol className="additional-category-list" key={index}>
            {list.map((category, index) => {
              const isLastItem = index + 1 === list.length;
              return (
                <li className="additional-category-list__item" key={category}>
                  <a className="additional-category-list__link" href="#">
                    {category}
                  </a>
                  {!isLastItem && (
                    <ChevronRightIcon
                      className="additional-category-list__separator"
                      aria-hidden="true"
                    />
                  )}
                </li>
              );
            })}
          </ol>
        );
      })}
    </SidebarBlock>
  );
}
