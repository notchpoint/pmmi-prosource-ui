import * as React from 'react';

import CategoryCard from './CategoryCard';

export default {
  title: 'Pages/Profile Page/Category Card',
  component: CategoryCard,
  parameters: {
    viewport: {
      viewports: {
        profileDesktop: {
          name: 'Profile Desktop',
          styles: {
            width: '1200px',
            height: '900px',
          },
        },
      },
    },
  },
};

const Template = args => <CategoryCard {...args} />;

export const Story = Template.bind({});

Story.args = {
  name: 'Liquid Fillers',
  body: (
    <React.Fragment>
      <p>
        Gravity and pressure gravity bottle fillers feature neck handling
        technology designed for speed and efficiency on high-volume production
        of carbonated and other liquid filling lines. Fastest line fills up to
        900 glass or plastic bottles per minute.{' '}
      </p>
      <p>
        See the <a href="#">liquid fillers</a> page on this company’s website.
      </p>
      <h3>Types of liquid fillers</h3>
      <p>Gravity Filler</p>
    </React.Fragment>
  ),
  features: ['high speed', 'automatic', 'semi-automatic', 'robotic', 'rotary'],
  packages: [
    'ampoulesVials',
    'blisterPackBatteries',
    'cartonsGambletop',
    'drums',
    'blisterPackLaminate',
    'linedCase',
    'bagR2',
  ],
  imageUrl: 'profile/fillers.png',
};
