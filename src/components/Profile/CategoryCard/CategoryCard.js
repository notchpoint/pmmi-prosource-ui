import * as React from 'react';

import ChipList from 'components/ChipList';
import Image from 'components/Image';

import Feature from './Feature';

const iconsById = {
  ampoulesVials: {
    name: 'ampoules vials',
    url: '/profile/packages/ampoules-vials.png',
  },
  bagR2: {
    name: 'bag r2',
    url: '/profile/packages/bag-r2.png',
  },
  blisterPackBatteries: {
    name: 'blister pack batteries',
    url: '/profile/packages/blister-pack-batteries.png',
  },
  blisterPackLaminate: {
    name: 'blister pack laminate',
    url: '/profile/packages/blister-pack-laminate.png',
  },
  cartonsGambletop: {
    name: 'cartons gambletop',
    url: '/profile/packages/cartons-gambletop.png',
  },
  drums: {
    name: 'drums',
    url: '/profile/packages/drums.png',
  },
  linedCase: {
    name: 'lined cse',
    url: '/profile/packages/lined-case.png',
  },
};

// todo: image, icons
export default function CategoryCard({
  body,
  children,
  name,
  imageUrl,
  features,
  packages,
}) {
  return (
    <article className="category-card">
      <h2 className="category-card__heading">{name}</h2>
      <div className="category-card__container">
        {children}
        {imageUrl && body && (
          <div className="category-card__overview">
            <div className="category-card__image">
              <Image src={imageUrl} alt={name} />
            </div>
            <div className="category-card__body">{body}</div>
          </div>
        )}
        {features && packages && (
          <div className="category-card__features">
            <Feature>
              <Feature.Heading>Features:</Feature.Heading>
              <ChipList chips={features} />
            </Feature>
            <Feature>
              <Feature.Heading>Package Types:</Feature.Heading>
              <ul className="category-card-packages">
                {packages.map(id => {
                  const packageIcon = iconsById[id];
                  return (
                    <li key={id}>
                      <img
                        className="category-card-packages__package"
                        src={packageIcon.url}
                        alt=""
                      />
                      <span className="sr-only">{packageIcon.name}</span>
                    </li>
                  );
                })}
              </ul>
            </Feature>
          </div>
        )}
      </div>
    </article>
  );
}
