import * as React from 'react';

export default function Feature({ children }) {
  return <div className="category-feature">{children}</div>;
}

Feature.Heading = function ({ children }) {
  return <h3 className="category-feature__heading">{children}</h3>;
};
