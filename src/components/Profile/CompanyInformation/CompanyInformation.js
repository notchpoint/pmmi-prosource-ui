import clsx from 'clsx';
import * as React from 'react';

import EmailIcon from './assets/email-24px.svg';
import WebsiteIcon from './assets/language-24px.svg';
import PhoneIcon from './assets/phone-24px.svg';

import ChevronDown from 'components/Icons/ChevronDownIcon';

const companyName = 'Fogg Filler Co.';

export default function CompanyInformation() {
  const [showFullDescription, setShowFullDescription] = React.useState(false);
  const [showPhone, setShowPhone] = React.useState(false);

  const handleShowDescriptionClick = () => {
    setShowFullDescription(true);
  };

  const handleShowPhoneClick = () => {
    setShowPhone(true);
  };

  return (
    <section className="company-header" aria-labelledby="companyName">
      <h1 className="company-header__name" id="companyName">
        {companyName}
      </h1>
      <address className="company-header__address">
        3455 John F Donnelly Dr. • Holland, MI 49424 USA
      </address>
      <div className="company-header__image">
        <img src={'/profile/logo.svg'} alt={companyName} />
      </div>
      <ul className="company-header__links">
        <li className="block tablet:hidden">
          <a className="company-header__link" href="tel:1234567890">
            <PhoneIcon
              className="company-header__link-icon"
              aria-hidden="true"
            />
            <span>Call</span>
            <span className="sr-only">&nbsp;Company</span>
          </a>
        </li>
        <li className="hidden tablet:block">
          <button
            className={clsx('company-header__link', {
              'company-header__link--hidden': showPhone,
            })}
            onClick={handleShowPhoneClick}
            aria-expanded={showPhone}
            aria-controls="company-phone-link"
          >
            <PhoneIcon
              className="company-header__link-icon"
              aria-hidden="true"
            />
            <span>Show&nbsp;</span>
            <span className="sr-only profile-desktop:not-sr-only">
              Phone&nbsp;
            </span>
            <span>Number</span>
            <ChevronDown
              className="company-header__link-toggle"
              aria-hidden="true"
            />
          </button>
          <a
            className={clsx('company-header__phone-link', {
              'company-header__phone-link--hidden': !showPhone,
            })}
            href="tel:1234567890"
            id="company-phone-link"
          >
            <PhoneIcon
              className="company-header__link-icon"
              aria-hidden="true"
            />
            <span>123-456-7890</span>
          </a>
        </li>
        <li>
          <a href="mailto:bob26@example.net" className="company-header__link">
            <EmailIcon
              className="company-header__link-icon"
              aria-hidden="true"
            />
            Email
            <span className="sr-only profile-desktop:not-sr-only">
              &nbsp;Company
            </span>
          </a>
        </li>
        <li>
          <a href="#" className="company-header__link">
            <WebsiteIcon
              className="company-header__link-icon"
              aria-hidden="true"
            />
            <span className="sr-only tablet:not-sr-only">Visit&nbsp;</span>
            <span className="sr-only profile-desktop:not-sr-only">
              Company&nbsp;
            </span>
            <span>Website</span>
          </a>
        </li>
      </ul>
      <div
        className={clsx('company-header__description', {
          'company-header__description--expanded': showFullDescription,
        })}
      >
        <p>
          Specialists in liquid packaging applications, Fogg Filler's rotary
          filling, rinsing, capping, and enclosure systems can be custom
          engineered for dairy, water, food and beverage, pharmaceutical, and
          consumer product goods applications. Product offering also includes
          related machinery, such as bottle and cap sanitizing units, sorters,
          and air sanitizers. Refurbished used equipment and parts are available
          through its Spartan Parts division.
        </p>
        <div className="company-header__description-mask" />
        <button
          className="company-header__description-toggle"
          onClick={handleShowDescriptionClick}
        >
          <ChevronDown />
          <span className="sr-only">show description</span>
        </button>
      </div>
    </section>
  );
}
