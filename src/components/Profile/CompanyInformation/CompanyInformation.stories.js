import React from 'react';

import CompanyInformation from './CompanyInformation';

export default {
  title: 'Pages/Profile Page/Company Description Card',
  component: CompanyInformation,
  parameters: {
    viewport: {
      viewports: {
        profileDesktop: {
          name: 'Profile Desktop',
          styles: {
            width: '1200px',
            height: '900px',
          },
        },
      },
    },
  },
};

const Template = args => <CompanyInformation {...args} />;

export const Story = Template.bind({});

Story.args = {};
