import * as React from 'react';

import Button from 'components/Button';

export default function ContactCard() {
  const handleSubmit = () => {
    console.log('submitted test form!');
  };

  return (
    <form onSubmit={handleSubmit}>
      <fieldset className="contact-card">
        <legend className="contact-card__legend">Contact this Company</legend>
        <div className="contact-card__fields">
          <label className="sr-only" htmlFor="message">
            Contact Message
          </label>
          <textarea
            className="contact-card__text"
            id="message"
            placeholder="Ask your questions here..."
            aria-describedby="contactMessageDesc"
            required
          />
          <div id="contactMessageDesc" className="sr-only">
            This field is required.
          </div>
          <Button as="input" type="submit" value="submit" />
        </div>
      </fieldset>
    </form>
  );
}
