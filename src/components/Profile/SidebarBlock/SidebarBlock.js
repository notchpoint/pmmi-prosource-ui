import { useId } from '@reach/auto-id';
import * as React from 'react';

export default function SidebarBlock({ children, heading, name, ...props }) {
  const headingId = useId();

  return (
    <section className="sidebar-block" aria-labelledby={headingId} {...props}>
      <h3 className="sidebar-block__heading" id={headingId}>
        {heading}
      </h3>
      <div className="sidebar-block__content">{children}</div>
    </section>
  );
}

SidebarBlock.Paragraph = function ({ children }) {
  return <p className="sidebar-block__paragraph">{children}</p>;
};

SidebarBlock.CallToAction = function ({ children, ...props }) {
  return (
    <React.Fragment>
      &nbsp;
      <a className="sidebar-block__cta" {...props}>
        {children}
      </a>
    </React.Fragment>
  );
};
