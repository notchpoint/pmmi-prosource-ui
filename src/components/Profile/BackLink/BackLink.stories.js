import React from 'react';

import BackLink from './BackLink';

export default {
  title: 'Pages/Profile Page/Back Link',
  component: BackLink,
};

const Template = args => <BackLink {...args} />;

export const Default = Template.bind({});

Default.args = {
  text: 'Back to Fogg Filler',
  inverse: false,
};

export const Inverse = Template.bind({});

Inverse.args = {
  text: 'Back to Fogg Filler',
  inverse: true,
};
