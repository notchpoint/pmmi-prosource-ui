import clsx from 'clsx';
import * as React from 'react';

import ChevronLeftIcon from 'components/Icons/ChevronLeftIcon';

export default function BackLink({ inverse, text }) {
  return (
    <div className={clsx('profile-back', { 'profile-back--inverse': inverse })}>
      <div className="profile-container">
        <a href="#" className="profile-back__link">
          <ChevronLeftIcon className="profile-back__icon" aria-hidden="true" />
          {text}
        </a>
      </div>
    </div>
  );
}
