import * as React from 'react';

import CategoryCard from './CategoryCard';

export default {
  title: 'Pages/Parent Category Page/Parent Category Card',
  component: CategoryCard,
};

const Template = args => <CategoryCard {...args} />;

export const Story = Template.bind({});

Story.args = {
  name: 'Liquid Fillers',
  numberOfCompanies: 36,
  description:
    'Machinery that fills liquid product into a primary rigid container before closing, capping, or seaming it.',
  types: [
    {
      name: 'Aerosol & dispensing filling systems',
      description:
        'A filling machine that xyz and abc and lors and this is more text so that we can demonstrated truncated overflow',
      link: 'View 12 companies',
    },
    {
      name: 'Piston Fillers',
      description:
        'A machine that fills by a certain kind of process that the designer is not familiar with',
      link: 'View 18 companies',
    },
    {
      name: 'Gravity Fillers',
      description:
        'A machine that fills by a certain kind of process that the designer is not familiar with',
      link: 'View 25 companies',
    },
    {
      name: 'Pressure Fillers',
      description:
        'A machine that fills by a certain kind of process that the designer is not familiar with',
      link: 'View 15 companies',
    },
    {
      name: 'Vacuum Fillers',
      description:
        'A machine that fills by a certain kind of process that the designer is not familiar with',
      link: 'View 9 companies',
    },
    {
      name: 'Flow Meter Fillers',
      description:
        'A machine that fills by a certain kind of process that the designer is not familiar with',
      link: 'View 10 companies',
    },
  ],
};
