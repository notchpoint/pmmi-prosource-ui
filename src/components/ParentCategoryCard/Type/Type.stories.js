import * as React from 'react';

import Type from './Type';

export default {
  title: 'Pages/Parent Category Page/Parent Category Type',
  component: Type,
};

const Template = args => (
  <div style={{ width: '300px' }}>
    <Type {...args} />
  </div>
);

export const Story = Template.bind({});

Story.args = {
  name: 'Aerosol & dispensing filling systems',
  description:
    'A filling machine that xyz and abc and lors and this is more text so that we can demonstrated truncated overflow',
  link: 'View 12 companies',
};
