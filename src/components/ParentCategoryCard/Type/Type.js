import * as React from 'react';

import CtaLink from 'components/CtaLink';

export default function Type({ name, description, link }) {
  return (
    <div className="parent-category-type">
      <div className="parent-category-type__overview">
        <h4 className="parent-category-type__heading">{name}</h4>
        <p className="parent-category-type__description" data-truncate="">
          {description}
        </p>
      </div>
      <div
        className="parent-category-type__action"
        data-truncate=""
        data-truncate-letter=""
      >
        <CtaLink>{link}</CtaLink>
      </div>
    </div>
  );
}
