import { useId } from '@reach/auto-id';
import faker from 'faker';
import * as React from 'react';

import CtaLink from 'components/CtaLink';

import Type from './Type';

export default function CategoryCard({
  headingLevel = 2,
  name,
  numberOfCompanies,
  description,
  types,
}) {
  const imageUrl = faker.image.image(181, 143);
  const typesHeadingId = useId();
  const headingId = useId();
  const descriptionId = useId();

  const Heading = `h${headingLevel}`;
  const TypesHeading = `h${headingLevel}`;
  return (
    <div
      className="parent-category-card"
      aria-labelledby={headingId}
      aria-describedby={descriptionId}
    >
      <div className="parent-category-card__overview">
        <img
          className="parent-category-card__image"
          src={imageUrl}
          alt={name}
        />
        <div>
          <a className="parent-category-card__heading-link" href="#">
            <Heading className="parent-category-card__name" id={headingId}>
              {name}
            </Heading>
            <p className="parent-category-card__results">
              &nbsp;({numberOfCompanies} companies)
            </p>
          </a>
          <p className="parent-category-card__description" id={descriptionId}>
            {description}
          </p>
          <p className="mt-16">
            <CtaLink>
              View {numberOfCompanies} <span className="sr-only">{name}</span>{' '}
              Companies
            </CtaLink>
          </p>
        </div>
      </div>
      <div className="parent-category-card__types">
        <TypesHeading
          className="parent-category-card__types-heading"
          id={typesHeadingId}
        >
          {name} Types
        </TypesHeading>
        <ul
          aria-labelledby={typesHeadingId}
          className="gap-16 grid grid-cols-12 tablet:gap-24"
        >
          {types.map(type => {
            return (
              <li
                className="col-span-6 sm-desktop:col-span-3 tablet:col-span-4"
                key={type.name}
              >
                <Type {...type} />
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
}
