import React from 'react';

import Chip from './Chip';

export default {
  title: 'General/Chip',
  component: Chip,
};

const Template = ({ text, ...args }) => <Chip {...args}>{text}</Chip>;

export const Story = Template.bind({});

Story.args = {
  isAltColor: false,
  text: 'Chip',
};
