import clsx from 'clsx';
import * as React from 'react';

export default function Chip({ children, isAltColor = false }) {
  return (
    <span className={clsx('chip', { 'chip--alt': isAltColor })}>
      {children}
    </span>
  );
}
