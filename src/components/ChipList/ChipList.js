import * as React from 'react';

import Chip from 'components/Chip';

export default function ChipList({ chips, isAltColor }) {
  return (
    <ul className="chip-list">
      {chips.map(chip => (
        <li className="chip-list__item" key={chip}>
          <Chip isAltColor={isAltColor}>{chip}</Chip>
        </li>
      ))}
    </ul>
  );
}
