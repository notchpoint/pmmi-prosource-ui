import clsx from 'clsx';
import * as React from 'react';

import ChevronRightIcon from 'components/Icons/ChevronRightIcon';
import CloseIcon from 'components/Icons/CloseIcon';

import './styles.css';

export function Sidebar({ children, subheading }) {
  return (
    <div
      data-mobile-sidebar=""
      className="mobile-sidebar"
      id="sidebar"
      role="region"
    >
      <div className="mobile-sidebar__content">
        {/* <h2 className="mobile-sidebar__heading">Browse By</h2>
        {subheading && (
          <h3 className="mobile-sidebar__subheading">{subheading}</h3>
        )} */}
        {children}
      </div>
      <button
        className="mobile-sidebar__toggle"
        data-mobile-sidebar-toggle=""
        aria-expanded="true"
      >
        <CloseIcon className="mobile-sidebar__toggle-icon" />
      </button>
    </div>
  );
}

export function Button({ children, className }) {
  return (
    <button
      data-mobile-sidebar-toggle=""
      className={clsx('mobile-sidebar-trigger', className)}
      aria-haspopup="true"
      aria-controls="sidebar"
      aria-expanded="false"
    >
      {children}
      <ChevronRightIcon
        className="mobile-sidebar-trigger__icon"
        aria-hidden="true"
      />
    </button>
  );
}

export function SearchButton({ children, className, ...props }) {
  return (
    <button className={clsx('mobile-sidebar-search', className)} {...props}>
      {children}
    </button>
  );
}

export function Heading({ label = 'Filter By' }) {
  return (
    <div className="mobile-sidebar-heading">
      <h2 className="mobile-sidebar-heading__label">{label}</h2>
      <button className="mobile-sidebar-heading__reset" type="button">
        Reset Search
      </button>
    </div>
  );
}

export function GlossarySection() {
  return (
    <p className="mobile-sidebar-glossary">
      Need definitions of our filters? View our <a href="#">glossary</a>.
    </p>
  );
}
