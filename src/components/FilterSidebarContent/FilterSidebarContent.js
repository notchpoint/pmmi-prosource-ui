import * as React from 'react';

import CategoryFilter from 'components/CategoryFilter';
import ParentFilter from 'components/ParentFilter';
import {
  GlossarySection,
  Heading,
  SearchButton,
} from 'components/MobileSidebar';
import ChildFilter from 'components/Search/ChildFilter';
import SuggestionChip from 'components/Search/SuggestionChip';
import Checkbox from 'components/Checkbox';

const SUGGESTIONS = [
  'Container cleaning/rinsing',
  'Container unscrambling',
  'Tray/cup packaging equipment',
];

export default function FilterSidebarContent({ isMobile = false }) {
  return (
    <div className="filter-sidebar-content">
      <div className="border-between">
        <div className="sidebar-related">
          <h2 className="sidebar-related__heading">Related to your search</h2>
          <ul className="sidebar-related__list" aria-label="related searches">
            {SUGGESTIONS.map(suggestion => (
              <li className="sidebar-related__list-item" key={suggestion}>
                <SuggestionChip suggestion={suggestion} />
              </li>
            ))}
          </ul>
        </div>
        <Heading />
        {/** Empty div container fixes browser overriding fieldset top border */}
        <div>
          <CategoryFilter
            isExpanded={!isMobile}
            name="Product Type"
            options={[
              { name: 'Non-viscous liquids', resultCount: 10 },
              { name: 'Viscous liquids', resultCount: 4 },
              { name: 'Liquids with particulates', resultCount: 2 },
              { name: 'Caustic/volatile chemicals', resultCount: 11 },
            ]}
          />
        </div>
        <div>
          <CategoryFilter
            isExpanded={!isMobile}
            name="Machine Type"
            options={[
              { name: 'Inline', resultCount: 8 },
              { name: 'Rotary', resultCount: 4 },
              { name: 'Monoblock', resultCount: 6 },
              { name: 'Rigid container form/fill/seal', resultCount: 7 },
            ]}
          />
        </div>
        <div>
          <CategoryFilter
            isExpanded={!isMobile}
            name="Fill Type"
            options={[
              { name: 'Volumetric', resultCount: 8 },
              { name: 'Fill by time', resultCount: 5 },
              { name: 'Fill by weight', resultCount: 2 },
              { name: 'Positive displacement', resultCount: 7 },
              { name: 'Hot fill', resultCount: 2 },
              { name: 'Compensator filling', resultCount: 3 },
            ]}
          />
        </div>
        <div>
          <CategoryFilter
            isExpanded={!isMobile}
            name="Certifications &amp; Sanitation"
            options={[
              { name: 'CIP', resultCount: 2 },
              { name: 'Washdown', resultCount: 2 },
              { name: 'Dairy 3A', resultCount: 1 },
              { name: 'USDA', resultCount: 8 },
              { name: 'Hygienic', resultCount: 10 },
            ]}
          />
        </div>
        <div>
          <CategoryFilter
            isExpanded={!isMobile}
            name="Shelf Life"
            options={[
              { name: 'Extended shelf life', resultCount: 4 },
              { name: 'Aseptic', resultCount: 1 },
              { name: 'Dosing (propellant or liquid gas)', resultCount: 3 },
              { name: 'Controlled atomospheric packaging', resultCount: 8 },
              { name: 'Modified atmospheric packaging', resultCount: 1 },
            ]}
          />
        </div>
        <div>
          <CategoryFilter
            isExpanded={!isMobile}
            name="Level of Automation"
            options={[
              { name: 'Automatic', resultCount: 13 },
              { name: 'Semi-automatic', resultCount: 8 },
              { name: 'Manual', resultCount: 4 },
            ]}
          />
        </div>
        <div>
          <ParentFilter name="Industry" isCollapsible>
            <ChildFilter fieldsId="foodFields" name="Food">
              <Checkbox
                name="foodFields"
                option={{ name: 'Bakery/Snack', resultCount: 20 }}
              />
              <Checkbox
                name="foodFields"
                option={{ name: 'Cereal, Breakfast Foods', resultCount: 12 }}
              />
              <Checkbox
                name="foodFields"
                option={{ name: 'Confection/Candy', resultCount: 10 }}
              />
            </ChildFilter>
          </ParentFilter>
        </div>
      </div>
      <SearchButton>Done</SearchButton>
      <GlossarySection />
    </div>
  );
}
