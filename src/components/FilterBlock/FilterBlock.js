import clsx from 'clsx';
import * as React from 'react';

import ChevronUpIcon from 'components/Icons/ChevronUpIcon';

const FilterContext = React.createContext();

export default function FilterBlock({
  as: Component = 'div',
  children,
  isCollapsible = true,
  isExpanded = true,
  fieldsId,
  name,
}) {
  return (
    <FilterContext.Provider
      value={{ isCollapsible, name, isExpanded, fieldsId }}
    >
      <Component
        className={clsx('filter-block', {
          'filter-block--collapsed': !isExpanded,
        })}
        data-filter-block=""
      >
        {children}
      </Component>
    </FilterContext.Provider>
  );
}

FilterBlock.Trigger = function ({ as: Component = 'div' }) {
  const { fieldsId, isCollapsible, isExpanded, name } = React.useContext(
    FilterContext,
  );

  if (!isCollapsible) {
    return (
      <Component className="filter-block__legend">
        <span className="filter-block__legend-name">{name}</span>
      </Component>
    );
  }

  return (
    <Component className="filter-block__legend">
      <button
        aria-controls={fieldsId}
        aria-expanded={isExpanded}
        data-filter-block-toggle=""
      >
        {name}
        <ChevronUpIcon className="filter-block__toggle-icon" />
      </button>
    </Component>
  );
};

FilterBlock.Content = function ({ children, className }) {
  const { fieldsId, isCollapsible } = React.useContext(FilterContext);
  return (
    <div
      className="filter-block__body"
      id={isCollapsible ? fieldsId : undefined}
      role={isCollapsible ? 'region' : undefined}
    >
      <div className={className}>{children}</div>
    </div>
  );
};
