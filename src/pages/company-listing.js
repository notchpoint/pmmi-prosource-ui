import faker from 'faker';
import * as React from 'react';
import Helmet from 'react-helmet';

import CompanyCard from 'components/CompanyCard';
import AdditionalCompanies from 'components/CompanyListing/AdditionalCompanies';
import FilterLayout from 'components/FilterLayout';
import FilterSidebarContent from 'components/FilterSidebarContent';
import {
  Button as MobileSidebarButton,
  Sidebar as MobileSidebar,
} from 'components/MobileSidebar';
import Page from 'components/Page';
import Heading from 'components/ParentCategory/Heading';
import RelatedSearches from 'components/CompanyListing/RelatedSearches';
import SponsoredListings from 'components/CompanyListing/SponsoredListings';
import SelectedFilterChip from 'components/Search/SelectedFilterChip';

// https://www.sketch.com/s/d1bf1b62-abec-4624-a81f-94668addb2d8/a/09paeKy
function CompanyListingPage() {
  return (
    <Page isShaded>
      <Helmet>
        <html lang="en" />
        <title>Gravity Fillers | PMMI ProSource</title>
      </Helmet>
      <FilterLayout isExpanded>
        <FilterLayout.Aside isExpanded>
          <FilterSidebarContent />
        </FilterLayout.Aside>
        <FilterLayout.Content withBreadcrumbs>
          <Page.Breadcrumbs
            links={[
              'Home',
              'Filling & Closing',
              'Liquid Fillers',
              'Gravity Fillers',
            ]}
          />
          <SelectedFilterChip.List>
            <SelectedFilterChip.ListItem>
              <SelectedFilterChip name="Caustic/volatile chemicals" />
            </SelectedFilterChip.ListItem>
            <SelectedFilterChip.ListItem>
              <SelectedFilterChip name="Inline" />
            </SelectedFilterChip.ListItem>
            <SelectedFilterChip.ListItem>
              <SelectedFilterChip name="Cups, Tubs, & Bowls" />
            </SelectedFilterChip.ListItem>
          </SelectedFilterChip.List>
          <Heading
            name="Gravity Fillers"
            numberOfCompanies={25}
            description="A closing machine that applies a threaded cap or lid, usually to a rigid container. Read more about filling and closing equipment here. "
          />
          <MobileSidebarButton className="mb-32">filter</MobileSidebarButton>
          <MobileSidebar subheading="Package Type">
            <FilterSidebarContent isMobile />
          </MobileSidebar>
          <ul className="company-listing-list">
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="Bausch Advanced Technologies"
                description="Product offering of inline, monobloc, and integrated filling equipment is targeted to primarily pharmaceutical liquid filling operations. Includes single- and."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="Berks Plant Design & Maintenance Inc."
                description="Co. distributes and integrates a wide variety of used pressure overflow and pressure gravity liquid fillers engineered specifically for liquids with low to medium."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="Coesia"
                description="Designed to handle disinfectants, gels, alcohol, cleaning products and other household and industrial products is the MBL Series, which includes the MCSP servo piston and gravity filler, MFX cappers."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="Cozzoli Machine Company"
                description="Product line includes a wide variety of gravity and pressure/gravity fillers suitable for bottling any water-thin to medium, consistent viscosity liquids. Gravity fillers are engineered."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="E-PAK Machinery"
                description="Product line includes a wide variety of gravity and pressure/gravity fillers suitable for bottling any water-thin to medium, consistent viscosity liquids. Gravity fillers are engineered for thin, foamy."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="ESS Technologies"
                description="Company’s filling machinery offering includes an aseptic syringe filling and capping system integrating FANUC clean class robots with OEM-supplied RABS to create a sterile environment for."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={2}
                alt="demo image"
                companyName="Filmatic"
                description="Product offering of inline, monobloc, and integrated filling equipment is targeted to primarily pharmaceutical liquid filling operations. Includes single- and multi-nozzle fillers, gravity."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
          </ul>
          <div className="-mx-32 mt-72">
            <SponsoredListings />
          </div>
          <h2 className="results-subheading">
            The following companies can incorporate{' '}
            <span className="results-subheading__keyword">Gravity Fillers</span>{' '}
            into their equipment
          </h2>
          <ul className="company-listing-list">
            <li>
              <CompanyCard
                headingLevel={3}
                alt="demo image"
                companyName="Aspacks"
                description="Specializing in filling machinery for the food, cosmetic, pharmaceutical, and chemical industries, company’s line of peristaltic pump and gravity and pump-feed overflow."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={3}
                alt="demo image"
                companyName="Flexicon"
                description="Product offering of inline, monobloc, and integrated filling equipment is targeted to primarily pharmaceutical liquid filling operations. Includes single- and multi-nozzle fillers, gravity ..."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={3}
                alt="demo image"
                companyName="Inline Filling Systems"
                description="Company’s filling machinery offering includes an aseptic syringe filling and capping system integrating FANUC clean class robots with OEM."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
            <li>
              <CompanyCard
                headingLevel={3}
                alt="demo image"
                companyName="Intellitech, Inc."
                description="Specializing in filling machinery for the food, cosmetic, pharmaceutical, and chemical industries, company’s line of peristaltic pump and gravity and pump-feed overflow  ..."
                image={faker.image.image(165, 125)}
                url="#"
              />
            </li>
          </ul>
          <div className="mt-72">
            <AdditionalCompanies
              list={[
                'Asceptic Filling Equipment & Packaging Machines, Inc.',
                'Chase-Logeman',
                'Kaps-All Packaging Systems',
                'M & O Perry Industries, Inc.',
                'Neumann Packaging Services LLC',
                'Optima',
                'Pope Scientific Inc',
              ]}
            />
          </div>
          <div className="border-t border-neutral-400 mt-72 pt-32">
            <RelatedSearches
              list={[
                'Aerosol & Dispensing Filling Systems',
                'Piston Fillers',
                'Vacuum Fillers',
                'Pressure Fillers',
                'Flow Meter Fillers',
              ]}
              type="Liquid Fillers"
            />
          </div>
          <div className="border-t border-neutral-400 mt-72 pt-24">
            <p>
              The companies represented on this page and throughout this
              directory are members in good standing of PMMI, the Association
              for Packaging & Processing Technologies. Companies who are not
              members of PMMI are not listed in this directory.
            </p>
          </div>
        </FilterLayout.Content>
      </FilterLayout>
    </Page>
  );
}

export default CompanyListingPage;
