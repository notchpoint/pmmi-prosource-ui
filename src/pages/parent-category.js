import * as React from 'react';
import Helmet from 'react-helmet';

import FilterLayout from 'components/FilterLayout';
import FilterSidebarContent from 'components/FilterSidebarContent';
import {
  Button as MobileSidebarButton,
  Sidebar as MobileSidebar,
} from 'components/MobileSidebar';
import Page from 'components/Page';
import Heading from 'components/ParentCategory/Heading';
import ParentCategoryCard from 'components/ParentCategoryCard';

function ParentCategoryPage() {
  return (
    <Page isShaded>
      <Helmet>
        <html lang="en" />
        <title>
          Filling & Closing Equipment (132 companies) | PMMI ProSource
        </title>
      </Helmet>
      <FilterLayout>
        <FilterLayout.Aside>
          <FilterSidebarContent />
        </FilterLayout.Aside>
        <FilterLayout.Content withBreadcrumbs>
          <Page.Breadcrumbs links={['Home', 'Filling & Closing']} />
          <Heading
            name="Filling & Closing Equipment"
            numberOfCompanies={132}
            description="Machinery that fills dry or liquid product into a primary rigid
        container before closing, capping, or seaming it."
          />
          <MobileSidebarButton>toggle sidebar</MobileSidebarButton>
          <MobileSidebar subheading="Package Type">
            <FilterSidebarContent isMobile />
          </MobileSidebar>
          <div className="mt-40 mb-72">
            <ParentCategoryCard
              name="Liquid Fillers"
              numberOfCompanies={36}
              description="Machinery that fills liquid product into a primary rigid container before closing, capping, or seaming it."
              types={[
                {
                  name: 'Aerosol & dispensing filling systems',
                  description:
                    'A filling machine that xyz and abc and lors and this is more text so that we can demonstrated truncated overflow',
                  link: 'View 12 companies',
                },
                {
                  name: 'Piston Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 18 companies',
                },
                {
                  name: 'Gravity Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 25 companies',
                },
                {
                  name: 'Pressure Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 15 companies',
                },
                {
                  name: 'Vacuum Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 9 companies',
                },
                {
                  name: 'Flow Meter Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 10 companies',
                },
              ]}
            />
            <ParentCategoryCard
              name="Dry Fillers"
              numberOfCompanies={45}
              description="Machinery that fills liquid product into a primary rigid container before closing, capping, or seaming it."
              types={[
                {
                  name: 'Auger Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 18 companies',
                },
                {
                  name: 'Piece Counters',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 18 companies',
                },
                {
                  name: 'Table Counters',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 14 companies',
                },
                {
                  name: 'Combination Scale',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 10 companies',
                },
                {
                  name: 'Vibratory Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 8 companies',
                },
                {
                  name: 'Net Weight Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 11 companies',
                },
                {
                  name: 'Pocket Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 13 companies',
                },
                {
                  name: 'Loss in Weight Fillers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 5 companies',
                },
              ]}
            />
            <ParentCategoryCard
              name="Cappers"
              numberOfCompanies={22}
              description="Machine that closes a container, usually a rigid container."
              types={[
                {
                  name: 'Crown Capping Machines',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 14 companies',
                },
                {
                  name: 'Roll-on Capping Machines',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 8 companies',
                },
                {
                  name: 'Press-on Capping Machines',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 10 companies',
                },
                {
                  name: 'Screw Capping Machines',
                  description:
                    'A closing machine that applies a threaded cap or lid, usually to a rigid',
                  link: 'View 18 companies',
                },
                {
                  name: 'Overcapping Machines',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 14 companies',
                },
                {
                  name: 'Retorquers',
                  description:
                    'A machine that fills by a certain kind of process that the designer is not familiar with',
                  link: 'View 6 companies',
                },
              ]}
            />
          </div>
        </FilterLayout.Content>
      </FilterLayout>
    </Page>
  );
}

export default ParentCategoryPage;
