import * as React from 'react';

import Page from 'components/Page';

function HomePage() {
  return (
    <Page>
      <Page.Content className="mb-72">
        <h1 className="font-bold my-24 text-h1">Design implementations</h1>
        <h2 className="text-h2">Profile</h2>
        <ol className="list-disc list-inside pl-40 mt-24 text-h2">
          <li className="mb-8">
            ☑️ <a href="/home">Home</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/company-listing">Company Listing</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/parent-company">Parent Company</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/parent-category">Parent Category</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/profile">Profile</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/profile-image-bg">Profile with Image Background</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/search-results">Search Results</a>
          </li>
          <li className="mb-8">
            ☑️ <a href="/no-search-results">No Search Results</a>
          </li>
          <li>
            ☑️ <a href="/404">404</a>
          </li>
        </ol>
      </Page.Content>
    </Page>
  );
}

export default HomePage;
