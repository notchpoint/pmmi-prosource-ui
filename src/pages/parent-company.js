import faker from 'faker';
import * as React from 'react';
import Helmet from 'react-helmet';

import ChipList from 'components/ChipList';
import Page from 'components/Page';
import AdditionalCategoriesSidebarBlock from 'components/Profile/AdditionalCategoriesSidebarBlock';
import BackLink from 'components/Profile/BackLink';
import CategoryCard from 'components/Profile/CategoryCard';
import CompanyInformation from 'components/Profile/CompanyInformation';
import Layout from 'components/Profile/Layout';
import SidebarBlock from 'components/Profile/SidebarBlock';
import ParentCompanyCard from 'components/ParentCompany/ParentCompanyCard';

const COMPANIES = getChildCompanies();

function ParentCompanyPage({ withImageBg = false }) {
  return (
    <Page withImageBg={withImageBg}>
      <Helmet>
        <html lang="en" />
        <title>ProMach | PMMI ProSource</title>
      </Helmet>
      <BackLink text="Back to Gravity Fillers" inverse={withImageBg} />
      <div className="profile-container" id="main-content">
        <CompanyInformation />
        <ul
          aria-label="List of child companies."
          className="grid grid-flow-row grid-cols-2 gap-x-16 gap-y-32 mt-8 sm-desktop:grid-cols-3 tablet:gap-x-32"
        >
          {COMPANIES.map(company => {
            return (
              <li>
                <ParentCompanyCard key={company.name} {...company} />
              </li>
            );
          })}
        </ul>
        <div className="my-72">
          <IssuesSidebarBlock />
        </div>
      </div>
    </Page>
  );
}

function IssuesSidebarBlock(props) {
  return (
    <SidebarBlock name="issues" heading="Report an Issue" {...props}>
      <SidebarBlock.Paragraph>
        Incorrect/inaccurate categorization? Broken link?
      </SidebarBlock.Paragraph>
      <SidebarBlock.CallToAction href="#">
        Report inaccuracies or problems
      </SidebarBlock.CallToAction>
    </SidebarBlock>
  );
}

function getChildCompanies() {
  return [
    {
      name: 'Allpax',
      description: 'Product & Package Handling Equipment',
      image: faker.image.image(200, 50),
    },
    {
      name: 'Axon',
      description: 'Wrapping Equipment',
      image: faker.image.image(200, 50),
    },
    {
      name: 'CodeTech',
      description: 'Bagging & Pouching Equipment, Cartoning Equipment',
      image: faker.image.image(200, 50),
    },
    {
      name: 'Benchmark',
      description:
        'Product & Package Handling Equipment, Conveyors, Feeding & Inserting Equipment',
      image: faker.image.image(200, 50),
    },
    {
      name: 'Brenton',
      description:
        'Case Packing Equipment, Robots, End-of-Arm Tooling, Palletizing, Pallet Stabilization',
      image: faker.image.image(200, 50),
    },
    {
      name: 'CodeTech',
      description: 'Coding & Marking Equipment, Labeling Equipment',
      image: faker.image.image(200, 50),
    },
  ];
}

export default ParentCompanyPage;
