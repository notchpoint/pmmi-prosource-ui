import { useId } from '@reach/auto-id';
import * as React from 'react';
import Helmet from 'react-helmet';

import FilterLayout from 'components/FilterLayout';
import FilterSidebarContent from 'components/Home/FilterSidebarContent';
import {
  Button as MobileSidebarButton,
  Sidebar as MobileSidebar,
} from 'components/MobileSidebar';
import Page from 'components/Page';
import CategoryCard from 'components/Home/CategoryCard';
import Heading from 'components/Home/Heading';

const CATEGORIES = getCategories();

function ParentCategoryPage() {
  const headingId = useId();
  return (
    <Page isShaded>
      <Helmet>
        <html lang="en" />
        <title>
          Browse Companies by Equipment, Materials &amp; Services | PMMI
          ProSource
        </title>
      </Helmet>
      <FilterLayout isExpanded>
        <FilterLayout.Aside>
          <FilterSidebarContent />
        </FilterLayout.Aside>
        <FilterLayout.Content>
          <Heading prefix="Browse companies by" id={headingId}>
            Equipment, Materials & Services
          </Heading>
          <MobileSidebarButton>Browse by package type</MobileSidebarButton>
          <MobileSidebar>
            <FilterSidebarContent isMobile />
          </MobileSidebar>
          <ul aria-labelledby={headingId} className="home-category-list">
            {CATEGORIES.map(category => {
              return (
                <li className="home-category-list__item" key={category.name}>
                  <CategoryCard {...category} />
                </li>
              );
            })}
          </ul>
        </FilterLayout.Content>
      </FilterLayout>
    </Page>
  );
}

export default ParentCategoryPage;

function getCategories() {
  return [
    {
      name: 'Processing Equipment',
      count: 132,
      tooltip: 'This is some more information',
    },
    {
      name: 'Filing & Closing Equipment',
      count: 132,
      tooltip: 'This is some more information',
    },
    {
      name: 'Bagging, Pouching & Wrapping Equipment',
      count: 246,
      tooltip: 'This is some more information',
    },
    {
      name: 'Tray, Clamshell, & Blister Packaging Equipment',
      count: 87,
      tooltip: 'This is some more information',
    },
    {
      name: 'Conveying, Feeding & Handling',
      count: 92,
      tooltip: 'This is some more information',
    },
    {
      name: 'Coding, Labeling & Printing',
      count: 295,
      tooltip: 'This is some more information',
    },
    {
      name: 'Inspection & Testing',
      count: 73,
      tooltip: 'This is some more information',
    },
    {
      name: 'Carton, Multipack & Case Packing',
      count: 311,
      tooltip: 'This is some more information',
    },
    {
      name: 'Robot & End-of-Arm Tooling Manufacturers',
      count: 38,
      tooltip: 'This is some more information',
    },
    {
      name: 'Specialty Equipment',
      count: 295,
      tooltip: 'This is some more information',
    },
    {
      name: 'Palletizing & Pallet Operations',
      count: 104,
      tooltip: 'This is some more information',
    },
    {
      name: 'Material Handling & Warehousing',
      count: 57,
      tooltip: 'This is some more information',
    },
    {
      name: 'Converting & Package Manufacturing Equipment',
      count: 42,
      tooltip: 'This is some more information',
    },
    {
      name: 'Materials, Containers & Consumables',
      count: 311,
      tooltip: 'This is some more information',
    },
    {
      name: 'Controls, Software & Components',
      count: 109,
      tooltip: 'This is some more information',
    },
    {
      name: 'Plant Facilities, Infrastructure & Operations',
      count: 85,
      tooltip: 'This is some more information',
    },
    {
      name: 'Professional & Outside Services',
      count: 59,
      tooltip: 'This is some more information',
    },
  ];
}
