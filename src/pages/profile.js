import * as React from 'react';
import Helmet from 'react-helmet';

import ChipList from 'components/ChipList';
import Page from 'components/Page';
import AdditionalCategoriesSidebarBlock from 'components/Profile/AdditionalCategoriesSidebarBlock';
import BackLink from 'components/Profile/BackLink';
import CategoryCard from 'components/Profile/CategoryCard';
import CompanyInformation from 'components/Profile/CompanyInformation';
import ContactCard from 'components/Profile/ContactCard';
import Layout from 'components/Profile/Layout';
import SidebarBlock from 'components/Profile/SidebarBlock';

function ProfilePage({ withImageBg = false }) {
  return (
    <Page withImageBg={withImageBg}>
      <Helmet>
        <html lang="en" />
        <title>Fogg Filler Co. | PMMI ProSource</title>
      </Helmet>
      <BackLink text="Back to Gravity Filler" inverse={withImageBg} />
      <div className="profile-container" id="main-content">
        <CompanyInformation />
        <Layout>
          <Layout.Main
            role="main"
            className="col-span-12 sm-desktop:col-span-8"
          >
            <CategoryCard
              name="Liquid Fillers"
              body={
                <React.Fragment>
                  <p>
                    Gravity and pressure gravity bottle fillers feature neck
                    handling technology designed for speed and efficiency on
                    high-volume production of carbonated and other liquid
                    filling lines. Fastest line fills up to 900 glass or plastic
                    bottles per minute.{' '}
                  </p>
                  <p>
                    See the <a href="#">liquid fillers</a> page on this
                    company’s website.
                  </p>
                  <h3>Types of liquid fillers</h3>
                  <p>Gravity Filler</p>
                </React.Fragment>
              }
              features={[
                'high speed',
                'automatic',
                'semi-automatic',
                'robotic',
                'rotary',
              ]}
              packages={[
                'ampoulesVials',
                'blisterPackBatteries',
                'cartonsGambletop',
                'drums',
                'blisterPackLaminate',
                'linedCase',
                'bagR2',
              ]}
              imageUrl={'/profile/fillers.png'}
            />
            <CategoryCard
              name="Cappers"
              body={
                <React.Fragment>
                  <p>
                    Using multiple turrets, capping equipment handles various
                    cap sizes, styles, and varieties, such as flat screw, sport,
                    snap, and snap screw closures -- all on a single machine.
                  </p>
                  <p>
                    See the <a href="#">cappers</a> page on this company’s
                    website.
                  </p>
                  <h3>Types of Cappers</h3>
                  <p>
                    Crown Capping Machine, Overcapping Machine, Screw Capping
                    Machine
                  </p>
                </React.Fragment>
              }
              features={[
                'high speed',
                'low speed',
                'manual',
                'semi-automatic',
                'automatic',
                'usda',
              ]}
              packages={['ampoulesVials', 'blisterPackLaminate']}
              imageUrl={'/profile/cappers.png'}
            />
            <CategoryCard
              name="Additional Offerings"
              body={
                <React.Fragment>
                  <p>
                    Gravity and pressure gravity bottle fillers feature neck
                    handling technology designed for speed and efficiency on
                    high-volume production of carbonated and other liquid
                    filling lines. Fastest line fills up to 900 glass or plastic
                    bottles per minute.
                  </p>
                  <p>
                    See the <a href="#">additional offerings</a> on the
                    company’s website.
                  </p>
                </React.Fragment>
              }
              imageUrl={'/profile/gravity-filler.png'}
            />
          </Layout.Main>
          <Layout.Aside className="col-span-12 sm-desktop:col-span-4">
            <div className="hidden sm-desktop:block">
              <h2 className="sr-only">More Information about Fogg Filler</h2>
              {/* <ContactCard /> */}
              <OverviewSidebarBlock />
              <ChipsSidebarBlock />
              <SustainabilitySidebarBlock />
              <ResearchSidebarBlock />
              <IssuesSidebarBlock />
              <AdditionalCategoriesSidebarBlock
                categoryLists={[
                  ['Liquid Fillers', 'Gravity Fillers'],
                  ['Cappers', 'Screw Cappers'],
                ]}
              />
            </div>
            <div className="mobile-sidebar-card-content">
              <CategoryCard name="More Information about Fogg Filter">
                <OverviewSidebarBlock />
                <ChipsSidebarBlock />
                <SustainabilitySidebarBlock />
                <ResearchSidebarBlock />
              </CategoryCard>
              <IssuesSidebarBlock />
              <AdditionalCategoriesSidebarBlock
                categoryLists={[
                  ['Liquid Fillers', 'Gravity Fillers'],
                  ['Cappers', 'Screw Cappers'],
                ]}
              />
            </div>
          </Layout.Aside>
        </Layout>
      </div>
    </Page>
  );
}

function IssuesSidebarBlock(props) {
  return (
    <SidebarBlock name="issues" heading="Report an Issue" {...props}>
      <SidebarBlock.Paragraph>
        Incorrect/inaccurate categorization? Broken link?
      </SidebarBlock.Paragraph>
      <SidebarBlock.CallToAction href="#">
        Report inaccuracies or problems
      </SidebarBlock.CallToAction>
    </SidebarBlock>
  );
}

function ResearchSidebarBlock(props) {
  return (
    <SidebarBlock name="research" heading="Continue Your Research" {...props}>
      <SidebarBlock.Paragraph>
        This company has videos, case studies, products and articles posted to
        our sister site Packaging World.
      </SidebarBlock.Paragraph>
      <SidebarBlock.CallToAction href="#">
        Explore additional content
      </SidebarBlock.CallToAction>
    </SidebarBlock>
  );
}

function SustainabilitySidebarBlock(props) {
  return (
    <SidebarBlock name="sustainability" heading="Sustainability" {...props}>
      <p>
        Fogg Filler is committed to the advancement of sustainbility efforts in
        the packaging community. We believe that lorem ipsum dolor sit amet.
        Fogg Filler is committed to the advancement of sustainbility efforts in
        the packaging community. We believe that lorem ipsum dolor sit amet.
      </p>
    </SidebarBlock>
  );
}

function ChipsSidebarBlock(props) {
  return (
    <SidebarBlock name="industries" heading="Industries" {...props}>
      <ChipList
        chips={[
          'beer',
          'soft drinks',
          'drinks, non-alcoholic',
          'water',
          'juice',
          'tea',
          'personal care',
          'pharmaceuticals',
        ]}
        isAltColor
      />
    </SidebarBlock>
  );
}

function OverviewSidebarBlock(props) {
  return (
    <SidebarBlock name="overview" heading="At-A-Glance" {...props}>
      <h3 className="font-bold inline">Years in business:</h3>
      <p className="inline ml-4">since 1956</p>
      <br />
      <h3 className="font-bold inline">Number of employees:</h3>
      <p className="inline ml-4">170</p>
      <br />
      <h3 className="font-bold inline">Support:</h3>
      <p className="inline ml-4">
        After-hours hotline for emergencies and department personnel and
        certification program.
      </p>
      <br />
      <h3 className="font-bold inline">Major Integration Partners:</h3>
      <p className="inline ml-4">Rockwell Automation</p>
      <br />
      <h3 className="font-bold inline">Parent Company:</h3>
      <p className="inline ml-4">Pro Mach</p>
    </SidebarBlock>
  );
}

export default ProfilePage;
