import * as React from 'react';
import Helmet from 'react-helmet';

import Page from 'components/Page';
import NotFound from 'components/NotFound';

function NotFoundPage({ withImageBg = false }) {
  return (
    <Page withImageBg={withImageBg}>
      <Helmet>
        <html lang="en" />
        <title>Page Not Found | PMMI ProSource</title>
      </Helmet>
      <div className="profile-container" id="main-content">
        <NotFound />
      </div>
    </Page>
  );
}

export default NotFoundPage;
