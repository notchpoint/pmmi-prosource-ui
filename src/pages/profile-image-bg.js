import * as React from 'react';

import ProfilePage from './profile';

function ImageBackgroundProfilePage() {
  return <ProfilePage withImageBg />;
}

export default ImageBackgroundProfilePage;
