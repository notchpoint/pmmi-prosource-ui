import faker from 'faker';
import * as React from 'react';
import Helmet from 'react-helmet';

import BackToHomeLink from 'components/BackToHomeLink';
import CompanyCard from 'components/CompanyCard';
import FilterLayout from 'components/FilterLayout';
import FilterSidebarContent from 'components/FilterSidebarContent';
import {
  Button as MobileSidebarButton,
  SearchButton as MobileSidebarSearchButton,
  Sidebar as MobileSidebar,
} from 'components/MobileSidebar';
import Page from 'components/Page';
import ParentCategoryCard from 'components/ParentCategoryCard';
import KeywordHighlight from 'components/Search/KeywordHighlight';
import SearchHeading from 'components/Search/Heading';
import SearchSection from 'components/Search/Section';
import SearchSubsection from 'components/Search/Subsection';

// https://www.sketch.com/s/d1bf1b62-abec-4624-a81f-94668addb2d8/a/Mykkn7Q
function SearchResultsPage() {
  return (
    <Page isShaded>
      <Helmet>
        <html lang="en" />
        <title>Search Results for "Label" | PMMI ProSource</title>
      </Helmet>
      <FilterLayout>
        <FilterLayout.Aside>
          <FilterSidebarContent />
        </FilterLayout.Aside>
        <FilterLayout.Content>
          <BackToHomeLink />
          <MobileSidebarButton className="mt-24">filter</MobileSidebarButton>
          <MobileSidebar subheading="Package Type">
            <FilterSidebarContent isMobile />
          </MobileSidebar>
          <SearchHeading keyword="Label" />
          <SearchSection type="Equipment, materials & services" keyword="Label">
            <SearchSubsection
              description="Machinery that makes marks onto packages and labels and can also print onto labels."
              heading={
                <KeywordHighlight
                  phrase="Coding, Labeling & Printing"
                  highlight="Label"
                />
              }
            >
              <ParentCategoryCard
                headingLevel={3}
                name={
                  <KeywordHighlight
                    phrase="Labeling Equipment"
                    highlight="Label"
                  />
                }
                numberOfCompanies={36}
                description="Machinery that fills liquid product into a primary rigid container before closing, capping, or seaming it."
                types={[
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Pressure Sensitive Label Applicators"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A filling machine that xyz and abc and lors and this is more text so that we can demonstrated truncated overflow',
                    link: 'View 12 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Glue-Applied Labelers"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 18 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Roll-Fed Labelers"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 25 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Cut-and-Stack Labelers"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 15 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="RFID Applicators"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 9 companies',
                  },
                  {
                    name: 'Flow Meter Fillers',
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 10 companies',
                  },
                ]}
              />
            </SearchSubsection>
            <SearchSubsection
              description="Products that are used in the factory and in any package-forming process."
              heading={
                <KeywordHighlight
                  phrase="Materials, Containers & Consumables"
                  highlight="Label"
                />
              }
            >
              <ParentCategoryCard
                headingLevel={3}
                name={
                  <KeywordHighlight
                    phrase="Labeling Equipment"
                    highlight="Label"
                  />
                }
                numberOfCompanies={36}
                description="Machinery that fills liquid product into a primary rigid container before closing, capping, or seaming it."
                types={[
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Pressure Sensitive Label Applicators"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A filling machine that xyz and abc and lors and this is more text so that we can demonstrated truncated overflow',
                    link: 'View 12 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Glue-Applied Labelers"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 18 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Roll-Fed Labelers"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 25 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="Cut-and-Stack Labelers"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 15 companies',
                  },
                  {
                    name: (
                      <KeywordHighlight
                        phrase="RFID Applicators"
                        highlight="Label"
                      />
                    ),
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 9 companies',
                  },
                  {
                    name: 'Flow Meter Fillers',
                    description:
                      'A machine that fills by a certain kind of process that the designer is not familiar with',
                    link: 'View 10 companies',
                  },
                ]}
              />
            </SearchSubsection>
          </SearchSection>
          <SearchSection type="Companies" keyword="Label">
            <ul className="company-card-search-list">
              <li>
                <CompanyCard
                  headingLevel={3}
                  alt="demo image"
                  companyName={
                    <KeywordHighlight
                      phrase="Label-Aire, Inc."
                      highlight="Label"
                    />
                  }
                  description="Very quick summary of this listing"
                  image={faker.image.image(165, 125)}
                  url="#"
                />
              </li>
              <li>
                <CompanyCard
                  headingLevel={3}
                  alt="demo image"
                  companyName={
                    <KeywordHighlight
                      phrase="Labelmate USA"
                      highlight="Label"
                    />
                  }
                  description="Quick summary of this listing. It is a little longer than the first company's summary."
                  image={faker.image.image(165, 125)}
                  url="#"
                />
              </li>
              <li>
                <CompanyCard
                  headingLevel={3}
                  alt="demo image"
                  companyName={
                    <KeywordHighlight
                      phrase="Labelpack Automation, Inc."
                      highlight="Label"
                    />
                  }
                  description="Quick summary of this listing. It is a little longer than the first company's summary."
                  image={faker.image.image(165, 125)}
                  url="#"
                />
              </li>
            </ul>
          </SearchSection>
        </FilterLayout.Content>
      </FilterLayout>
    </Page>
  );
}

export default SearchResultsPage;
