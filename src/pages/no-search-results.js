import faker from 'faker';
import * as React from 'react';
import Helmet from 'react-helmet';

import BackToHomeLink from 'components/BackToHomeLink';
import FilterLayout from 'components/FilterLayout';
import FilterSidebarContent from 'components/FilterSidebarContent';
import {
  Button as MobileSidebarButton,
  Sidebar as MobileSidebar,
} from 'components/MobileSidebar';
import Page from 'components/Page';
import SearchHeading from 'components/Search/Heading';
import SearchTip from 'components/Search/SearchTip';

// https://www.sketch.com/s/d1bf1b62-abec-4624-a81f-94668addb2d8/a/g07JPVa
function NoSearchResultsPage() {
  return (
    <Page isShaded>
      <Helmet>
        <html lang="en" />
        <title>No search results for "Labelz" | PMMI ProSource</title>
      </Helmet>
      <FilterLayout>
        <FilterLayout.Aside>
          <FilterSidebarContent />
        </FilterLayout.Aside>
        <FilterLayout.Content>
          <BackToHomeLink />
          <MobileSidebarButton className="mt-24">filter</MobileSidebarButton>
          <MobileSidebar subheading="Package Type">
            <FilterSidebarContent isMobile />
          </MobileSidebar>
          <SearchHeading
            keyword="Labelz"
            hasResults={false}
            suggestions={['Labeling Equipment', 'Label Types']}
          />
          <h2 className="font-bold mb-24 text-h3">Search tips:</h2>
          <SearchTip
            title="Use the auto-complete for maximum efficiency and precision"
            image={faker.image.image(400, 200)}
          />
          <SearchTip
            title="Check for typos"
            description={
              <React.Fragment>
                <strong>Gravity filler</strong> instead of{' '}
                <strong>Gravitie filler</strong>
              </React.Fragment>
            }
          />
          <SearchTip
            title="Keywords map only to company names and categories"
            image={faker.image.image(400, 200)}
          />
          <SearchTip
            title={
              <React.Fragment>
                Browse our directory by equipment, materials & services
                categories from the <a href="#">home page</a>
              </React.Fragment>
            }
            image={faker.image.image(400, 200)}
          />
        </FilterLayout.Content>
      </FilterLayout>
    </Page>
  );
}

export default NoSearchResultsPage;
