module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      transparent: 'transparent',
      primary: {
        blue: '#0078BD',
        light: '#F3F7F5',
        navy: '#00476E',
      },
      secondary: {
        light: '#D0DDE4',
        red: '#B7102D',
      },
      neutral: {
        100: '#ffffff',
        200: '#fafaf9',
        300: '#eeeeee',
        400: '#d8d8d8',
        500: '#b4b4b4',
        600: '#666666',
        700: '#3c3c3c',
        800: '#000000',
      },
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '1rem',
        tablet: '1.5rem',
        'sm-desktop': '1.5rem',
        'profile-desktop': '1.5rem',
      },
    },
    fontFamily: {
      sans: 'Roboto, Helvetica, Arial, sans-serif',
    },
    fontSize: {
      h1: ['2.125rem', 'normal'],
      h2: ['1.5rem', 'normal'],
      h3: ['1.25rem', '1.375rem'],
      h4: ['1rem', '1.25rem'],
      h5: ['0.875rem', '1rem'],
      p: ['1rem', '1.5rem'],
      'p-sm': ['0.875rem', '1.25rem'],
      caption: ['0.75rem', '1rem'],
      label: ['0.875rem', '1rem'],
    },
    screens: {
      'lg-mobile': '30rem', // 480px
      tablet: '48rem', // 768px
      'sm-desktop': '64rem', // 1024px
      'profile-desktop': '75rem', // 1200px
      'hd-desktop': '82.25rem', // 1396px
    },
    spacing: {
      0: 0,
      4: '0.25rem',
      8: '0.5rem',
      16: '1rem',
      24: '1.5rem',
      32: '2rem',
      40: '2.5rem',
      72: '4.5rem',
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require('tailwindcss-elevation')(['responsive'])],
};
