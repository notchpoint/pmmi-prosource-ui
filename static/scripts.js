/**
 * @popperjs/core v2.9.2 - MIT License
 */

'use strict';
!(function (e, t) {
  'object' == typeof exports && 'undefined' != typeof module
    ? t(exports)
    : 'function' == typeof define && define.amd
    ? define(['exports'], t)
    : t(
        ((e =
          'undefined' != typeof globalThis
            ? globalThis
            : e || self).Popper = {}),
      );
})(this, function (e) {
  function t(e) {
    return {
      width: (e = e.getBoundingClientRect()).width,
      height: e.height,
      top: e.top,
      right: e.right,
      bottom: e.bottom,
      left: e.left,
      x: e.left,
      y: e.top,
    };
  }
  function n(e) {
    return null == e
      ? window
      : '[object Window]' !== e.toString()
      ? ((e = e.ownerDocument) && e.defaultView) || window
      : e;
  }
  function o(e) {
    return { scrollLeft: (e = n(e)).pageXOffset, scrollTop: e.pageYOffset };
  }
  function r(e) {
    return e instanceof n(e).Element || e instanceof Element;
  }
  function i(e) {
    return e instanceof n(e).HTMLElement || e instanceof HTMLElement;
  }
  function a(e) {
    return (
      'undefined' != typeof ShadowRoot &&
      (e instanceof n(e).ShadowRoot || e instanceof ShadowRoot)
    );
  }
  function s(e) {
    return e ? (e.nodeName || '').toLowerCase() : null;
  }
  function f(e) {
    return (
      (r(e) ? e.ownerDocument : e.document) || window.document
    ).documentElement;
  }
  function p(e) {
    return t(f(e)).left + o(e).scrollLeft;
  }
  function c(e) {
    return n(e).getComputedStyle(e);
  }
  function l(e) {
    return (
      (e = c(e)),
      /auto|scroll|overlay|hidden/.test(e.overflow + e.overflowY + e.overflowX)
    );
  }
  function u(e, r, a) {
    void 0 === a && (a = !1);
    var c = f(r);
    e = t(e);
    var u = i(r),
      d = { scrollLeft: 0, scrollTop: 0 },
      m = { x: 0, y: 0 };
    return (
      (u || (!u && !a)) &&
        (('body' !== s(r) || l(c)) &&
          (d =
            r !== n(r) && i(r)
              ? { scrollLeft: r.scrollLeft, scrollTop: r.scrollTop }
              : o(r)),
        i(r)
          ? (((m = t(r)).x += r.clientLeft), (m.y += r.clientTop))
          : c && (m.x = p(c))),
      {
        x: e.left + d.scrollLeft - m.x,
        y: e.top + d.scrollTop - m.y,
        width: e.width,
        height: e.height,
      }
    );
  }
  function d(e) {
    var n = t(e),
      o = e.offsetWidth,
      r = e.offsetHeight;
    return (
      1 >= Math.abs(n.width - o) && (o = n.width),
      1 >= Math.abs(n.height - r) && (r = n.height),
      { x: e.offsetLeft, y: e.offsetTop, width: o, height: r }
    );
  }
  function m(e) {
    return 'html' === s(e)
      ? e
      : e.assignedSlot || e.parentNode || (a(e) ? e.host : null) || f(e);
  }
  function h(e) {
    return 0 <= ['html', 'body', '#document'].indexOf(s(e))
      ? e.ownerDocument.body
      : i(e) && l(e)
      ? e
      : h(m(e));
  }
  function v(e, t) {
    var o;
    void 0 === t && (t = []);
    var r = h(e);
    return (
      (e = r === (null == (o = e.ownerDocument) ? void 0 : o.body)),
      (o = n(r)),
      (r = e ? [o].concat(o.visualViewport || [], l(r) ? r : []) : r),
      (t = t.concat(r)),
      e ? t : t.concat(v(m(r)))
    );
  }
  function g(e) {
    return i(e) && 'fixed' !== c(e).position ? e.offsetParent : null;
  }
  function y(e) {
    for (
      var t = n(e), o = g(e);
      o &&
      0 <= ['table', 'td', 'th'].indexOf(s(o)) &&
      'static' === c(o).position;

    )
      o = g(o);
    if (
      o &&
      ('html' === s(o) || ('body' === s(o) && 'static' === c(o).position))
    )
      return t;
    if (!o)
      e: {
        if (
          ((o = -1 !== navigator.userAgent.toLowerCase().indexOf('firefox')),
          -1 === navigator.userAgent.indexOf('Trident') ||
            !i(e) ||
            'fixed' !== c(e).position)
        )
          for (e = m(e); i(e) && 0 > ['html', 'body'].indexOf(s(e)); ) {
            var r = c(e);
            if (
              'none' !== r.transform ||
              'none' !== r.perspective ||
              'paint' === r.contain ||
              -1 !== ['transform', 'perspective'].indexOf(r.willChange) ||
              (o && 'filter' === r.willChange) ||
              (o && r.filter && 'none' !== r.filter)
            ) {
              o = e;
              break e;
            }
            e = e.parentNode;
          }
        o = null;
      }
    return o || t;
  }
  function b(e) {
    function t(e) {
      o.add(e.name),
        []
          .concat(e.requires || [], e.requiresIfExists || [])
          .forEach(function (e) {
            o.has(e) || ((e = n.get(e)) && t(e));
          }),
        r.push(e);
    }
    var n = new Map(),
      o = new Set(),
      r = [];
    return (
      e.forEach(function (e) {
        n.set(e.name, e);
      }),
      e.forEach(function (e) {
        o.has(e.name) || t(e);
      }),
      r
    );
  }
  function w(e) {
    var t;
    return function () {
      return (
        t ||
          (t = new Promise(function (n) {
            Promise.resolve().then(function () {
              (t = void 0), n(e());
            });
          })),
        t
      );
    };
  }
  function x(e) {
    return e.split('-')[0];
  }
  function O(e, t) {
    var n = t.getRootNode && t.getRootNode();
    if (e.contains(t)) return !0;
    if (n && a(n))
      do {
        if (t && e.isSameNode(t)) return !0;
        t = t.parentNode || t.host;
      } while (t);
    return !1;
  }
  function j(e) {
    return Object.assign({}, e, {
      left: e.x,
      top: e.y,
      right: e.x + e.width,
      bottom: e.y + e.height,
    });
  }
  function E(e, r) {
    if ('viewport' === r) {
      r = n(e);
      var a = f(e);
      r = r.visualViewport;
      var s = a.clientWidth;
      a = a.clientHeight;
      var l = 0,
        u = 0;
      r &&
        ((s = r.width),
        (a = r.height),
        /^((?!chrome|android).)*safari/i.test(navigator.userAgent) ||
          ((l = r.offsetLeft), (u = r.offsetTop))),
        (e = j((e = { width: s, height: a, x: l + p(e), y: u })));
    } else i(r) ? (((e = t(r)).top += r.clientTop), (e.left += r.clientLeft), (e.bottom = e.top + r.clientHeight), (e.right = e.left + r.clientWidth), (e.width = r.clientWidth), (e.height = r.clientHeight), (e.x = e.left), (e.y = e.top)) : ((u = f(e)), (e = f(u)), (s = o(u)), (r = null == (a = u.ownerDocument) ? void 0 : a.body), (a = _(e.scrollWidth, e.clientWidth, r ? r.scrollWidth : 0, r ? r.clientWidth : 0)), (l = _(e.scrollHeight, e.clientHeight, r ? r.scrollHeight : 0, r ? r.clientHeight : 0)), (u = -s.scrollLeft + p(u)), (s = -s.scrollTop), 'rtl' === c(r || e).direction && (u += _(e.clientWidth, r ? r.clientWidth : 0) - a), (e = j({ width: a, height: l, x: u, y: s })));
    return e;
  }
  function D(e, t, n) {
    return (
      (t =
        'clippingParents' === t
          ? (function (e) {
              var t = v(m(e)),
                n =
                  0 <= ['absolute', 'fixed'].indexOf(c(e).position) && i(e)
                    ? y(e)
                    : e;
              return r(n)
                ? t.filter(function (e) {
                    return r(e) && O(e, n) && 'body' !== s(e);
                  })
                : [];
            })(e)
          : [].concat(t)),
      ((n = (n = [].concat(t, [n])).reduce(function (t, n) {
        return (
          (n = E(e, n)),
          (t.top = _(n.top, t.top)),
          (t.right = U(n.right, t.right)),
          (t.bottom = U(n.bottom, t.bottom)),
          (t.left = _(n.left, t.left)),
          t
        );
      }, E(e, n[0]))).width = n.right - n.left),
      (n.height = n.bottom - n.top),
      (n.x = n.left),
      (n.y = n.top),
      n
    );
  }
  function L(e) {
    return 0 <= ['top', 'bottom'].indexOf(e) ? 'x' : 'y';
  }
  function P(e) {
    var t = e.reference,
      n = e.element,
      o = (e = e.placement) ? x(e) : null;
    e = e ? e.split('-')[1] : null;
    var r = t.x + t.width / 2 - n.width / 2,
      i = t.y + t.height / 2 - n.height / 2;
    switch (o) {
      case 'top':
        r = { x: r, y: t.y - n.height };
        break;
      case 'bottom':
        r = { x: r, y: t.y + t.height };
        break;
      case 'right':
        r = { x: t.x + t.width, y: i };
        break;
      case 'left':
        r = { x: t.x - n.width, y: i };
        break;
      default:
        r = { x: t.x, y: t.y };
    }
    if (null != (o = o ? L(o) : null))
      switch (((i = 'y' === o ? 'height' : 'width'), e)) {
        case 'start':
          r[o] -= t[i] / 2 - n[i] / 2;
          break;
        case 'end':
          r[o] += t[i] / 2 - n[i] / 2;
      }
    return r;
  }
  function M(e) {
    return Object.assign({}, { top: 0, right: 0, bottom: 0, left: 0 }, e);
  }
  function k(e, t) {
    return t.reduce(function (t, n) {
      return (t[n] = e), t;
    }, {});
  }
  function A(e, n) {
    void 0 === n && (n = {});
    var o = n;
    n = void 0 === (n = o.placement) ? e.placement : n;
    var i = o.boundary,
      a = void 0 === i ? 'clippingParents' : i,
      s = void 0 === (i = o.rootBoundary) ? 'viewport' : i;
    i = void 0 === (i = o.elementContext) ? 'popper' : i;
    var p = o.altBoundary,
      c = void 0 !== p && p;
    o = M(
      'number' != typeof (o = void 0 === (o = o.padding) ? 0 : o) ? o : k(o, C),
    );
    var l = e.elements.reference;
    (p = e.rects.popper),
      (a = D(
        r((c = e.elements[c ? ('popper' === i ? 'reference' : 'popper') : i]))
          ? c
          : c.contextElement || f(e.elements.popper),
        a,
        s,
      )),
      (c = P({
        reference: (s = t(l)),
        element: p,
        strategy: 'absolute',
        placement: n,
      })),
      (p = j(Object.assign({}, p, c))),
      (s = 'popper' === i ? p : s);
    var u = {
      top: a.top - s.top + o.top,
      bottom: s.bottom - a.bottom + o.bottom,
      left: a.left - s.left + o.left,
      right: s.right - a.right + o.right,
    };
    if (((e = e.modifiersData.offset), 'popper' === i && e)) {
      var d = e[n];
      Object.keys(u).forEach(function (e) {
        var t = 0 <= ['right', 'bottom'].indexOf(e) ? 1 : -1,
          n = 0 <= ['top', 'bottom'].indexOf(e) ? 'y' : 'x';
        u[e] += d[n] * t;
      });
    }
    return u;
  }
  function W() {
    for (var e = arguments.length, t = Array(e), n = 0; n < e; n++)
      t[n] = arguments[n];
    return !t.some(function (e) {
      return !(e && 'function' == typeof e.getBoundingClientRect);
    });
  }
  function B(e) {
    void 0 === e && (e = {});
    var t = e.defaultModifiers,
      n = void 0 === t ? [] : t,
      o = void 0 === (e = e.defaultOptions) ? F : e;
    return function (e, t, i) {
      function a() {
        f.forEach(function (e) {
          return e();
        }),
          (f = []);
      }
      void 0 === i && (i = o);
      var s = {
          placement: 'bottom',
          orderedModifiers: [],
          options: Object.assign({}, F, o),
          modifiersData: {},
          elements: { reference: e, popper: t },
          attributes: {},
          styles: {},
        },
        f = [],
        p = !1,
        c = {
          state: s,
          setOptions: function (i) {
            return (
              a(),
              (s.options = Object.assign({}, o, s.options, i)),
              (s.scrollParents = {
                reference: r(e)
                  ? v(e)
                  : e.contextElement
                  ? v(e.contextElement)
                  : [],
                popper: v(t),
              }),
              (i = (function (e) {
                var t = b(e);
                return I.reduce(function (e, n) {
                  return e.concat(
                    t.filter(function (e) {
                      return e.phase === n;
                    }),
                  );
                }, []);
              })(
                (function (e) {
                  var t = e.reduce(function (e, t) {
                    var n = e[t.name];
                    return (
                      (e[t.name] = n
                        ? Object.assign({}, n, t, {
                            options: Object.assign({}, n.options, t.options),
                            data: Object.assign({}, n.data, t.data),
                          })
                        : t),
                      e
                    );
                  }, {});
                  return Object.keys(t).map(function (e) {
                    return t[e];
                  });
                })([].concat(n, s.options.modifiers)),
              )),
              (s.orderedModifiers = i.filter(function (e) {
                return e.enabled;
              })),
              s.orderedModifiers.forEach(function (e) {
                var t = e.name,
                  n = e.options;
                (n = void 0 === n ? {} : n),
                  'function' == typeof (e = e.effect) &&
                    ((t = e({ state: s, name: t, instance: c, options: n })),
                    f.push(t || function () {}));
              }),
              c.update()
            );
          },
          forceUpdate: function () {
            if (!p) {
              var e = s.elements,
                t = e.reference;
              if (W(t, (e = e.popper)))
                for (
                  s.rects = {
                    reference: u(t, y(e), 'fixed' === s.options.strategy),
                    popper: d(e),
                  },
                    s.reset = !1,
                    s.placement = s.options.placement,
                    s.orderedModifiers.forEach(function (e) {
                      return (s.modifiersData[e.name] = Object.assign(
                        {},
                        e.data,
                      ));
                    }),
                    t = 0;
                  t < s.orderedModifiers.length;
                  t++
                )
                  if (!0 === s.reset) (s.reset = !1), (t = -1);
                  else {
                    var n = s.orderedModifiers[t];
                    e = n.fn;
                    var o = n.options;
                    (o = void 0 === o ? {} : o),
                      (n = n.name),
                      'function' == typeof e &&
                        (s =
                          e({ state: s, options: o, name: n, instance: c }) ||
                          s);
                  }
            }
          },
          update: w(function () {
            return new Promise(function (e) {
              c.forceUpdate(), e(s);
            });
          }),
          destroy: function () {
            a(), (p = !0);
          },
        };
      return W(e, t)
        ? (c.setOptions(i).then(function (e) {
            !p && i.onFirstUpdate && i.onFirstUpdate(e);
          }),
          c)
        : c;
    };
  }
  function T(e) {
    var t,
      o = e.popper,
      r = e.popperRect,
      i = e.placement,
      a = e.offsets,
      s = e.position,
      p = e.gpuAcceleration,
      l = e.adaptive;
    if (!0 === (e = e.roundOffsets)) {
      e = a.y;
      var u = window.devicePixelRatio || 1;
      e = { x: z(z(a.x * u) / u) || 0, y: z(z(e * u) / u) || 0 };
    } else e = 'function' == typeof e ? e(a) : a;
    (e = void 0 === (e = (u = e).x) ? 0 : e),
      (u = void 0 === (u = u.y) ? 0 : u);
    var d = a.hasOwnProperty('x');
    a = a.hasOwnProperty('y');
    var m,
      h = 'left',
      v = 'top',
      g = window;
    if (l) {
      var b = y(o),
        w = 'clientHeight',
        x = 'clientWidth';
      b === n(o) &&
        'static' !== c((b = f(o))).position &&
        ((w = 'scrollHeight'), (x = 'scrollWidth')),
        'top' === i &&
          ((v = 'bottom'), (u -= b[w] - r.height), (u *= p ? 1 : -1)),
        'left' === i &&
          ((h = 'right'), (e -= b[x] - r.width), (e *= p ? 1 : -1));
    }
    return (
      (o = Object.assign({ position: s }, l && J)),
      p
        ? Object.assign(
            {},
            o,
            (((m = {})[v] = a ? '0' : ''),
            (m[h] = d ? '0' : ''),
            (m.transform =
              2 > (g.devicePixelRatio || 1)
                ? 'translate(' + e + 'px, ' + u + 'px)'
                : 'translate3d(' + e + 'px, ' + u + 'px, 0)'),
            m),
          )
        : Object.assign(
            {},
            o,
            (((t = {})[v] = a ? u + 'px' : ''),
            (t[h] = d ? e + 'px' : ''),
            (t.transform = ''),
            t),
          )
    );
  }
  function H(e) {
    return e.replace(/left|right|bottom|top/g, function (e) {
      return $[e];
    });
  }
  function R(e) {
    return e.replace(/start|end/g, function (e) {
      return ee[e];
    });
  }
  function S(e, t, n) {
    return (
      void 0 === n && (n = { x: 0, y: 0 }),
      {
        top: e.top - t.height - n.y,
        right: e.right - t.width + n.x,
        bottom: e.bottom - t.height + n.y,
        left: e.left - t.width - n.x,
      }
    );
  }
  function q(e) {
    return ['top', 'right', 'bottom', 'left'].some(function (t) {
      return 0 <= e[t];
    });
  }
  var C = ['top', 'bottom', 'right', 'left'],
    N = C.reduce(function (e, t) {
      return e.concat([t + '-start', t + '-end']);
    }, []),
    V = [].concat(C, ['auto']).reduce(function (e, t) {
      return e.concat([t, t + '-start', t + '-end']);
    }, []),
    I = 'beforeRead read afterRead beforeMain main afterMain beforeWrite write afterWrite'.split(
      ' ',
    ),
    _ = Math.max,
    U = Math.min,
    z = Math.round,
    F = { placement: 'bottom', modifiers: [], strategy: 'absolute' },
    X = { passive: !0 },
    Y = {
      name: 'eventListeners',
      enabled: !0,
      phase: 'write',
      fn: function () {},
      effect: function (e) {
        var t = e.state,
          o = e.instance,
          r = (e = e.options).scroll,
          i = void 0 === r || r,
          a = void 0 === (e = e.resize) || e,
          s = n(t.elements.popper),
          f = [].concat(t.scrollParents.reference, t.scrollParents.popper);
        return (
          i &&
            f.forEach(function (e) {
              e.addEventListener('scroll', o.update, X);
            }),
          a && s.addEventListener('resize', o.update, X),
          function () {
            i &&
              f.forEach(function (e) {
                e.removeEventListener('scroll', o.update, X);
              }),
              a && s.removeEventListener('resize', o.update, X);
          }
        );
      },
      data: {},
    },
    G = {
      name: 'popperOffsets',
      enabled: !0,
      phase: 'read',
      fn: function (e) {
        var t = e.state;
        t.modifiersData[e.name] = P({
          reference: t.rects.reference,
          element: t.rects.popper,
          strategy: 'absolute',
          placement: t.placement,
        });
      },
      data: {},
    },
    J = { top: 'auto', right: 'auto', bottom: 'auto', left: 'auto' },
    K = {
      name: 'computeStyles',
      enabled: !0,
      phase: 'beforeWrite',
      fn: function (e) {
        var t = e.state,
          n = e.options;
        e = void 0 === (e = n.gpuAcceleration) || e;
        var o = n.adaptive;
        (o = void 0 === o || o),
          (n = void 0 === (n = n.roundOffsets) || n),
          (e = {
            placement: x(t.placement),
            popper: t.elements.popper,
            popperRect: t.rects.popper,
            gpuAcceleration: e,
          }),
          null != t.modifiersData.popperOffsets &&
            (t.styles.popper = Object.assign(
              {},
              t.styles.popper,
              T(
                Object.assign({}, e, {
                  offsets: t.modifiersData.popperOffsets,
                  position: t.options.strategy,
                  adaptive: o,
                  roundOffsets: n,
                }),
              ),
            )),
          null != t.modifiersData.arrow &&
            (t.styles.arrow = Object.assign(
              {},
              t.styles.arrow,
              T(
                Object.assign({}, e, {
                  offsets: t.modifiersData.arrow,
                  position: 'absolute',
                  adaptive: !1,
                  roundOffsets: n,
                }),
              ),
            )),
          (t.attributes.popper = Object.assign({}, t.attributes.popper, {
            'data-popper-placement': t.placement,
          }));
      },
      data: {},
    },
    Q = {
      name: 'applyStyles',
      enabled: !0,
      phase: 'write',
      fn: function (e) {
        var t = e.state;
        Object.keys(t.elements).forEach(function (e) {
          var n = t.styles[e] || {},
            o = t.attributes[e] || {},
            r = t.elements[e];
          i(r) &&
            s(r) &&
            (Object.assign(r.style, n),
            Object.keys(o).forEach(function (e) {
              var t = o[e];
              !1 === t
                ? r.removeAttribute(e)
                : r.setAttribute(e, !0 === t ? '' : t);
            }));
        });
      },
      effect: function (e) {
        var t = e.state,
          n = {
            popper: {
              position: t.options.strategy,
              left: '0',
              top: '0',
              margin: '0',
            },
            arrow: { position: 'absolute' },
            reference: {},
          };
        return (
          Object.assign(t.elements.popper.style, n.popper),
          (t.styles = n),
          t.elements.arrow && Object.assign(t.elements.arrow.style, n.arrow),
          function () {
            Object.keys(t.elements).forEach(function (e) {
              var o = t.elements[e],
                r = t.attributes[e] || {};
              (e = Object.keys(
                t.styles.hasOwnProperty(e) ? t.styles[e] : n[e],
              ).reduce(function (e, t) {
                return (e[t] = ''), e;
              }, {})),
                i(o) &&
                  s(o) &&
                  (Object.assign(o.style, e),
                  Object.keys(r).forEach(function (e) {
                    o.removeAttribute(e);
                  }));
            });
          }
        );
      },
      requires: ['computeStyles'],
    },
    Z = {
      name: 'offset',
      enabled: !0,
      phase: 'main',
      requires: ['popperOffsets'],
      fn: function (e) {
        var t = e.state,
          n = e.name,
          o = void 0 === (e = e.options.offset) ? [0, 0] : e,
          r = (e = V.reduce(function (e, n) {
            var r = t.rects,
              i = x(n),
              a = 0 <= ['left', 'top'].indexOf(i) ? -1 : 1,
              s =
                'function' == typeof o
                  ? o(Object.assign({}, r, { placement: n }))
                  : o;
            return (
              (r = (r = s[0]) || 0),
              (s = ((s = s[1]) || 0) * a),
              (i =
                0 <= ['left', 'right'].indexOf(i)
                  ? { x: s, y: r }
                  : { x: r, y: s }),
              (e[n] = i),
              e
            );
          }, {}))[t.placement],
          i = r.x;
        (r = r.y),
          null != t.modifiersData.popperOffsets &&
            ((t.modifiersData.popperOffsets.x += i),
            (t.modifiersData.popperOffsets.y += r)),
          (t.modifiersData[n] = e);
      },
    },
    $ = { left: 'right', right: 'left', bottom: 'top', top: 'bottom' },
    ee = { start: 'end', end: 'start' },
    te = {
      name: 'flip',
      enabled: !0,
      phase: 'main',
      fn: function (e) {
        var t = e.state,
          n = e.options;
        if (((e = e.name), !t.modifiersData[e]._skip)) {
          var o = n.mainAxis;
          o = void 0 === o || o;
          var r = n.altAxis;
          r = void 0 === r || r;
          var i = n.fallbackPlacements,
            a = n.padding,
            s = n.boundary,
            f = n.rootBoundary,
            p = n.altBoundary,
            c = n.flipVariations,
            l = void 0 === c || c,
            u = n.allowedAutoPlacements;
          (c = x((n = t.options.placement))),
            (i =
              i ||
              (c !== n && l
                ? (function (e) {
                    if ('auto' === x(e)) return [];
                    var t = H(e);
                    return [R(e), t, R(t)];
                  })(n)
                : [H(n)]));
          var d = [n].concat(i).reduce(function (e, n) {
            return e.concat(
              'auto' === x(n)
                ? (function (e, t) {
                    void 0 === t && (t = {});
                    var n = t.boundary,
                      o = t.rootBoundary,
                      r = t.padding,
                      i = t.flipVariations,
                      a = t.allowedAutoPlacements,
                      s = void 0 === a ? V : a,
                      f = t.placement.split('-')[1];
                    0 ===
                      (i = (t = f
                        ? i
                          ? N
                          : N.filter(function (e) {
                              return e.split('-')[1] === f;
                            })
                        : C).filter(function (e) {
                        return 0 <= s.indexOf(e);
                      })).length && (i = t);
                    var p = i.reduce(function (t, i) {
                      return (
                        (t[i] = A(e, {
                          placement: i,
                          boundary: n,
                          rootBoundary: o,
                          padding: r,
                        })[x(i)]),
                        t
                      );
                    }, {});
                    return Object.keys(p).sort(function (e, t) {
                      return p[e] - p[t];
                    });
                  })(t, {
                    placement: n,
                    boundary: s,
                    rootBoundary: f,
                    padding: a,
                    flipVariations: l,
                    allowedAutoPlacements: u,
                  })
                : n,
            );
          }, []);
          (n = t.rects.reference), (i = t.rects.popper);
          var m = new Map();
          c = !0;
          for (var h = d[0], v = 0; v < d.length; v++) {
            var g = d[v],
              y = x(g),
              b = 'start' === g.split('-')[1],
              w = 0 <= ['top', 'bottom'].indexOf(y),
              O = w ? 'width' : 'height',
              j = A(t, {
                placement: g,
                boundary: s,
                rootBoundary: f,
                altBoundary: p,
                padding: a,
              });
            if (
              ((b = w ? (b ? 'right' : 'left') : b ? 'bottom' : 'top'),
              n[O] > i[O] && (b = H(b)),
              (O = H(b)),
              (w = []),
              o && w.push(0 >= j[y]),
              r && w.push(0 >= j[b], 0 >= j[O]),
              w.every(function (e) {
                return e;
              }))
            ) {
              (h = g), (c = !1);
              break;
            }
            m.set(g, w);
          }
          if (c)
            for (
              o = function (e) {
                var t = d.find(function (t) {
                  if ((t = m.get(t)))
                    return t.slice(0, e).every(function (e) {
                      return e;
                    });
                });
                if (t) return (h = t), 'break';
              },
                r = l ? 3 : 1;
              0 < r && 'break' !== o(r);
              r--
            );
          t.placement !== h &&
            ((t.modifiersData[e]._skip = !0),
            (t.placement = h),
            (t.reset = !0));
        }
      },
      requiresIfExists: ['offset'],
      data: { _skip: !1 },
    },
    ne = {
      name: 'preventOverflow',
      enabled: !0,
      phase: 'main',
      fn: function (e) {
        var t = e.state,
          n = e.options;
        e = e.name;
        var o = n.mainAxis,
          r = void 0 === o || o,
          i = void 0 !== (o = n.altAxis) && o;
        o = void 0 === (o = n.tether) || o;
        var a = n.tetherOffset,
          s = void 0 === a ? 0 : a,
          f = A(t, {
            boundary: n.boundary,
            rootBoundary: n.rootBoundary,
            padding: n.padding,
            altBoundary: n.altBoundary,
          });
        n = x(t.placement);
        var p = t.placement.split('-')[1],
          c = !p,
          l = L(n);
        (n = 'x' === l ? 'y' : 'x'), (a = t.modifiersData.popperOffsets);
        var u = t.rects.reference,
          m = t.rects.popper,
          h =
            'function' == typeof s
              ? s(Object.assign({}, t.rects, { placement: t.placement }))
              : s;
        if (((s = { x: 0, y: 0 }), a)) {
          if (r || i) {
            var v = 'y' === l ? 'top' : 'left',
              g = 'y' === l ? 'bottom' : 'right',
              b = 'y' === l ? 'height' : 'width',
              w = a[l],
              O = a[l] + f[v],
              j = a[l] - f[g],
              E = o ? -m[b] / 2 : 0,
              D = 'start' === p ? u[b] : m[b];
            (p = 'start' === p ? -m[b] : -u[b]),
              (m = t.elements.arrow),
              (m = o && m ? d(m) : { width: 0, height: 0 });
            var P = t.modifiersData['arrow#persistent']
              ? t.modifiersData['arrow#persistent'].padding
              : { top: 0, right: 0, bottom: 0, left: 0 };
            (v = P[v]),
              (g = P[g]),
              (m = _(0, U(u[b], m[b]))),
              (D = c ? u[b] / 2 - E - m - v - h : D - m - v - h),
              (u = c ? -u[b] / 2 + E + m + g + h : p + m + g + h),
              (c = t.elements.arrow && y(t.elements.arrow)),
              (h = t.modifiersData.offset
                ? t.modifiersData.offset[t.placement][l]
                : 0),
              (c =
                a[l] +
                D -
                h -
                (c ? ('y' === l ? c.clientTop || 0 : c.clientLeft || 0) : 0)),
              (u = a[l] + u - h),
              r &&
                ((r = o ? U(O, c) : O),
                (j = o ? _(j, u) : j),
                (r = _(r, U(w, j))),
                (a[l] = r),
                (s[l] = r - w)),
              i &&
                ((r = (i = a[n]) + f['x' === l ? 'top' : 'left']),
                (f = i - f['x' === l ? 'bottom' : 'right']),
                (r = o ? U(r, c) : r),
                (o = o ? _(f, u) : f),
                (o = _(r, U(i, o))),
                (a[n] = o),
                (s[n] = o - i));
          }
          t.modifiersData[e] = s;
        }
      },
      requiresIfExists: ['offset'],
    },
    oe = {
      name: 'arrow',
      enabled: !0,
      phase: 'main',
      fn: function (e) {
        var t,
          n = e.state,
          o = e.name,
          r = e.options,
          i = n.elements.arrow,
          a = n.modifiersData.popperOffsets,
          s = x(n.placement);
        if (
          ((e = L(s)),
          (s = 0 <= ['left', 'right'].indexOf(s) ? 'height' : 'width'),
          i && a)
        ) {
          r = M(
            'number' !=
              typeof (r =
                'function' == typeof (r = r.padding)
                  ? r(Object.assign({}, n.rects, { placement: n.placement }))
                  : r)
              ? r
              : k(r, C),
          );
          var f = d(i),
            p = 'y' === e ? 'top' : 'left',
            c = 'y' === e ? 'bottom' : 'right',
            l =
              n.rects.reference[s] +
              n.rects.reference[e] -
              a[e] -
              n.rects.popper[s];
          (a = a[e] - n.rects.reference[e]),
            (a =
              (i = (i = y(i))
                ? 'y' === e
                  ? i.clientHeight || 0
                  : i.clientWidth || 0
                : 0) /
                2 -
              f[s] / 2 +
              (l / 2 - a / 2)),
            (s = _(r[p], U(a, i - f[s] - r[c]))),
            (n.modifiersData[o] =
              (((t = {})[e] = s), (t.centerOffset = s - a), t));
        }
      },
      effect: function (e) {
        var t = e.state;
        if (
          null !=
          (e = void 0 === (e = e.options.element) ? '[data-popper-arrow]' : e)
        ) {
          if ('string' == typeof e && !(e = t.elements.popper.querySelector(e)))
            return;
          O(t.elements.popper, e) && (t.elements.arrow = e);
        }
      },
      requires: ['popperOffsets'],
      requiresIfExists: ['preventOverflow'],
    },
    re = {
      name: 'hide',
      enabled: !0,
      phase: 'main',
      requiresIfExists: ['preventOverflow'],
      fn: function (e) {
        var t = e.state;
        e = e.name;
        var n = t.rects.reference,
          o = t.rects.popper,
          r = t.modifiersData.preventOverflow,
          i = A(t, { elementContext: 'reference' }),
          a = A(t, { altBoundary: !0 });
        (n = S(i, n)),
          (o = S(a, o, r)),
          (r = q(n)),
          (a = q(o)),
          (t.modifiersData[e] = {
            referenceClippingOffsets: n,
            popperEscapeOffsets: o,
            isReferenceHidden: r,
            hasPopperEscaped: a,
          }),
          (t.attributes.popper = Object.assign({}, t.attributes.popper, {
            'data-popper-reference-hidden': r,
            'data-popper-escaped': a,
          }));
      },
    },
    ie = B({ defaultModifiers: [Y, G, K, Q] }),
    ae = [Y, G, K, Q, Z, te, ne, oe, re],
    se = B({ defaultModifiers: ae });
  (e.applyStyles = Q),
    (e.arrow = oe),
    (e.computeStyles = K),
    (e.createPopper = se),
    (e.createPopperLite = ie),
    (e.defaultModifiers = ae),
    (e.detectOverflow = A),
    (e.eventListeners = Y),
    (e.flip = te),
    (e.hide = re),
    (e.offset = Z),
    (e.popperGenerator = B),
    (e.popperOffsets = G),
    (e.preventOverflow = ne),
    Object.defineProperty(e, '__esModule', { value: !0 });
});
//# sourceMappingURL=popper.min.js.map

/**!
 * tippy.js v6.3.1
 * (c) 2017-2021 atomiks
 * MIT License
 */
(function (global, factory) {
  typeof exports === 'object' && typeof module !== 'undefined'
    ? (module.exports = factory(require('@popperjs/core')))
    : typeof define === 'function' && define.amd
    ? define(['@popperjs/core'], factory)
    : ((global = global || self), (global.tippy = factory(global.Popper)));
})(this, function (core) {
  'use strict';

  var css =
    '.tippy-box[data-animation=fade][data-state=hidden]{opacity:0}[data-tippy-root]{max-width:calc(100vw - 10px)}.tippy-box{position:relative;background-color:#333;color:#fff;border-radius:4px;font-size:14px;line-height:1.4;outline:0;transition-property:transform,visibility,opacity}.tippy-box[data-placement^=top]>.tippy-arrow{bottom:0}.tippy-box[data-placement^=top]>.tippy-arrow:before{bottom:-7px;left:0;border-width:8px 8px 0;border-top-color:initial;transform-origin:center top}.tippy-box[data-placement^=bottom]>.tippy-arrow{top:0}.tippy-box[data-placement^=bottom]>.tippy-arrow:before{top:-7px;left:0;border-width:0 8px 8px;border-bottom-color:initial;transform-origin:center bottom}.tippy-box[data-placement^=left]>.tippy-arrow{right:0}.tippy-box[data-placement^=left]>.tippy-arrow:before{border-width:8px 0 8px 8px;border-left-color:initial;right:-7px;transform-origin:center left}.tippy-box[data-placement^=right]>.tippy-arrow{left:0}.tippy-box[data-placement^=right]>.tippy-arrow:before{left:-7px;border-width:8px 8px 8px 0;border-right-color:initial;transform-origin:center right}.tippy-box[data-inertia][data-state=visible]{transition-timing-function:cubic-bezier(.54,1.5,.38,1.11)}.tippy-arrow{width:16px;height:16px;color:#333}.tippy-arrow:before{content:"";position:absolute;border-color:transparent;border-style:solid}.tippy-content{position:relative;padding:5px 9px;z-index:1}';

  function injectCSS(css) {
    var style = document.createElement('style');
    style.textContent = css;
    style.setAttribute('data-tippy-stylesheet', '');
    var head = document.head;
    var firstStyleOrLinkTag = document.querySelector('head>style,head>link');

    if (firstStyleOrLinkTag) {
      head.insertBefore(style, firstStyleOrLinkTag);
    } else {
      head.appendChild(style);
    }
  }

  var isBrowser =
    typeof window !== 'undefined' && typeof document !== 'undefined';
  var ua = isBrowser ? navigator.userAgent : '';
  var isIE = /MSIE |Trident\//.test(ua);

  var ROUND_ARROW =
    '<svg width="16" height="6" xmlns="http://www.w3.org/2000/svg"><path d="M0 6s1.796-.013 4.67-3.615C5.851.9 6.93.006 8 0c1.07-.006 2.148.887 3.343 2.385C14.233 6.005 16 6 16 6H0z"></svg>';
  var BOX_CLASS = 'tippy-box';
  var CONTENT_CLASS = 'tippy-content';
  var BACKDROP_CLASS = 'tippy-backdrop';
  var ARROW_CLASS = 'tippy-arrow';
  var SVG_ARROW_CLASS = 'tippy-svg-arrow';
  var TOUCH_OPTIONS = {
    passive: true,
    capture: true,
  };

  function hasOwnProperty(obj, key) {
    return {}.hasOwnProperty.call(obj, key);
  }
  function getValueAtIndexOrReturn(value, index, defaultValue) {
    if (Array.isArray(value)) {
      var v = value[index];
      return v == null
        ? Array.isArray(defaultValue)
          ? defaultValue[index]
          : defaultValue
        : v;
    }

    return value;
  }
  function isType(value, type) {
    var str = {}.toString.call(value);
    return str.indexOf('[object') === 0 && str.indexOf(type + ']') > -1;
  }
  function invokeWithArgsOrReturn(value, args) {
    return typeof value === 'function' ? value.apply(void 0, args) : value;
  }
  function debounce(fn, ms) {
    // Avoid wrapping in `setTimeout` if ms is 0 anyway
    if (ms === 0) {
      return fn;
    }

    var timeout;
    return function (arg) {
      clearTimeout(timeout);
      timeout = setTimeout(function () {
        fn(arg);
      }, ms);
    };
  }
  function removeProperties(obj, keys) {
    var clone = Object.assign({}, obj);
    keys.forEach(function (key) {
      delete clone[key];
    });
    return clone;
  }
  function splitBySpaces(value) {
    return value.split(/\s+/).filter(Boolean);
  }
  function normalizeToArray(value) {
    return [].concat(value);
  }
  function pushIfUnique(arr, value) {
    if (arr.indexOf(value) === -1) {
      arr.push(value);
    }
  }
  function unique(arr) {
    return arr.filter(function (item, index) {
      return arr.indexOf(item) === index;
    });
  }
  function getBasePlacement(placement) {
    return placement.split('-')[0];
  }
  function arrayFrom(value) {
    return [].slice.call(value);
  }
  function removeUndefinedProps(obj) {
    return Object.keys(obj).reduce(function (acc, key) {
      if (obj[key] !== undefined) {
        acc[key] = obj[key];
      }

      return acc;
    }, {});
  }

  function div() {
    return document.createElement('div');
  }
  function isElement(value) {
    return ['Element', 'Fragment'].some(function (type) {
      return isType(value, type);
    });
  }
  function isNodeList(value) {
    return isType(value, 'NodeList');
  }
  function isMouseEvent(value) {
    return isType(value, 'MouseEvent');
  }
  function isReferenceElement(value) {
    return !!(value && value._tippy && value._tippy.reference === value);
  }
  function getArrayOfElements(value) {
    if (isElement(value)) {
      return [value];
    }

    if (isNodeList(value)) {
      return arrayFrom(value);
    }

    if (Array.isArray(value)) {
      return value;
    }

    return arrayFrom(document.querySelectorAll(value));
  }
  function setTransitionDuration(els, value) {
    els.forEach(function (el) {
      if (el) {
        el.style.transitionDuration = value + 'ms';
      }
    });
  }
  function setVisibilityState(els, state) {
    els.forEach(function (el) {
      if (el) {
        el.setAttribute('data-state', state);
      }
    });
  }
  function getOwnerDocument(elementOrElements) {
    var _element$ownerDocumen;

    var _normalizeToArray = normalizeToArray(elementOrElements),
      element = _normalizeToArray[0]; // Elements created via a <template> have an ownerDocument with no reference to the body

    return (
      element == null
        ? void 0
        : (_element$ownerDocumen = element.ownerDocument) == null
        ? void 0
        : _element$ownerDocumen.body
    )
      ? element.ownerDocument
      : document;
  }
  function isCursorOutsideInteractiveBorder(popperTreeData, event) {
    var clientX = event.clientX,
      clientY = event.clientY;
    return popperTreeData.every(function (_ref) {
      var popperRect = _ref.popperRect,
        popperState = _ref.popperState,
        props = _ref.props;
      var interactiveBorder = props.interactiveBorder;
      var basePlacement = getBasePlacement(popperState.placement);
      var offsetData = popperState.modifiersData.offset;

      if (!offsetData) {
        return true;
      }

      var topDistance = basePlacement === 'bottom' ? offsetData.top.y : 0;
      var bottomDistance = basePlacement === 'top' ? offsetData.bottom.y : 0;
      var leftDistance = basePlacement === 'right' ? offsetData.left.x : 0;
      var rightDistance = basePlacement === 'left' ? offsetData.right.x : 0;
      var exceedsTop =
        popperRect.top - clientY + topDistance > interactiveBorder;
      var exceedsBottom =
        clientY - popperRect.bottom - bottomDistance > interactiveBorder;
      var exceedsLeft =
        popperRect.left - clientX + leftDistance > interactiveBorder;
      var exceedsRight =
        clientX - popperRect.right - rightDistance > interactiveBorder;
      return exceedsTop || exceedsBottom || exceedsLeft || exceedsRight;
    });
  }
  function updateTransitionEndListener(box, action, listener) {
    var method = action + 'EventListener'; // some browsers apparently support `transition` (unprefixed) but only fire
    // `webkitTransitionEnd`...

    ['transitionend', 'webkitTransitionEnd'].forEach(function (event) {
      box[method](event, listener);
    });
  }

  var currentInput = {
    isTouch: false,
  };
  var lastMouseMoveTime = 0;
  /**
   * When a `touchstart` event is fired, it's assumed the user is using touch
   * input. We'll bind a `mousemove` event listener to listen for mouse input in
   * the future. This way, the `isTouch` property is fully dynamic and will handle
   * hybrid devices that use a mix of touch + mouse input.
   */

  function onDocumentTouchStart() {
    if (currentInput.isTouch) {
      return;
    }

    currentInput.isTouch = true;

    if (window.performance) {
      document.addEventListener('mousemove', onDocumentMouseMove);
    }
  }
  /**
   * When two `mousemove` event are fired consecutively within 20ms, it's assumed
   * the user is using mouse input again. `mousemove` can fire on touch devices as
   * well, but very rarely that quickly.
   */

  function onDocumentMouseMove() {
    var now = performance.now();

    if (now - lastMouseMoveTime < 20) {
      currentInput.isTouch = false;
      document.removeEventListener('mousemove', onDocumentMouseMove);
    }

    lastMouseMoveTime = now;
  }
  /**
   * When an element is in focus and has a tippy, leaving the tab/window and
   * returning causes it to show again. For mouse users this is unexpected, but
   * for keyboard use it makes sense.
   * TODO: find a better technique to solve this problem
   */

  function onWindowBlur() {
    var activeElement = document.activeElement;

    if (isReferenceElement(activeElement)) {
      var instance = activeElement._tippy;

      if (activeElement.blur && !instance.state.isVisible) {
        activeElement.blur();
      }
    }
  }
  function bindGlobalEventListeners() {
    document.addEventListener(
      'touchstart',
      onDocumentTouchStart,
      TOUCH_OPTIONS,
    );
    window.addEventListener('blur', onWindowBlur);
  }

  function createMemoryLeakWarning(method) {
    var txt = method === 'destroy' ? 'n already-' : ' ';
    return [
      method +
        '() was called on a' +
        txt +
        'destroyed instance. This is a no-op but',
      'indicates a potential memory leak.',
    ].join(' ');
  }
  function clean(value) {
    var spacesAndTabs = /[ \t]{2,}/g;
    var lineStartWithSpaces = /^[ \t]*/gm;
    return value
      .replace(spacesAndTabs, ' ')
      .replace(lineStartWithSpaces, '')
      .trim();
  }

  function getDevMessage(message) {
    return clean(
      '\n  %ctippy.js\n\n  %c' +
        clean(message) +
        '\n\n  %c\uD83D\uDC77\u200D This is a development-only message. It will be removed in production.\n  ',
    );
  }

  function getFormattedMessage(message) {
    return [
      getDevMessage(message), // title
      'color: #00C584; font-size: 1.3em; font-weight: bold;', // message
      'line-height: 1.5', // footer
      'color: #a6a095;',
    ];
  } // Assume warnings and errors never have the same message

  var visitedMessages;

  {
    resetVisitedMessages();
  }

  function resetVisitedMessages() {
    visitedMessages = new Set();
  }
  function warnWhen(condition, message) {
    if (condition && !visitedMessages.has(message)) {
      var _console;

      visitedMessages.add(message);

      (_console = console).warn.apply(_console, getFormattedMessage(message));
    }
  }
  function errorWhen(condition, message) {
    if (condition && !visitedMessages.has(message)) {
      var _console2;

      visitedMessages.add(message);

      (_console2 = console).error.apply(
        _console2,
        getFormattedMessage(message),
      );
    }
  }
  function validateTargets(targets) {
    var didPassFalsyValue = !targets;
    var didPassPlainObject =
      Object.prototype.toString.call(targets) === '[object Object]' &&
      !targets.addEventListener;
    errorWhen(
      didPassFalsyValue,
      [
        'tippy() was passed',
        '`' + String(targets) + '`',
        'as its targets (first) argument. Valid types are: String, Element,',
        'Element[], or NodeList.',
      ].join(' '),
    );
    errorWhen(
      didPassPlainObject,
      [
        'tippy() was passed a plain object which is not supported as an argument',
        'for virtual positioning. Use props.getReferenceClientRect instead.',
      ].join(' '),
    );
  }

  var pluginProps = {
    animateFill: false,
    followCursor: false,
    inlinePositioning: false,
    sticky: false,
  };
  var renderProps = {
    allowHTML: false,
    animation: 'fade',
    arrow: true,
    content: '',
    inertia: false,
    maxWidth: 350,
    role: 'tooltip',
    theme: '',
    zIndex: 9999,
  };
  var defaultProps = Object.assign(
    {
      appendTo: function appendTo() {
        return document.body;
      },
      aria: {
        content: 'auto',
        expanded: 'auto',
      },
      delay: 0,
      duration: [300, 250],
      getReferenceClientRect: null,
      hideOnClick: true,
      ignoreAttributes: false,
      interactive: false,
      interactiveBorder: 2,
      interactiveDebounce: 0,
      moveTransition: '',
      offset: [0, 10],
      onAfterUpdate: function onAfterUpdate() {},
      onBeforeUpdate: function onBeforeUpdate() {},
      onCreate: function onCreate() {},
      onDestroy: function onDestroy() {},
      onHidden: function onHidden() {},
      onHide: function onHide() {},
      onMount: function onMount() {},
      onShow: function onShow() {},
      onShown: function onShown() {},
      onTrigger: function onTrigger() {},
      onUntrigger: function onUntrigger() {},
      onClickOutside: function onClickOutside() {},
      placement: 'top',
      plugins: [],
      popperOptions: {},
      render: null,
      showOnCreate: false,
      touch: true,
      trigger: 'mouseenter focus',
      triggerTarget: null,
    },
    pluginProps,
    {},
    renderProps,
  );
  var defaultKeys = Object.keys(defaultProps);
  var setDefaultProps = function setDefaultProps(partialProps) {
    /* istanbul ignore else */
    {
      validateProps(partialProps, []);
    }

    var keys = Object.keys(partialProps);
    keys.forEach(function (key) {
      defaultProps[key] = partialProps[key];
    });
  };
  function getExtendedPassedProps(passedProps) {
    var plugins = passedProps.plugins || [];
    var pluginProps = plugins.reduce(function (acc, plugin) {
      var name = plugin.name,
        defaultValue = plugin.defaultValue;

      if (name) {
        acc[name] =
          passedProps[name] !== undefined ? passedProps[name] : defaultValue;
      }

      return acc;
    }, {});
    return Object.assign({}, passedProps, {}, pluginProps);
  }
  function getDataAttributeProps(reference, plugins) {
    var propKeys = plugins
      ? Object.keys(
          getExtendedPassedProps(
            Object.assign({}, defaultProps, {
              plugins: plugins,
            }),
          ),
        )
      : defaultKeys;
    var props = propKeys.reduce(function (acc, key) {
      var valueAsString = (
        reference.getAttribute('data-tippy-' + key) || ''
      ).trim();

      if (!valueAsString) {
        return acc;
      }

      if (key === 'content') {
        acc[key] = valueAsString;
      } else {
        try {
          acc[key] = JSON.parse(valueAsString);
        } catch (e) {
          acc[key] = valueAsString;
        }
      }

      return acc;
    }, {});
    return props;
  }
  function evaluateProps(reference, props) {
    var out = Object.assign(
      {},
      props,
      {
        content: invokeWithArgsOrReturn(props.content, [reference]),
      },
      props.ignoreAttributes
        ? {}
        : getDataAttributeProps(reference, props.plugins),
    );
    out.aria = Object.assign({}, defaultProps.aria, {}, out.aria);
    out.aria = {
      expanded:
        out.aria.expanded === 'auto' ? props.interactive : out.aria.expanded,
      content:
        out.aria.content === 'auto'
          ? props.interactive
            ? null
            : 'describedby'
          : out.aria.content,
    };
    return out;
  }
  function validateProps(partialProps, plugins) {
    if (partialProps === void 0) {
      partialProps = {};
    }

    if (plugins === void 0) {
      plugins = [];
    }

    var keys = Object.keys(partialProps);
    keys.forEach(function (prop) {
      var nonPluginProps = removeProperties(
        defaultProps,
        Object.keys(pluginProps),
      );
      var didPassUnknownProp = !hasOwnProperty(nonPluginProps, prop); // Check if the prop exists in `plugins`

      if (didPassUnknownProp) {
        didPassUnknownProp =
          plugins.filter(function (plugin) {
            return plugin.name === prop;
          }).length === 0;
      }

      warnWhen(
        didPassUnknownProp,
        [
          '`' + prop + '`',
          "is not a valid prop. You may have spelled it incorrectly, or if it's",
          'a plugin, forgot to pass it in an array as props.plugins.',
          '\n\n',
          'All props: https://atomiks.github.io/tippyjs/v6/all-props/\n',
          'Plugins: https://atomiks.github.io/tippyjs/v6/plugins/',
        ].join(' '),
      );
    });
  }

  var innerHTML = function innerHTML() {
    return 'innerHTML';
  };

  function dangerouslySetInnerHTML(element, html) {
    element[innerHTML()] = html;
  }

  function createArrowElement(value) {
    var arrow = div();

    if (value === true) {
      arrow.className = ARROW_CLASS;
    } else {
      arrow.className = SVG_ARROW_CLASS;

      if (isElement(value)) {
        arrow.appendChild(value);
      } else {
        dangerouslySetInnerHTML(arrow, value);
      }
    }

    return arrow;
  }

  function setContent(content, props) {
    if (isElement(props.content)) {
      dangerouslySetInnerHTML(content, '');
      content.appendChild(props.content);
    } else if (typeof props.content !== 'function') {
      if (props.allowHTML) {
        dangerouslySetInnerHTML(content, props.content);
      } else {
        content.textContent = props.content;
      }
    }
  }
  function getChildren(popper) {
    var box = popper.firstElementChild;
    var boxChildren = arrayFrom(box.children);
    return {
      box: box,
      content: boxChildren.find(function (node) {
        return node.classList.contains(CONTENT_CLASS);
      }),
      arrow: boxChildren.find(function (node) {
        return (
          node.classList.contains(ARROW_CLASS) ||
          node.classList.contains(SVG_ARROW_CLASS)
        );
      }),
      backdrop: boxChildren.find(function (node) {
        return node.classList.contains(BACKDROP_CLASS);
      }),
    };
  }
  function render(instance) {
    var popper = div();
    var box = div();
    box.className = BOX_CLASS;
    box.setAttribute('data-state', 'hidden');
    box.setAttribute('tabindex', '-1');
    var content = div();
    content.className = CONTENT_CLASS;
    content.setAttribute('data-state', 'hidden');
    setContent(content, instance.props);
    popper.appendChild(box);
    box.appendChild(content);
    onUpdate(instance.props, instance.props);

    function onUpdate(prevProps, nextProps) {
      var _getChildren = getChildren(popper),
        box = _getChildren.box,
        content = _getChildren.content,
        arrow = _getChildren.arrow;

      if (nextProps.theme) {
        box.setAttribute('data-theme', nextProps.theme);
      } else {
        box.removeAttribute('data-theme');
      }

      if (typeof nextProps.animation === 'string') {
        box.setAttribute('data-animation', nextProps.animation);
      } else {
        box.removeAttribute('data-animation');
      }

      if (nextProps.inertia) {
        box.setAttribute('data-inertia', '');
      } else {
        box.removeAttribute('data-inertia');
      }

      box.style.maxWidth =
        typeof nextProps.maxWidth === 'number'
          ? nextProps.maxWidth + 'px'
          : nextProps.maxWidth;

      if (nextProps.role) {
        box.setAttribute('role', nextProps.role);
      } else {
        box.removeAttribute('role');
      }

      if (
        prevProps.content !== nextProps.content ||
        prevProps.allowHTML !== nextProps.allowHTML
      ) {
        setContent(content, instance.props);
      }

      if (nextProps.arrow) {
        if (!arrow) {
          box.appendChild(createArrowElement(nextProps.arrow));
        } else if (prevProps.arrow !== nextProps.arrow) {
          box.removeChild(arrow);
          box.appendChild(createArrowElement(nextProps.arrow));
        }
      } else if (arrow) {
        box.removeChild(arrow);
      }
    }

    return {
      popper: popper,
      onUpdate: onUpdate,
    };
  } // Runtime check to identify if the render function is the default one; this
  // way we can apply default CSS transitions logic and it can be tree-shaken away

  render.$$tippy = true;

  var idCounter = 1;
  var mouseMoveListeners = []; // Used by `hideAll()`

  var mountedInstances = [];
  function createTippy(reference, passedProps) {
    var props = evaluateProps(
      reference,
      Object.assign(
        {},
        defaultProps,
        {},
        getExtendedPassedProps(removeUndefinedProps(passedProps)),
      ),
    ); // ===========================================================================
    // 🔒 Private members
    // ===========================================================================

    var showTimeout;
    var hideTimeout;
    var scheduleHideAnimationFrame;
    var isVisibleFromClick = false;
    var didHideDueToDocumentMouseDown = false;
    var didTouchMove = false;
    var ignoreOnFirstUpdate = false;
    var lastTriggerEvent;
    var currentTransitionEndListener;
    var onFirstUpdate;
    var listeners = [];
    var debouncedOnMouseMove = debounce(onMouseMove, props.interactiveDebounce);
    var currentTarget; // ===========================================================================
    // 🔑 Public members
    // ===========================================================================

    var id = idCounter++;
    var popperInstance = null;
    var plugins = unique(props.plugins);
    var state = {
      // Is the instance currently enabled?
      isEnabled: true,
      // Is the tippy currently showing and not transitioning out?
      isVisible: false,
      // Has the instance been destroyed?
      isDestroyed: false,
      // Is the tippy currently mounted to the DOM?
      isMounted: false,
      // Has the tippy finished transitioning in?
      isShown: false,
    };
    var instance = {
      // properties
      id: id,
      reference: reference,
      popper: div(),
      popperInstance: popperInstance,
      props: props,
      state: state,
      plugins: plugins,
      // methods
      clearDelayTimeouts: clearDelayTimeouts,
      setProps: setProps,
      setContent: setContent,
      show: show,
      hide: hide,
      hideWithInteractivity: hideWithInteractivity,
      enable: enable,
      disable: disable,
      unmount: unmount,
      destroy: destroy,
    }; // TODO: Investigate why this early return causes a TDZ error in the tests —
    // it doesn't seem to happen in the browser

    /* istanbul ignore if */

    if (!props.render) {
      {
        errorWhen(true, 'render() function has not been supplied.');
      }

      return instance;
    } // ===========================================================================
    // Initial mutations
    // ===========================================================================

    var _props$render = props.render(instance),
      popper = _props$render.popper,
      onUpdate = _props$render.onUpdate;

    popper.setAttribute('data-tippy-root', '');
    popper.id = 'tippy-' + instance.id;
    instance.popper = popper;
    reference._tippy = instance;
    popper._tippy = instance;
    var pluginsHooks = plugins.map(function (plugin) {
      return plugin.fn(instance);
    });
    var hasAriaExpanded = reference.hasAttribute('aria-expanded');
    addListeners();
    handleAriaExpandedAttribute();
    handleStyles();
    invokeHook('onCreate', [instance]);

    if (props.showOnCreate) {
      scheduleShow();
    } // Prevent a tippy with a delay from hiding if the cursor left then returned
    // before it started hiding

    popper.addEventListener('mouseenter', function () {
      if (instance.props.interactive && instance.state.isVisible) {
        instance.clearDelayTimeouts();
      }
    });
    popper.addEventListener('mouseleave', function (event) {
      if (
        instance.props.interactive &&
        instance.props.trigger.indexOf('mouseenter') >= 0
      ) {
        getDocument().addEventListener('mousemove', debouncedOnMouseMove);
        debouncedOnMouseMove(event);
      }
    });
    return instance; // ===========================================================================
    // 🔒 Private methods
    // ===========================================================================

    function getNormalizedTouchSettings() {
      var touch = instance.props.touch;
      return Array.isArray(touch) ? touch : [touch, 0];
    }

    function getIsCustomTouchBehavior() {
      return getNormalizedTouchSettings()[0] === 'hold';
    }

    function getIsDefaultRenderFn() {
      var _instance$props$rende;

      // @ts-ignore
      return !!((_instance$props$rende = instance.props.render) == null
        ? void 0
        : _instance$props$rende.$$tippy);
    }

    function getCurrentTarget() {
      return currentTarget || reference;
    }

    function getDocument() {
      var parent = getCurrentTarget().parentNode;
      return parent ? getOwnerDocument(parent) : document;
    }

    function getDefaultTemplateChildren() {
      return getChildren(popper);
    }

    function getDelay(isShow) {
      // For touch or keyboard input, force `0` delay for UX reasons
      // Also if the instance is mounted but not visible (transitioning out),
      // ignore delay
      if (
        (instance.state.isMounted && !instance.state.isVisible) ||
        currentInput.isTouch ||
        (lastTriggerEvent && lastTriggerEvent.type === 'focus')
      ) {
        return 0;
      }

      return getValueAtIndexOrReturn(
        instance.props.delay,
        isShow ? 0 : 1,
        defaultProps.delay,
      );
    }

    function handleStyles() {
      popper.style.pointerEvents =
        instance.props.interactive && instance.state.isVisible ? '' : 'none';
      popper.style.zIndex = '' + instance.props.zIndex;
    }

    function invokeHook(hook, args, shouldInvokePropsHook) {
      if (shouldInvokePropsHook === void 0) {
        shouldInvokePropsHook = true;
      }

      pluginsHooks.forEach(function (pluginHooks) {
        if (pluginHooks[hook]) {
          pluginHooks[hook].apply(void 0, args);
        }
      });

      if (shouldInvokePropsHook) {
        var _instance$props;

        (_instance$props = instance.props)[hook].apply(_instance$props, args);
      }
    }

    function handleAriaContentAttribute() {
      var aria = instance.props.aria;

      if (!aria.content) {
        return;
      }

      var attr = 'aria-' + aria.content;
      var id = popper.id;
      var nodes = normalizeToArray(instance.props.triggerTarget || reference);
      nodes.forEach(function (node) {
        var currentValue = node.getAttribute(attr);

        if (instance.state.isVisible) {
          node.setAttribute(attr, currentValue ? currentValue + ' ' + id : id);
        } else {
          var nextValue = currentValue && currentValue.replace(id, '').trim();

          if (nextValue) {
            node.setAttribute(attr, nextValue);
          } else {
            node.removeAttribute(attr);
          }
        }
      });
    }

    function handleAriaExpandedAttribute() {
      if (hasAriaExpanded || !instance.props.aria.expanded) {
        return;
      }

      var nodes = normalizeToArray(instance.props.triggerTarget || reference);
      nodes.forEach(function (node) {
        if (instance.props.interactive) {
          node.setAttribute(
            'aria-expanded',
            instance.state.isVisible && node === getCurrentTarget()
              ? 'true'
              : 'false',
          );
        } else {
          node.removeAttribute('aria-expanded');
        }
      });
    }

    function cleanupInteractiveMouseListeners() {
      getDocument().removeEventListener('mousemove', debouncedOnMouseMove);
      mouseMoveListeners = mouseMoveListeners.filter(function (listener) {
        return listener !== debouncedOnMouseMove;
      });
    }

    function onDocumentPress(event) {
      // Moved finger to scroll instead of an intentional tap outside
      if (currentInput.isTouch) {
        if (didTouchMove || event.type === 'mousedown') {
          return;
        }
      } // Clicked on interactive popper

      if (instance.props.interactive && popper.contains(event.target)) {
        return;
      } // Clicked on the event listeners target

      if (getCurrentTarget().contains(event.target)) {
        if (currentInput.isTouch) {
          return;
        }

        if (
          instance.state.isVisible &&
          instance.props.trigger.indexOf('click') >= 0
        ) {
          return;
        }
      } else {
        invokeHook('onClickOutside', [instance, event]);
      }

      if (instance.props.hideOnClick === true) {
        instance.clearDelayTimeouts();
        instance.hide(); // `mousedown` event is fired right before `focus` if pressing the
        // currentTarget. This lets a tippy with `focus` trigger know that it
        // should not show

        didHideDueToDocumentMouseDown = true;
        setTimeout(function () {
          didHideDueToDocumentMouseDown = false;
        }); // The listener gets added in `scheduleShow()`, but this may be hiding it
        // before it shows, and hide()'s early bail-out behavior can prevent it
        // from being cleaned up

        if (!instance.state.isMounted) {
          removeDocumentPress();
        }
      }
    }

    function onTouchMove() {
      didTouchMove = true;
    }

    function onTouchStart() {
      didTouchMove = false;
    }

    function addDocumentPress() {
      var doc = getDocument();
      doc.addEventListener('mousedown', onDocumentPress, true);
      doc.addEventListener('touchend', onDocumentPress, TOUCH_OPTIONS);
      doc.addEventListener('touchstart', onTouchStart, TOUCH_OPTIONS);
      doc.addEventListener('touchmove', onTouchMove, TOUCH_OPTIONS);
    }

    function removeDocumentPress() {
      var doc = getDocument();
      doc.removeEventListener('mousedown', onDocumentPress, true);
      doc.removeEventListener('touchend', onDocumentPress, TOUCH_OPTIONS);
      doc.removeEventListener('touchstart', onTouchStart, TOUCH_OPTIONS);
      doc.removeEventListener('touchmove', onTouchMove, TOUCH_OPTIONS);
    }

    function onTransitionedOut(duration, callback) {
      onTransitionEnd(duration, function () {
        if (
          !instance.state.isVisible &&
          popper.parentNode &&
          popper.parentNode.contains(popper)
        ) {
          callback();
        }
      });
    }

    function onTransitionedIn(duration, callback) {
      onTransitionEnd(duration, callback);
    }

    function onTransitionEnd(duration, callback) {
      var box = getDefaultTemplateChildren().box;

      function listener(event) {
        if (event.target === box) {
          updateTransitionEndListener(box, 'remove', listener);
          callback();
        }
      } // Make callback synchronous if duration is 0
      // `transitionend` won't fire otherwise

      if (duration === 0) {
        return callback();
      }

      updateTransitionEndListener(box, 'remove', currentTransitionEndListener);
      updateTransitionEndListener(box, 'add', listener);
      currentTransitionEndListener = listener;
    }

    function on(eventType, handler, options) {
      if (options === void 0) {
        options = false;
      }

      var nodes = normalizeToArray(instance.props.triggerTarget || reference);
      nodes.forEach(function (node) {
        node.addEventListener(eventType, handler, options);
        listeners.push({
          node: node,
          eventType: eventType,
          handler: handler,
          options: options,
        });
      });
    }

    function addListeners() {
      if (getIsCustomTouchBehavior()) {
        on('touchstart', onTrigger, {
          passive: true,
        });
        on('touchend', onMouseLeave, {
          passive: true,
        });
      }

      splitBySpaces(instance.props.trigger).forEach(function (eventType) {
        if (eventType === 'manual') {
          return;
        }

        on(eventType, onTrigger);

        switch (eventType) {
          case 'mouseenter':
            on('mouseleave', onMouseLeave);
            break;

          case 'focus':
            on(isIE ? 'focusout' : 'blur', onBlurOrFocusOut);
            break;

          case 'focusin':
            on('focusout', onBlurOrFocusOut);
            break;
        }
      });
    }

    function removeListeners() {
      listeners.forEach(function (_ref) {
        var node = _ref.node,
          eventType = _ref.eventType,
          handler = _ref.handler,
          options = _ref.options;
        node.removeEventListener(eventType, handler, options);
      });
      listeners = [];
    }

    function onTrigger(event) {
      var _lastTriggerEvent;

      var shouldScheduleClickHide = false;

      if (
        !instance.state.isEnabled ||
        isEventListenerStopped(event) ||
        didHideDueToDocumentMouseDown
      ) {
        return;
      }

      var wasFocused =
        ((_lastTriggerEvent = lastTriggerEvent) == null
          ? void 0
          : _lastTriggerEvent.type) === 'focus';
      lastTriggerEvent = event;
      currentTarget = event.currentTarget;
      handleAriaExpandedAttribute();

      if (!instance.state.isVisible && isMouseEvent(event)) {
        // If scrolling, `mouseenter` events can be fired if the cursor lands
        // over a new target, but `mousemove` events don't get fired. This
        // causes interactive tooltips to get stuck open until the cursor is
        // moved
        mouseMoveListeners.forEach(function (listener) {
          return listener(event);
        });
      } // Toggle show/hide when clicking click-triggered tooltips

      if (
        event.type === 'click' &&
        (instance.props.trigger.indexOf('mouseenter') < 0 ||
          isVisibleFromClick) &&
        instance.props.hideOnClick !== false &&
        instance.state.isVisible
      ) {
        shouldScheduleClickHide = true;
      } else {
        scheduleShow(event);
      }

      if (event.type === 'click') {
        isVisibleFromClick = !shouldScheduleClickHide;
      }

      if (shouldScheduleClickHide && !wasFocused) {
        scheduleHide(event);
      }
    }

    function onMouseMove(event) {
      var target = event.target;
      var isCursorOverReferenceOrPopper =
        getCurrentTarget().contains(target) || popper.contains(target);

      if (event.type === 'mousemove' && isCursorOverReferenceOrPopper) {
        return;
      }

      var popperTreeData = getNestedPopperTree()
        .concat(popper)
        .map(function (popper) {
          var _instance$popperInsta;

          var instance = popper._tippy;
          var state =
            (_instance$popperInsta = instance.popperInstance) == null
              ? void 0
              : _instance$popperInsta.state;

          if (state) {
            return {
              popperRect: popper.getBoundingClientRect(),
              popperState: state,
              props: props,
            };
          }

          return null;
        })
        .filter(Boolean);

      if (isCursorOutsideInteractiveBorder(popperTreeData, event)) {
        cleanupInteractiveMouseListeners();
        scheduleHide(event);
      }
    }

    function onMouseLeave(event) {
      var shouldBail =
        isEventListenerStopped(event) ||
        (instance.props.trigger.indexOf('click') >= 0 && isVisibleFromClick);

      if (shouldBail) {
        return;
      }

      if (instance.props.interactive) {
        instance.hideWithInteractivity(event);
        return;
      }

      scheduleHide(event);
    }

    function onBlurOrFocusOut(event) {
      if (
        instance.props.trigger.indexOf('focusin') < 0 &&
        event.target !== getCurrentTarget()
      ) {
        return;
      } // If focus was moved to within the popper

      if (
        instance.props.interactive &&
        event.relatedTarget &&
        popper.contains(event.relatedTarget)
      ) {
        return;
      }

      scheduleHide(event);
    }

    function isEventListenerStopped(event) {
      return currentInput.isTouch
        ? getIsCustomTouchBehavior() !== event.type.indexOf('touch') >= 0
        : false;
    }

    function createPopperInstance() {
      destroyPopperInstance();
      var _instance$props2 = instance.props,
        popperOptions = _instance$props2.popperOptions,
        placement = _instance$props2.placement,
        offset = _instance$props2.offset,
        getReferenceClientRect = _instance$props2.getReferenceClientRect,
        moveTransition = _instance$props2.moveTransition;
      var arrow = getIsDefaultRenderFn() ? getChildren(popper).arrow : null;
      var computedReference = getReferenceClientRect
        ? {
            getBoundingClientRect: getReferenceClientRect,
            contextElement:
              getReferenceClientRect.contextElement || getCurrentTarget(),
          }
        : reference;
      var tippyModifier = {
        name: '$$tippy',
        enabled: true,
        phase: 'beforeWrite',
        requires: ['computeStyles'],
        fn: function fn(_ref2) {
          var state = _ref2.state;

          if (getIsDefaultRenderFn()) {
            var _getDefaultTemplateCh = getDefaultTemplateChildren(),
              box = _getDefaultTemplateCh.box;

            ['placement', 'reference-hidden', 'escaped'].forEach(function (
              attr,
            ) {
              if (attr === 'placement') {
                box.setAttribute('data-placement', state.placement);
              } else {
                if (state.attributes.popper['data-popper-' + attr]) {
                  box.setAttribute('data-' + attr, '');
                } else {
                  box.removeAttribute('data-' + attr);
                }
              }
            });
            state.attributes.popper = {};
          }
        },
      };
      var modifiers = [
        {
          name: 'offset',
          options: {
            offset: offset,
          },
        },
        {
          name: 'preventOverflow',
          options: {
            padding: {
              top: 2,
              bottom: 2,
              left: 5,
              right: 5,
            },
          },
        },
        {
          name: 'flip',
          options: {
            padding: 5,
          },
        },
        {
          name: 'computeStyles',
          options: {
            adaptive: !moveTransition,
          },
        },
        tippyModifier,
      ];

      if (getIsDefaultRenderFn() && arrow) {
        modifiers.push({
          name: 'arrow',
          options: {
            element: arrow,
            padding: 3,
          },
        });
      }

      modifiers.push.apply(
        modifiers,
        (popperOptions == null ? void 0 : popperOptions.modifiers) || [],
      );
      instance.popperInstance = core.createPopper(
        computedReference,
        popper,
        Object.assign({}, popperOptions, {
          placement: placement,
          onFirstUpdate: onFirstUpdate,
          modifiers: modifiers,
        }),
      );
    }

    function destroyPopperInstance() {
      if (instance.popperInstance) {
        instance.popperInstance.destroy();
        instance.popperInstance = null;
      }
    }

    function mount() {
      var appendTo = instance.props.appendTo;
      var parentNode; // By default, we'll append the popper to the triggerTargets's parentNode so
      // it's directly after the reference element so the elements inside the
      // tippy can be tabbed to
      // If there are clipping issues, the user can specify a different appendTo
      // and ensure focus management is handled correctly manually

      var node = getCurrentTarget();

      if (
        (instance.props.interactive && appendTo === defaultProps.appendTo) ||
        appendTo === 'parent'
      ) {
        parentNode = node.parentNode;
      } else {
        parentNode = invokeWithArgsOrReturn(appendTo, [node]);
      } // The popper element needs to exist on the DOM before its position can be
      // updated as Popper needs to read its dimensions

      if (!parentNode.contains(popper)) {
        parentNode.appendChild(popper);
      }

      createPopperInstance();
      /* istanbul ignore else */

      {
        // Accessibility check
        warnWhen(
          instance.props.interactive &&
            appendTo === defaultProps.appendTo &&
            node.nextElementSibling !== popper,
          [
            'Interactive tippy element may not be accessible via keyboard',
            'navigation because it is not directly after the reference element',
            'in the DOM source order.',
            '\n\n',
            'Using a wrapper <div> or <span> tag around the reference element',
            'solves this by creating a new parentNode context.',
            '\n\n',
            'Specifying `appendTo: document.body` silences this warning, but it',
            'assumes you are using a focus management solution to handle',
            'keyboard navigation.',
            '\n\n',
            'See: https://atomiks.github.io/tippyjs/v6/accessibility/#interactivity',
          ].join(' '),
        );
      }
    }

    function getNestedPopperTree() {
      return arrayFrom(popper.querySelectorAll('[data-tippy-root]'));
    }

    function scheduleShow(event) {
      instance.clearDelayTimeouts();

      if (event) {
        invokeHook('onTrigger', [instance, event]);
      }

      addDocumentPress();
      var delay = getDelay(true);

      var _getNormalizedTouchSe = getNormalizedTouchSettings(),
        touchValue = _getNormalizedTouchSe[0],
        touchDelay = _getNormalizedTouchSe[1];

      if (currentInput.isTouch && touchValue === 'hold' && touchDelay) {
        delay = touchDelay;
      }

      if (delay) {
        showTimeout = setTimeout(function () {
          instance.show();
        }, delay);
      } else {
        instance.show();
      }
    }

    function scheduleHide(event) {
      instance.clearDelayTimeouts();
      invokeHook('onUntrigger', [instance, event]);

      if (!instance.state.isVisible) {
        removeDocumentPress();
        return;
      } // For interactive tippies, scheduleHide is added to a document.body handler
      // from onMouseLeave so must intercept scheduled hides from mousemove/leave
      // events when trigger contains mouseenter and click, and the tip is
      // currently shown as a result of a click.

      if (
        instance.props.trigger.indexOf('mouseenter') >= 0 &&
        instance.props.trigger.indexOf('click') >= 0 &&
        ['mouseleave', 'mousemove'].indexOf(event.type) >= 0 &&
        isVisibleFromClick
      ) {
        return;
      }

      var delay = getDelay(false);

      if (delay) {
        hideTimeout = setTimeout(function () {
          if (instance.state.isVisible) {
            instance.hide();
          }
        }, delay);
      } else {
        // Fixes a `transitionend` problem when it fires 1 frame too
        // late sometimes, we don't want hide() to be called.
        scheduleHideAnimationFrame = requestAnimationFrame(function () {
          instance.hide();
        });
      }
    } // ===========================================================================
    // 🔑 Public methods
    // ===========================================================================

    function enable() {
      instance.state.isEnabled = true;
    }

    function disable() {
      // Disabling the instance should also hide it
      // https://github.com/atomiks/tippy.js-react/issues/106
      instance.hide();
      instance.state.isEnabled = false;
    }

    function clearDelayTimeouts() {
      clearTimeout(showTimeout);
      clearTimeout(hideTimeout);
      cancelAnimationFrame(scheduleHideAnimationFrame);
    }

    function setProps(partialProps) {
      /* istanbul ignore else */
      {
        warnWhen(
          instance.state.isDestroyed,
          createMemoryLeakWarning('setProps'),
        );
      }

      if (instance.state.isDestroyed) {
        return;
      }

      invokeHook('onBeforeUpdate', [instance, partialProps]);
      removeListeners();
      var prevProps = instance.props;
      var nextProps = evaluateProps(
        reference,
        Object.assign({}, instance.props, {}, partialProps, {
          ignoreAttributes: true,
        }),
      );
      instance.props = nextProps;
      addListeners();

      if (prevProps.interactiveDebounce !== nextProps.interactiveDebounce) {
        cleanupInteractiveMouseListeners();
        debouncedOnMouseMove = debounce(
          onMouseMove,
          nextProps.interactiveDebounce,
        );
      } // Ensure stale aria-expanded attributes are removed

      if (prevProps.triggerTarget && !nextProps.triggerTarget) {
        normalizeToArray(prevProps.triggerTarget).forEach(function (node) {
          node.removeAttribute('aria-expanded');
        });
      } else if (nextProps.triggerTarget) {
        reference.removeAttribute('aria-expanded');
      }

      handleAriaExpandedAttribute();
      handleStyles();

      if (onUpdate) {
        onUpdate(prevProps, nextProps);
      }

      if (instance.popperInstance) {
        createPopperInstance(); // Fixes an issue with nested tippies if they are all getting re-rendered,
        // and the nested ones get re-rendered first.
        // https://github.com/atomiks/tippyjs-react/issues/177
        // TODO: find a cleaner / more efficient solution(!)

        getNestedPopperTree().forEach(function (nestedPopper) {
          // React (and other UI libs likely) requires a rAF wrapper as it flushes
          // its work in one
          requestAnimationFrame(nestedPopper._tippy.popperInstance.forceUpdate);
        });
      }

      invokeHook('onAfterUpdate', [instance, partialProps]);
    }

    function setContent(content) {
      instance.setProps({
        content: content,
      });
    }

    function show() {
      /* istanbul ignore else */
      {
        warnWhen(instance.state.isDestroyed, createMemoryLeakWarning('show'));
      } // Early bail-out

      var isAlreadyVisible = instance.state.isVisible;
      var isDestroyed = instance.state.isDestroyed;
      var isDisabled = !instance.state.isEnabled;
      var isTouchAndTouchDisabled =
        currentInput.isTouch && !instance.props.touch;
      var duration = getValueAtIndexOrReturn(
        instance.props.duration,
        0,
        defaultProps.duration,
      );

      if (
        isAlreadyVisible ||
        isDestroyed ||
        isDisabled ||
        isTouchAndTouchDisabled
      ) {
        return;
      } // Normalize `disabled` behavior across browsers.
      // Firefox allows events on disabled elements, but Chrome doesn't.
      // Using a wrapper element (i.e. <span>) is recommended.

      if (getCurrentTarget().hasAttribute('disabled')) {
        return;
      }

      invokeHook('onShow', [instance], false);

      if (instance.props.onShow(instance) === false) {
        return;
      }

      instance.state.isVisible = true;

      if (getIsDefaultRenderFn()) {
        popper.style.visibility = 'visible';
      }

      handleStyles();
      addDocumentPress();

      if (!instance.state.isMounted) {
        popper.style.transition = 'none';
      } // If flipping to the opposite side after hiding at least once, the
      // animation will use the wrong placement without resetting the duration

      if (getIsDefaultRenderFn()) {
        var _getDefaultTemplateCh2 = getDefaultTemplateChildren(),
          box = _getDefaultTemplateCh2.box,
          content = _getDefaultTemplateCh2.content;

        setTransitionDuration([box, content], 0);
      }

      onFirstUpdate = function onFirstUpdate() {
        var _instance$popperInsta2;

        if (!instance.state.isVisible || ignoreOnFirstUpdate) {
          return;
        }

        ignoreOnFirstUpdate = true; // reflow

        void popper.offsetHeight;
        popper.style.transition = instance.props.moveTransition;

        if (getIsDefaultRenderFn() && instance.props.animation) {
          var _getDefaultTemplateCh3 = getDefaultTemplateChildren(),
            _box = _getDefaultTemplateCh3.box,
            _content = _getDefaultTemplateCh3.content;

          setTransitionDuration([_box, _content], duration);
          setVisibilityState([_box, _content], 'visible');
        }

        handleAriaContentAttribute();
        handleAriaExpandedAttribute();
        pushIfUnique(mountedInstances, instance); // certain modifiers (e.g. `maxSize`) require a second update after the
        // popper has been positioned for the first time

        (_instance$popperInsta2 = instance.popperInstance) == null
          ? void 0
          : _instance$popperInsta2.forceUpdate();
        instance.state.isMounted = true;
        invokeHook('onMount', [instance]);

        if (instance.props.animation && getIsDefaultRenderFn()) {
          onTransitionedIn(duration, function () {
            instance.state.isShown = true;
            invokeHook('onShown', [instance]);
          });
        }
      };

      mount();
    }

    function hide() {
      /* istanbul ignore else */
      {
        warnWhen(instance.state.isDestroyed, createMemoryLeakWarning('hide'));
      } // Early bail-out

      var isAlreadyHidden = !instance.state.isVisible;
      var isDestroyed = instance.state.isDestroyed;
      var isDisabled = !instance.state.isEnabled;
      var duration = getValueAtIndexOrReturn(
        instance.props.duration,
        1,
        defaultProps.duration,
      );

      if (isAlreadyHidden || isDestroyed || isDisabled) {
        return;
      }

      invokeHook('onHide', [instance], false);

      if (instance.props.onHide(instance) === false) {
        return;
      }

      instance.state.isVisible = false;
      instance.state.isShown = false;
      ignoreOnFirstUpdate = false;
      isVisibleFromClick = false;

      if (getIsDefaultRenderFn()) {
        popper.style.visibility = 'hidden';
      }

      cleanupInteractiveMouseListeners();
      removeDocumentPress();
      handleStyles();

      if (getIsDefaultRenderFn()) {
        var _getDefaultTemplateCh4 = getDefaultTemplateChildren(),
          box = _getDefaultTemplateCh4.box,
          content = _getDefaultTemplateCh4.content;

        if (instance.props.animation) {
          setTransitionDuration([box, content], duration);
          setVisibilityState([box, content], 'hidden');
        }
      }

      handleAriaContentAttribute();
      handleAriaExpandedAttribute();

      if (instance.props.animation) {
        if (getIsDefaultRenderFn()) {
          onTransitionedOut(duration, instance.unmount);
        }
      } else {
        instance.unmount();
      }
    }

    function hideWithInteractivity(event) {
      /* istanbul ignore else */
      {
        warnWhen(
          instance.state.isDestroyed,
          createMemoryLeakWarning('hideWithInteractivity'),
        );
      }

      getDocument().addEventListener('mousemove', debouncedOnMouseMove);
      pushIfUnique(mouseMoveListeners, debouncedOnMouseMove);
      debouncedOnMouseMove(event);
    }

    function unmount() {
      /* istanbul ignore else */
      {
        warnWhen(
          instance.state.isDestroyed,
          createMemoryLeakWarning('unmount'),
        );
      }

      if (instance.state.isVisible) {
        instance.hide();
      }

      if (!instance.state.isMounted) {
        return;
      }

      destroyPopperInstance(); // If a popper is not interactive, it will be appended outside the popper
      // tree by default. This seems mainly for interactive tippies, but we should
      // find a workaround if possible

      getNestedPopperTree().forEach(function (nestedPopper) {
        nestedPopper._tippy.unmount();
      });

      if (popper.parentNode) {
        popper.parentNode.removeChild(popper);
      }

      mountedInstances = mountedInstances.filter(function (i) {
        return i !== instance;
      });
      instance.state.isMounted = false;
      invokeHook('onHidden', [instance]);
    }

    function destroy() {
      /* istanbul ignore else */
      {
        warnWhen(
          instance.state.isDestroyed,
          createMemoryLeakWarning('destroy'),
        );
      }

      if (instance.state.isDestroyed) {
        return;
      }

      instance.clearDelayTimeouts();
      instance.unmount();
      removeListeners();
      delete reference._tippy;
      instance.state.isDestroyed = true;
      invokeHook('onDestroy', [instance]);
    }
  }

  function tippy(targets, optionalProps) {
    if (optionalProps === void 0) {
      optionalProps = {};
    }

    var plugins = defaultProps.plugins.concat(optionalProps.plugins || []);
    /* istanbul ignore else */

    {
      validateTargets(targets);
      validateProps(optionalProps, plugins);
    }

    bindGlobalEventListeners();
    var passedProps = Object.assign({}, optionalProps, {
      plugins: plugins,
    });
    var elements = getArrayOfElements(targets);
    /* istanbul ignore else */

    {
      var isSingleContentElement = isElement(passedProps.content);
      var isMoreThanOneReferenceElement = elements.length > 1;
      warnWhen(
        isSingleContentElement && isMoreThanOneReferenceElement,
        [
          'tippy() was passed an Element as the `content` prop, but more than',
          'one tippy instance was created by this invocation. This means the',
          'content element will only be appended to the last tippy instance.',
          '\n\n',
          'Instead, pass the .innerHTML of the element, or use a function that',
          'returns a cloned version of the element instead.',
          '\n\n',
          '1) content: element.innerHTML\n',
          '2) content: () => element.cloneNode(true)',
        ].join(' '),
      );
    }

    var instances = elements.reduce(function (acc, reference) {
      var instance = reference && createTippy(reference, passedProps);

      if (instance) {
        acc.push(instance);
      }

      return acc;
    }, []);
    return isElement(targets) ? instances[0] : instances;
  }

  tippy.defaultProps = defaultProps;
  tippy.setDefaultProps = setDefaultProps;
  tippy.currentInput = currentInput;
  var hideAll = function hideAll(_temp) {
    var _ref = _temp === void 0 ? {} : _temp,
      excludedReferenceOrInstance = _ref.exclude,
      duration = _ref.duration;

    mountedInstances.forEach(function (instance) {
      var isExcluded = false;

      if (excludedReferenceOrInstance) {
        isExcluded = isReferenceElement(excludedReferenceOrInstance)
          ? instance.reference === excludedReferenceOrInstance
          : instance.popper === excludedReferenceOrInstance.popper;
      }

      if (!isExcluded) {
        var originalDuration = instance.props.duration;
        instance.setProps({
          duration: duration,
        });
        instance.hide();

        if (!instance.state.isDestroyed) {
          instance.setProps({
            duration: originalDuration,
          });
        }
      }
    });
  };

  // every time the popper is destroyed (i.e. a new target), removing the styles
  // and causing transitions to break for singletons when the console is open, but
  // most notably for non-transform styles being used, `gpuAcceleration: false`.

  var applyStylesModifier = Object.assign({}, core.applyStyles, {
    effect: function effect(_ref) {
      var state = _ref.state;
      var initialStyles = {
        popper: {
          position: state.options.strategy,
          left: '0',
          top: '0',
          margin: '0',
        },
        arrow: {
          position: 'absolute',
        },
        reference: {},
      };
      Object.assign(state.elements.popper.style, initialStyles.popper);
      state.styles = initialStyles;

      if (state.elements.arrow) {
        Object.assign(state.elements.arrow.style, initialStyles.arrow);
      } // intentionally return no cleanup function
      // return () => { ... }
    },
  });

  var createSingleton = function createSingleton(
    tippyInstances,
    optionalProps,
  ) {
    var _optionalProps$popper;

    if (optionalProps === void 0) {
      optionalProps = {};
    }

    /* istanbul ignore else */
    {
      errorWhen(
        !Array.isArray(tippyInstances),
        [
          'The first argument passed to createSingleton() must be an array of',
          'tippy instances. The passed value was',
          String(tippyInstances),
        ].join(' '),
      );
    }

    var individualInstances = tippyInstances;
    var references = [];
    var currentTarget;
    var overrides = optionalProps.overrides;
    var interceptSetPropsCleanups = [];
    var shownOnCreate = false;

    function setReferences() {
      references = individualInstances.map(function (instance) {
        return instance.reference;
      });
    }

    function enableInstances(isEnabled) {
      individualInstances.forEach(function (instance) {
        if (isEnabled) {
          instance.enable();
        } else {
          instance.disable();
        }
      });
    }

    function interceptSetProps(singleton) {
      return individualInstances.map(function (instance) {
        var originalSetProps = instance.setProps;

        instance.setProps = function (props) {
          originalSetProps(props);

          if (instance.reference === currentTarget) {
            singleton.setProps(props);
          }
        };

        return function () {
          instance.setProps = originalSetProps;
        };
      });
    } // have to pass singleton, as it maybe undefined on first call

    function prepareInstance(singleton, target) {
      var index = references.indexOf(target); // bail-out

      if (target === currentTarget) {
        return;
      }

      currentTarget = target;
      var overrideProps = (overrides || [])
        .concat('content')
        .reduce(function (acc, prop) {
          acc[prop] = individualInstances[index].props[prop];
          return acc;
        }, {});
      singleton.setProps(
        Object.assign({}, overrideProps, {
          getReferenceClientRect:
            typeof overrideProps.getReferenceClientRect === 'function'
              ? overrideProps.getReferenceClientRect
              : function () {
                  return target.getBoundingClientRect();
                },
        }),
      );
    }

    enableInstances(false);
    setReferences();
    var plugin = {
      fn: function fn() {
        return {
          onDestroy: function onDestroy() {
            enableInstances(true);
          },
          onHidden: function onHidden() {
            currentTarget = null;
          },
          onClickOutside: function onClickOutside(instance) {
            if (instance.props.showOnCreate && !shownOnCreate) {
              shownOnCreate = true;
              currentTarget = null;
            }
          },
          onShow: function onShow(instance) {
            if (instance.props.showOnCreate && !shownOnCreate) {
              shownOnCreate = true;
              prepareInstance(instance, references[0]);
            }
          },
          onTrigger: function onTrigger(instance, event) {
            prepareInstance(instance, event.currentTarget);
          },
        };
      },
    };
    var singleton = tippy(
      div(),
      Object.assign({}, removeProperties(optionalProps, ['overrides']), {
        plugins: [plugin].concat(optionalProps.plugins || []),
        triggerTarget: references,
        popperOptions: Object.assign({}, optionalProps.popperOptions, {
          modifiers: [].concat(
            ((_optionalProps$popper = optionalProps.popperOptions) == null
              ? void 0
              : _optionalProps$popper.modifiers) || [],
            [applyStylesModifier],
          ),
        }),
      }),
    );
    var originalShow = singleton.show;

    singleton.show = function (target) {
      originalShow(); // first time, showOnCreate or programmatic call with no params
      // default to showing first instance

      if (!currentTarget && target == null) {
        return prepareInstance(singleton, references[0]);
      } // triggered from event (do nothing as prepareInstance already called by onTrigger)
      // programmatic call with no params when already visible (do nothing again)

      if (currentTarget && target == null) {
        return;
      } // target is index of instance

      if (typeof target === 'number') {
        return (
          references[target] && prepareInstance(singleton, references[target])
        );
      } // target is a child tippy instance

      if (individualInstances.includes(target)) {
        var ref = target.reference;
        return prepareInstance(singleton, ref);
      } // target is a ReferenceElement

      if (references.includes(target)) {
        return prepareInstance(singleton, target);
      }
    };

    singleton.showNext = function () {
      var first = references[0];

      if (!currentTarget) {
        return singleton.show(0);
      }

      var index = references.indexOf(currentTarget);
      singleton.show(references[index + 1] || first);
    };

    singleton.showPrevious = function () {
      var last = references[references.length - 1];

      if (!currentTarget) {
        return singleton.show(last);
      }

      var index = references.indexOf(currentTarget);
      var target = references[index - 1] || last;
      singleton.show(target);
    };

    var originalSetProps = singleton.setProps;

    singleton.setProps = function (props) {
      overrides = props.overrides || overrides;
      originalSetProps(props);
    };

    singleton.setInstances = function (nextInstances) {
      enableInstances(true);
      interceptSetPropsCleanups.forEach(function (fn) {
        return fn();
      });
      individualInstances = nextInstances;
      enableInstances(false);
      setReferences();
      interceptSetProps(singleton);
      singleton.setProps({
        triggerTarget: references,
      });
    };

    interceptSetPropsCleanups = interceptSetProps(singleton);
    return singleton;
  };

  var BUBBLING_EVENTS_MAP = {
    mouseover: 'mouseenter',
    focusin: 'focus',
    click: 'click',
  };
  /**
   * Creates a delegate instance that controls the creation of tippy instances
   * for child elements (`target` CSS selector).
   */

  function delegate(targets, props) {
    /* istanbul ignore else */
    {
      errorWhen(
        !(props && props.target),
        [
          'You must specity a `target` prop indicating a CSS selector string matching',
          'the target elements that should receive a tippy.',
        ].join(' '),
      );
    }

    var listeners = [];
    var childTippyInstances = [];
    var disabled = false;
    var target = props.target;
    var nativeProps = removeProperties(props, ['target']);
    var parentProps = Object.assign({}, nativeProps, {
      trigger: 'manual',
      touch: false,
    });
    var childProps = Object.assign({}, nativeProps, {
      showOnCreate: true,
    });
    var returnValue = tippy(targets, parentProps);
    var normalizedReturnValue = normalizeToArray(returnValue);

    function onTrigger(event) {
      if (!event.target || disabled) {
        return;
      }

      var targetNode = event.target.closest(target);

      if (!targetNode) {
        return;
      } // Get relevant trigger with fallbacks:
      // 1. Check `data-tippy-trigger` attribute on target node
      // 2. Fallback to `trigger` passed to `delegate()`
      // 3. Fallback to `defaultProps.trigger`

      var trigger =
        targetNode.getAttribute('data-tippy-trigger') ||
        props.trigger ||
        defaultProps.trigger; // @ts-ignore

      if (targetNode._tippy) {
        return;
      }

      if (
        event.type === 'touchstart' &&
        typeof childProps.touch === 'boolean'
      ) {
        return;
      }

      if (
        event.type !== 'touchstart' &&
        trigger.indexOf(BUBBLING_EVENTS_MAP[event.type]) < 0
      ) {
        return;
      }

      var instance = tippy(targetNode, childProps);

      if (instance) {
        childTippyInstances = childTippyInstances.concat(instance);
      }
    }

    function on(node, eventType, handler, options) {
      if (options === void 0) {
        options = false;
      }

      node.addEventListener(eventType, handler, options);
      listeners.push({
        node: node,
        eventType: eventType,
        handler: handler,
        options: options,
      });
    }

    function addEventListeners(instance) {
      var reference = instance.reference;
      on(reference, 'touchstart', onTrigger, TOUCH_OPTIONS);
      on(reference, 'mouseover', onTrigger);
      on(reference, 'focusin', onTrigger);
      on(reference, 'click', onTrigger);
    }

    function removeEventListeners() {
      listeners.forEach(function (_ref) {
        var node = _ref.node,
          eventType = _ref.eventType,
          handler = _ref.handler,
          options = _ref.options;
        node.removeEventListener(eventType, handler, options);
      });
      listeners = [];
    }

    function applyMutations(instance) {
      var originalDestroy = instance.destroy;
      var originalEnable = instance.enable;
      var originalDisable = instance.disable;

      instance.destroy = function (shouldDestroyChildInstances) {
        if (shouldDestroyChildInstances === void 0) {
          shouldDestroyChildInstances = true;
        }

        if (shouldDestroyChildInstances) {
          childTippyInstances.forEach(function (instance) {
            instance.destroy();
          });
        }

        childTippyInstances = [];
        removeEventListeners();
        originalDestroy();
      };

      instance.enable = function () {
        originalEnable();
        childTippyInstances.forEach(function (instance) {
          return instance.enable();
        });
        disabled = false;
      };

      instance.disable = function () {
        originalDisable();
        childTippyInstances.forEach(function (instance) {
          return instance.disable();
        });
        disabled = true;
      };

      addEventListeners(instance);
    }

    normalizedReturnValue.forEach(applyMutations);
    return returnValue;
  }

  var animateFill = {
    name: 'animateFill',
    defaultValue: false,
    fn: function fn(instance) {
      var _instance$props$rende;

      // @ts-ignore
      if (
        !((_instance$props$rende = instance.props.render) == null
          ? void 0
          : _instance$props$rende.$$tippy)
      ) {
        {
          errorWhen(
            instance.props.animateFill,
            'The `animateFill` plugin requires the default render function.',
          );
        }

        return {};
      }

      var _getChildren = getChildren(instance.popper),
        box = _getChildren.box,
        content = _getChildren.content;

      var backdrop = instance.props.animateFill
        ? createBackdropElement()
        : null;
      return {
        onCreate: function onCreate() {
          if (backdrop) {
            box.insertBefore(backdrop, box.firstElementChild);
            box.setAttribute('data-animatefill', '');
            box.style.overflow = 'hidden';
            instance.setProps({
              arrow: false,
              animation: 'shift-away',
            });
          }
        },
        onMount: function onMount() {
          if (backdrop) {
            var transitionDuration = box.style.transitionDuration;
            var duration = Number(transitionDuration.replace('ms', '')); // The content should fade in after the backdrop has mostly filled the
            // tooltip element. `clip-path` is the other alternative but is not
            // well-supported and is buggy on some devices.

            content.style.transitionDelay = Math.round(duration / 10) + 'ms';
            backdrop.style.transitionDuration = transitionDuration;
            setVisibilityState([backdrop], 'visible');
          }
        },
        onShow: function onShow() {
          if (backdrop) {
            backdrop.style.transitionDuration = '0ms';
          }
        },
        onHide: function onHide() {
          if (backdrop) {
            setVisibilityState([backdrop], 'hidden');
          }
        },
      };
    },
  };

  function createBackdropElement() {
    var backdrop = div();
    backdrop.className = BACKDROP_CLASS;
    setVisibilityState([backdrop], 'hidden');
    return backdrop;
  }

  var mouseCoords = {
    clientX: 0,
    clientY: 0,
  };
  var activeInstances = [];

  function storeMouseCoords(_ref) {
    var clientX = _ref.clientX,
      clientY = _ref.clientY;
    mouseCoords = {
      clientX: clientX,
      clientY: clientY,
    };
  }

  function addMouseCoordsListener(doc) {
    doc.addEventListener('mousemove', storeMouseCoords);
  }

  function removeMouseCoordsListener(doc) {
    doc.removeEventListener('mousemove', storeMouseCoords);
  }

  var followCursor = {
    name: 'followCursor',
    defaultValue: false,
    fn: function fn(instance) {
      var reference = instance.reference;
      var doc = getOwnerDocument(instance.props.triggerTarget || reference);
      var isInternalUpdate = false;
      var wasFocusEvent = false;
      var isUnmounted = true;
      var prevProps = instance.props;

      function getIsInitialBehavior() {
        return (
          instance.props.followCursor === 'initial' && instance.state.isVisible
        );
      }

      function addListener() {
        doc.addEventListener('mousemove', onMouseMove);
      }

      function removeListener() {
        doc.removeEventListener('mousemove', onMouseMove);
      }

      function unsetGetReferenceClientRect() {
        isInternalUpdate = true;
        instance.setProps({
          getReferenceClientRect: null,
        });
        isInternalUpdate = false;
      }

      function onMouseMove(event) {
        // If the instance is interactive, avoid updating the position unless it's
        // over the reference element
        var isCursorOverReference = event.target
          ? reference.contains(event.target)
          : true;
        var followCursor = instance.props.followCursor;
        var clientX = event.clientX,
          clientY = event.clientY;
        var rect = reference.getBoundingClientRect();
        var relativeX = clientX - rect.left;
        var relativeY = clientY - rect.top;

        if (isCursorOverReference || !instance.props.interactive) {
          instance.setProps({
            getReferenceClientRect: function getReferenceClientRect() {
              var rect = reference.getBoundingClientRect();
              var x = clientX;
              var y = clientY;

              if (followCursor === 'initial') {
                x = rect.left + relativeX;
                y = rect.top + relativeY;
              }

              var top = followCursor === 'horizontal' ? rect.top : y;
              var right = followCursor === 'vertical' ? rect.right : x;
              var bottom = followCursor === 'horizontal' ? rect.bottom : y;
              var left = followCursor === 'vertical' ? rect.left : x;
              return {
                width: right - left,
                height: bottom - top,
                top: top,
                right: right,
                bottom: bottom,
                left: left,
              };
            },
          });
        }
      }

      function create() {
        if (instance.props.followCursor) {
          activeInstances.push({
            instance: instance,
            doc: doc,
          });
          addMouseCoordsListener(doc);
        }
      }

      function destroy() {
        activeInstances = activeInstances.filter(function (data) {
          return data.instance !== instance;
        });

        if (
          activeInstances.filter(function (data) {
            return data.doc === doc;
          }).length === 0
        ) {
          removeMouseCoordsListener(doc);
        }
      }

      return {
        onCreate: create,
        onDestroy: destroy,
        onBeforeUpdate: function onBeforeUpdate() {
          prevProps = instance.props;
        },
        onAfterUpdate: function onAfterUpdate(_, _ref2) {
          var followCursor = _ref2.followCursor;

          if (isInternalUpdate) {
            return;
          }

          if (
            followCursor !== undefined &&
            prevProps.followCursor !== followCursor
          ) {
            destroy();

            if (followCursor) {
              create();

              if (
                instance.state.isMounted &&
                !wasFocusEvent &&
                !getIsInitialBehavior()
              ) {
                addListener();
              }
            } else {
              removeListener();
              unsetGetReferenceClientRect();
            }
          }
        },
        onMount: function onMount() {
          if (instance.props.followCursor && !wasFocusEvent) {
            if (isUnmounted) {
              onMouseMove(mouseCoords);
              isUnmounted = false;
            }

            if (!getIsInitialBehavior()) {
              addListener();
            }
          }
        },
        onTrigger: function onTrigger(_, event) {
          if (isMouseEvent(event)) {
            mouseCoords = {
              clientX: event.clientX,
              clientY: event.clientY,
            };
          }

          wasFocusEvent = event.type === 'focus';
        },
        onHidden: function onHidden() {
          if (instance.props.followCursor) {
            unsetGetReferenceClientRect();
            removeListener();
            isUnmounted = true;
          }
        },
      };
    },
  };

  function getProps(props, modifier) {
    var _props$popperOptions;

    return {
      popperOptions: Object.assign({}, props.popperOptions, {
        modifiers: [].concat(
          (
            ((_props$popperOptions = props.popperOptions) == null
              ? void 0
              : _props$popperOptions.modifiers) || []
          ).filter(function (_ref) {
            var name = _ref.name;
            return name !== modifier.name;
          }),
          [modifier],
        ),
      }),
    };
  }

  var inlinePositioning = {
    name: 'inlinePositioning',
    defaultValue: false,
    fn: function fn(instance) {
      var reference = instance.reference;

      function isEnabled() {
        return !!instance.props.inlinePositioning;
      }

      var placement;
      var cursorRectIndex = -1;
      var isInternalUpdate = false;
      var modifier = {
        name: 'tippyInlinePositioning',
        enabled: true,
        phase: 'afterWrite',
        fn: function fn(_ref2) {
          var state = _ref2.state;

          if (isEnabled()) {
            if (placement !== state.placement) {
              instance.setProps({
                getReferenceClientRect: function getReferenceClientRect() {
                  return _getReferenceClientRect(state.placement);
                },
              });
            }

            placement = state.placement;
          }
        },
      };

      function _getReferenceClientRect(placement) {
        return getInlineBoundingClientRect(
          getBasePlacement(placement),
          reference.getBoundingClientRect(),
          arrayFrom(reference.getClientRects()),
          cursorRectIndex,
        );
      }

      function setInternalProps(partialProps) {
        isInternalUpdate = true;
        instance.setProps(partialProps);
        isInternalUpdate = false;
      }

      function addModifier() {
        if (!isInternalUpdate) {
          setInternalProps(getProps(instance.props, modifier));
        }
      }

      return {
        onCreate: addModifier,
        onAfterUpdate: addModifier,
        onTrigger: function onTrigger(_, event) {
          if (isMouseEvent(event)) {
            var rects = arrayFrom(instance.reference.getClientRects());
            var cursorRect = rects.find(function (rect) {
              return (
                rect.left - 2 <= event.clientX &&
                rect.right + 2 >= event.clientX &&
                rect.top - 2 <= event.clientY &&
                rect.bottom + 2 >= event.clientY
              );
            });
            cursorRectIndex = rects.indexOf(cursorRect);
          }
        },
        onUntrigger: function onUntrigger() {
          cursorRectIndex = -1;
        },
      };
    },
  };
  function getInlineBoundingClientRect(
    currentBasePlacement,
    boundingRect,
    clientRects,
    cursorRectIndex,
  ) {
    // Not an inline element, or placement is not yet known
    if (clientRects.length < 2 || currentBasePlacement === null) {
      return boundingRect;
    } // There are two rects and they are disjoined

    if (
      clientRects.length === 2 &&
      cursorRectIndex >= 0 &&
      clientRects[0].left > clientRects[1].right
    ) {
      return clientRects[cursorRectIndex] || boundingRect;
    }

    switch (currentBasePlacement) {
      case 'top':
      case 'bottom': {
        var firstRect = clientRects[0];
        var lastRect = clientRects[clientRects.length - 1];
        var isTop = currentBasePlacement === 'top';
        var top = firstRect.top;
        var bottom = lastRect.bottom;
        var left = isTop ? firstRect.left : lastRect.left;
        var right = isTop ? firstRect.right : lastRect.right;
        var width = right - left;
        var height = bottom - top;
        return {
          top: top,
          bottom: bottom,
          left: left,
          right: right,
          width: width,
          height: height,
        };
      }

      case 'left':
      case 'right': {
        var minLeft = Math.min.apply(
          Math,
          clientRects.map(function (rects) {
            return rects.left;
          }),
        );
        var maxRight = Math.max.apply(
          Math,
          clientRects.map(function (rects) {
            return rects.right;
          }),
        );
        var measureRects = clientRects.filter(function (rect) {
          return currentBasePlacement === 'left'
            ? rect.left === minLeft
            : rect.right === maxRight;
        });
        var _top = measureRects[0].top;
        var _bottom = measureRects[measureRects.length - 1].bottom;
        var _left = minLeft;
        var _right = maxRight;

        var _width = _right - _left;

        var _height = _bottom - _top;

        return {
          top: _top,
          bottom: _bottom,
          left: _left,
          right: _right,
          width: _width,
          height: _height,
        };
      }

      default: {
        return boundingRect;
      }
    }
  }

  var sticky = {
    name: 'sticky',
    defaultValue: false,
    fn: function fn(instance) {
      var reference = instance.reference,
        popper = instance.popper;

      function getReference() {
        return instance.popperInstance
          ? instance.popperInstance.state.elements.reference
          : reference;
      }

      function shouldCheck(value) {
        return (
          instance.props.sticky === true || instance.props.sticky === value
        );
      }

      var prevRefRect = null;
      var prevPopRect = null;

      function updatePosition() {
        var currentRefRect = shouldCheck('reference')
          ? getReference().getBoundingClientRect()
          : null;
        var currentPopRect = shouldCheck('popper')
          ? popper.getBoundingClientRect()
          : null;

        if (
          (currentRefRect && areRectsDifferent(prevRefRect, currentRefRect)) ||
          (currentPopRect && areRectsDifferent(prevPopRect, currentPopRect))
        ) {
          if (instance.popperInstance) {
            instance.popperInstance.update();
          }
        }

        prevRefRect = currentRefRect;
        prevPopRect = currentPopRect;

        if (instance.state.isMounted) {
          requestAnimationFrame(updatePosition);
        }
      }

      return {
        onMount: function onMount() {
          if (instance.props.sticky) {
            updatePosition();
          }
        },
      };
    },
  };

  function areRectsDifferent(rectA, rectB) {
    if (rectA && rectB) {
      return (
        rectA.top !== rectB.top ||
        rectA.right !== rectB.right ||
        rectA.bottom !== rectB.bottom ||
        rectA.left !== rectB.left
      );
    }

    return true;
  }

  if (isBrowser) {
    injectCSS(css);
  }

  tippy.setDefaultProps({
    plugins: [animateFill, followCursor, inlinePositioning, sticky],
    render: render,
  });
  tippy.createSingleton = createSingleton;
  tippy.delegate = delegate;
  tippy.hideAll = hideAll;
  tippy.roundArrow = ROUND_ARROW;

  return tippy;
});
//# sourceMappingURL=tippy-bundle.umd.js.map

/*!
 *	dotdotdot JS 4.1.0
 *
 *	dotdotdot.frebsite.nl
 *
 *	Copyright (c) Fred Heusschen
 *	www.frebsite.nl
 *
 *	License: CC-BY-NC-4.0
 *	http://creativecommons.org/licenses/by-nc/4.0/
 */
var Dotdotdot = (function () {
  function t(e, i) {
    void 0 === i && (i = t.options);
    var n = this;
    for (var o in ((this.container = e),
    (this.options = i || {}),
    (this.watchTimeout = null),
    (this.watchInterval = null),
    (this.resizeEvent = null),
    t.options))
      t.options.hasOwnProperty(o) &&
        void 0 === this.options[o] &&
        (this.options[o] = t.options[o]);
    var r = this.container.dotdotdot;
    r && r.destroy(),
      (this.API = {}),
      ['truncate', 'restore', 'destroy', 'watch', 'unwatch'].forEach(function (
        t,
      ) {
        n.API[t] = function () {
          return n[t].call(n);
        };
      }),
      (this.container.dotdotdot = this.API),
      (this.originalStyle = this.container.getAttribute('style') || ''),
      (this.originalContent = this._getOriginalContent()),
      (this.ellipsis = document.createTextNode(this.options.ellipsis));
    var s = window.getComputedStyle(this.container);
    'break-word' !== s['word-wrap'] &&
      (this.container.style['word-wrap'] = 'break-word'),
      'pre' === s['white-space']
        ? (this.container.style['white-space'] = 'pre-wrap')
        : 'nowrap' === s['white-space'] &&
          (this.container.style['white-space'] = 'normal'),
      null === this.options.height &&
        (this.options.height = this._getMaxHeight()),
      this.truncate(),
      this.options.watch && this.watch();
  }
  return (
    (t.prototype.restore = function () {
      var t = this;
      this.unwatch(),
        this.container.setAttribute('style', this.originalStyle),
        this.container.classList.remove('ddd-truncated'),
        (this.container.innerHTML = ''),
        this.originalContent.forEach(function (e) {
          t.container.append(e);
        });
    }),
    (t.prototype.destroy = function () {
      this.restore(), (this.container.dotdotdot = null);
    }),
    (t.prototype.watch = function () {
      var t = this;
      this.unwatch();
      var e = { width: null, height: null },
        i = function (i, n, o) {
          if (
            t.container.offsetWidth ||
            t.container.offsetHeight ||
            t.container.getClientRects().length
          ) {
            var r = { width: i[n], height: i[o] };
            return (
              (e.width == r.width && e.height == r.height) || t.truncate(), r
            );
          }
          return e;
        };
      'window' === this.options.watch
        ? ((this.resizeEvent = function (n) {
            t.watchTimeout && clearTimeout(t.watchTimeout),
              (t.watchTimeout = setTimeout(function () {
                e = i(window, 'innerWidth', 'innerHeight');
              }, 100));
          }),
          window.addEventListener('resize', this.resizeEvent))
        : (this.watchInterval = setInterval(function () {
            e = i(t.container, 'clientWidth', 'clientHeight');
          }, 1e3));
    }),
    (t.prototype.unwatch = function () {
      this.resizeEvent &&
        (window.removeEventListener('resize', this.resizeEvent),
        (this.resizeEvent = null)),
        this.watchInterval && clearInterval(this.watchInterval),
        this.watchTimeout && clearTimeout(this.watchTimeout);
    }),
    (t.prototype.truncate = function () {
      var t = this,
        e = !1;
      return (
        (this.container.innerHTML = ''),
        this.originalContent.forEach(function (e) {
          t.container.append(e.cloneNode(!0));
        }),
        (this.maxHeight = this._getMaxHeight()),
        this._fits() || ((e = !0), this._truncateToNode(this.container)),
        this.container.classList[e ? 'add' : 'remove']('ddd-truncated'),
        this.options.callback.call(this.container, e),
        e
      );
    }),
    (t.prototype._truncateToNode = function (e) {
      var i = [],
        n = [];
      if (
        (t.$.contents(e).forEach(function (t) {
          if (1 != t.nodeType || !t.matches('.ddd-keep')) {
            var e = document.createComment('');
            t.replaceWith(e), n.push(t), i.push(e);
          }
        }),
        n.length)
      ) {
        for (var o = 0; o < n.length; o++) {
          i[o].replaceWith(n[o]);
          var r = this.ellipsis.cloneNode(!0);
          switch (n[o].nodeType) {
            case 1:
              n[o].append(r);
              break;
            case 3:
              n[o].after(r);
          }
          var s = this._fits();
          if ((r.parentElement.removeChild(r), !s)) {
            if ('node' == this.options.truncate && o > 1)
              return void n[o - 2].remove();
            break;
          }
        }
        for (var a = o; a < i.length; a++) i[a].remove();
        var h = n[Math.max(0, Math.min(o, n.length - 1))];
        if (1 == h.nodeType) {
          var c = document.createElement(h.nodeName);
          c.append(this.ellipsis),
            h.replaceWith(c),
            this._fits()
              ? c.replaceWith(h)
              : (c.remove(), (h = n[Math.max(0, o - 1)]));
        }
        1 == h.nodeType ? this._truncateToNode(h) : this._truncateToWord(h);
      }
    }),
    (t.prototype._truncateToWord = function (t) {
      for (
        var e = t.textContent,
          i = -1 !== e.indexOf(' ') ? ' ' : '　',
          n = e.split(i),
          o = n.length;
        o >= 0;
        o--
      )
        if (
          ((t.textContent = this._addEllipsis(n.slice(0, o).join(i))),
          this._fits())
        ) {
          'letter' == this.options.truncate &&
            ((t.textContent = n.slice(0, o + 1).join(i)),
            this._truncateToLetter(t));
          break;
        }
    }),
    (t.prototype._truncateToLetter = function (t) {
      for (
        var e = t.textContent.split(''), i = '', n = e.length;
        n >= 0 &&
        (!(i = e.slice(0, n).join('')).length ||
          ((t.textContent = this._addEllipsis(i)), !this._fits()));
        n--
      );
    }),
    (t.prototype._fits = function () {
      return (
        this.container.scrollHeight <= this.maxHeight + this.options.tolerance
      );
    }),
    (t.prototype._addEllipsis = function (t) {
      for (
        var e = [' ', '　', ',', ';', '.', '!', '?'];
        e.indexOf(t.slice(-1)) > -1;

      )
        t = t.slice(0, -1);
      return (t += this.ellipsis.textContent);
    }),
    (t.prototype._getOriginalContent = function () {
      var e = 'script, style';
      this.options.keep && (e += ', ' + this.options.keep),
        t.$.find(e, this.container).forEach(function (t) {
          t.classList.add('ddd-keep');
        });
      var i =
        'div, section, article, header, footer, p, h1, h2, h3, h4, h5, h6, table, td, td, dt, dd, li';
      [this.container]
        .concat(t.$.find('*', this.container))
        .forEach(function (e) {
          e.normalize(),
            t.$.contents(e).forEach(function (t) {
              8 == t.nodeType && e.removeChild(t);
            }),
            t.$.contents(e).forEach(function (t) {
              if (3 == t.nodeType && '' == t.textContent.trim()) {
                var n = t.previousSibling,
                  o = t.nextSibling;
                (t.parentElement.matches(
                  'table, thead, tbody, tfoot, tr, dl, ul, ol, video',
                ) ||
                  !n ||
                  (1 == n.nodeType && n.matches(i)) ||
                  !o ||
                  (1 == o.nodeType && o.matches(i))) &&
                  e.removeChild(t);
              }
            });
        });
      var n = [];
      return (
        t.$.contents(this.container).forEach(function (t) {
          n.push(t.cloneNode(!0));
        }),
        n
      );
    }),
    (t.prototype._getMaxHeight = function () {
      if ('number' == typeof this.options.height) return this.options.height;
      for (
        var t = window.getComputedStyle(this.container),
          e = ['maxHeight', 'height'],
          i = 0,
          n = 0;
        n < e.length;
        n++
      ) {
        if ('px' == (o = t[e[n]]).slice(-2)) {
          i = parseFloat(o);
          break;
        }
      }
      if ('border-box' == t.boxSizing) {
        e = [
          'borderTopWidth',
          'borderBottomWidth',
          'paddingTop',
          'paddingBottom',
        ];
        for (n = 0; n < e.length; n++) {
          var o;
          'px' == (o = t[e[n]]).slice(-2) && (i -= parseFloat(o));
        }
      }
      return Math.max(i, 0);
    }),
    (t.version = '4.1.0'),
    (t.options = {
      ellipsis: '… ',
      callback: function (t) {},
      truncate: 'word',
      tolerance: 0,
      keep: null,
      watch: 'window',
      height: null,
    }),
    (t.$ = {
      find: function (t, e) {
        return (
          (e = e || document), Array.prototype.slice.call(e.querySelectorAll(t))
        );
      },
      contents: function (t) {
        return (t = t || document), Array.prototype.slice.call(t.childNodes);
      },
    }),
    t
  );
})();
!(function (t) {
  void 0 !== t &&
    (t.fn.dotdotdot = function (t) {
      return this.each(function (e, i) {
        var n = new Dotdotdot(i, t);
        i.dotdotdot = n.API;
      });
    });
})(window.Zepto || window.jQuery);

/**
 * Waits for an element satisfying selector to exist, then resolves promise with the element.
 * Useful for resolving race conditions.
 *
 * @param selector
 * @returns {Promise}
 */
function elementReady(selector) {
  return new Promise((resolve, reject) => {
    let el = document.querySelector(selector);
    if (el) {
      resolve(el);
    }
    new MutationObserver((mutationRecords, observer) => {
      // Query for elements matching the specified selector
      Array.from(document.querySelectorAll(selector)).forEach(element => {
        resolve(element);
        //Once we have resolved we don't need the observer anymore.
        observer.disconnect();
      });
    }).observe(document.documentElement, {
      childList: true,
      subtree: true,
    });
  });
}

function truncateElements() {
  let wrappers = document.querySelectorAll('[data-truncate]');

  if (!wrappers) return;

  wrappers.forEach(wrapper => {
    let options = {
      height: wrapper.clientHeight,
      truncate: wrapper.hasAttribute('data-truncate-letter')
        ? 'letter'
        : 'word',
    };
    new Dotdotdot(wrapper, options);
  });
}

function initFilterToggle() {
  const dataToggles = document.querySelectorAll('[data-toggle]');

  if (!dataToggles) return;

  Array.prototype.forEach.call(dataToggles, dataToggle => {
    dataToggle.addEventListener('click', function (event) {
      const isExpanded =
        event.currentTarget.getAttribute('aria-expanded') === 'true';

      const fieldset = event.currentTarget.closest('fieldset');

      if (isExpanded) {
        fieldset.classList.add('child-filter--collapsed');
        event.currentTarget.setAttribute('aria-expanded', false);
      } else {
        fieldset.classList.remove('child-filter--collapsed');
        event.currentTarget.setAttribute('aria-expanded', true);
      }
    });
  });
}

function initFilterBlockToggle() {
  const filterToggles = document.querySelectorAll('[data-filter-block-toggle]');

  Array.prototype.forEach.call(filterToggles, toggle => {
    toggle.addEventListener('click', function (event) {
      const isExpanded =
        event.currentTarget.getAttribute('aria-expanded') === 'true';

      const container = event.currentTarget.closest('[data-filter-block]');

      if (isExpanded) {
        container.classList.add('filter-block--collapsed');
        event.currentTarget.setAttribute('aria-expanded', false);
      } else {
        container.classList.remove('filter-block--collapsed');
        event.currentTarget.setAttribute('aria-expanded', true);
      }
    });
  });
}

function initDesktopSidebarToggle() {
  const sidebarToggle = document.querySelector('[data-desktop-sidebar-toggle]');

  if (!sidebarToggle) return;

  sidebarToggle.addEventListener('click', function (event) {
    const isExpanded =
      event.currentTarget.getAttribute('aria-expanded') === 'true';

    const filterLayout = document.querySelector('[data-filter-layout]');

    if (!isExpanded) {
      filterLayout.classList.remove('filter-layout--collapsed');
    } else {
      filterLayout.classList.add('filter-layout--collapsed');
    }

    const sidebar = document.querySelector('[data-desktop-sidebar]');

    if (!isExpanded) {
      sidebar.classList.remove('filter-layout__aside--collapsed');
    } else {
      sidebar.classList.add('filter-layout__aside--collapsed');
    }

    sidebarToggle.setAttribute('aria-expanded', !isExpanded);
    window.setTimeout(truncateElements, 300); // re-truncate everything, but after CSS animation has completed
  });
}

function initMobileSidebarToggle() {
  const sidebarToggles = document.querySelectorAll(
    '[data-mobile-sidebar-toggle]',
  );

  if (!sidebarToggles) return;

  sidebarToggles.forEach(sidebarToggle => {
    sidebarToggle.addEventListener('click', function (event) {
      const isExpanded =
        event.currentTarget.getAttribute('aria-expanded') === 'true';

      const sidebar = document.querySelector('[data-mobile-sidebar]');

      if (!isExpanded) {
        sidebar.classList.add('mobile-sidebar--expanded');
      } else {
        sidebar.classList.remove('mobile-sidebar--expanded');
      }

      // multiple because both the trigger and the close icon have this attribute
      sidebarToggles.forEach(toggle =>
        toggle.setAttribute('aria-expanded', !isExpanded),
      );
    });
  });
}

function adjustDesktopSidebarScroll() {
  const sidebar = document.querySelector('[data-desktop-sidebar]');

  if (!sidebar) return;

  sidebar.style.top = window.scrollY >= 72 ? 0 : `${72 - window.scrollY}px`;

  const distanceFromBottom =
    document.body.clientHeight - (window.innerHeight + window.scrollY);

  sidebar.style.bottom =
    distanceFromBottom <= 72 ? `${72 - distanceFromBottom}px` : 0;
}

function initDesktopSidebarScroll() {
  adjustDesktopSidebarScroll();

  document.addEventListener('scroll', function () {
    adjustDesktopSidebarScroll();
  });
}

function initTooltips() {
  tippy('[data-tippy-content]', { placement: 'bottom', theme: 'pmmi' });
}

function initMainNavToggle() {
  const mainNavToggles = document.querySelectorAll('[data-main-nav-toggle]');

  if (!mainNavToggles) return;

  mainNavToggles.forEach(mainNavToggle => {
    mainNavToggle.addEventListener('click', function (event) {
      const isExpanded =
        event.currentTarget.getAttribute('aria-expanded') === 'true';

      const mainNav = document.querySelector('[data-main-nav]');

      if (!isExpanded) {
        mainNav.classList.add('mobile-nav-menu--expanded');
      } else {
        mainNav.classList.remove('mobile-nav-menu--expanded');
      }

      // multiple because both the trigger and the close icon have this attribute
      mainNavToggles.forEach(toggle =>
        toggle.setAttribute('aria-expanded', !isExpanded),
      );
    });
  });
}

function scripts() {
  truncateElements();
  window.addEventListener('resize', truncateElements, true);

  initFilterToggle();
  initDesktopSidebarToggle();
  initDesktopSidebarScroll();
  initMobileSidebarToggle();
  initFilterBlockToggle();
  initTooltips();
  initMainNavToggle();
}

scripts();

// the below is just for storybook ;)
function runOnInit() {
  scripts();
}
function runOnPageChange() {
  scripts();
}
document.addEventListener(
  'DOMContentLoaded',
  function () {
    runOnInit();
    const callback = function (mutationsList) {
      for (let i = 0, len = mutationsList.length; i < len; i++) {
        if (mutationsList[i].type == 'childList') {
          runOnPageChange();
          break;
        }
      }
    };

    const observer = new MutationObserver(callback);
    const config = { childList: true, subtree: false };
    observer.observe(document.getElementById('root'), config);
  },
  false,
);
